import { StyleSheet, Text, View, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import { NavigationContainer, useNavigationState } from '@react-navigation/native';
import {
  TransitionPresets,
  createNativeStackNavigator,
} from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { NavigationState } from 'react-navigation';



import Navigation from './App/Navigation/index';
import DrawerContent from './App/Drawer/DrawerContent';

import { vrscale, mdscale } from './App/PixelRatio/index';

{/** Vector icon */ }
import Icon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/FontAwesome5';
import Palet from 'react-native-vector-icons/AntDesign'



{/** all screens */ }

import Home from './App/Screens/Home/Home';

import Mycourse from './App/Screens/Mycourse';
import Assesment from './App/Screens/Assesment/Assesment';
import Report from './App/Screens/Report';
import Payment from './App/Screens/Payment/Payment';
import Schedule from './App/Screens/DrawerScreen/Schedule';
import Poll from './App/Screens/DrawerScreen/Pools';
import Mycoursedetail from './App/Screens/Mycoursedetail';
import More from './App/Screens/More';
import Assignment from './App/Screens/Assignment';
import Login from './App/Screens/Auth/Login'
import { useSelector, useDispatch } from 'react-redux'
import { setuser, logout } from './App/Redux/reducer/User'
import AuthService from './App/Service/Auth'
import { log } from 'react-native-reanimated';
import TestPage from './App/Screens/TestPage';
import ViewPdf from './App/Screens/ViewPdf';
import ViewDoc from './App/Screens/ViewDoc'
import JoinClassPage from './App/Screens/JoinClassPage';
import Faqs from './App/Screens/Faqs';
import SurveysCard from './App/Screens/DrawerScreen/SurveysCard';
import Forums from './App/Screens/Forums';
import AssesmentTest from './App/Screens/Assesment/AssesmentTest';






const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();




function Tabs(props) {
  const state = useNavigationState(state => state);
  
  useEffect(() => {
    // const routeName = state.routeNames[state.index];
    // // console.log(routeName);
    console.log(props.route.name);
    // console.log("subhan");
  }, [props.route])
  return (
    <Tab.Navigator


      initialRouteName="Home"
      tabBarOptions={{
        tabBarActiveTintColor: '#FFFFFF',
        tabBarInactiveTintColor: 'lightgray',
        showLabel: false

      }}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        tabBarStyle: {
          backgroundColor: '#3E48A0',
          height: vrscale(50)
        },

      }}
    >
      {/* {props.route.name == "Home" ? */}
      <Tab.Screen name='Home' component={Home}
        options={{
          //tabBarLabel: 'Home',
          showLabel: false,
          tabBarIcon: ({focused}) => (
            <>
              {focused == true ? <Image source={require("./App/Assets/TabBar/ci_home-fill(1).png")} />:
                <Icon name={'home'} color={'#A3AED0'} size={26} /> 
               }</>
          ),

        }}
      />
   

      <Tab.Screen name="Mycourse" component={Mycourse}
        options={{
          //tabBarLabel: 'Mycourse',
          showLabel: false,
          tabBarIcon: ({focused}) => (
            <>
              {focused == true ?<Image source={require("./App/Assets/TabBar/ant-design_play-circle-outlined.png")} />
               :<Palet name={'playcircleo'} color={'#A3AED0'} size={26} />}
            </>
           
          ),

        }}
      />


      <Tab.Screen name="Assesment" component={Assesment}
        options={{
          tabBarLabel: 'Assesment',
          tabBarIcon: ({focused}) => (
            <>
              {focused == true ?<Image source={require("./App/Assets/TabBar/Group54.png")} />
               : <Image source={require("./App/Assets/TabBar/Group53.png")} />}
            </>
           
          ),

        }}
      />

      {/* <Tab.Screen name="Report" component={Report}
        options={{
          tabBarLabel: 'Report',
          tabBarIcon: ({focused}) => (
            <>
              {focused == true ?<Image source={require("./App/Assets/TabBar/Icon.png")} />
               : <Image source={require("./App/Assets/TabBar/Icon(1).png")} />}
            </>
           
          ),
        }}
      /> */}


      {/* <Tab.Screen name="Payment" component={Payment}
        options={{
          tabBarLabel: 'Payment',
          tabBarIcon: ({focused}) => (
            <>
              {focused == true ?<Image source={require("./App/Assets/TabBar/fluent_payment-16-regular(1).png")} />
               : <Image source={require("./App/Assets/TabBar/Vector(2).png")} />}
            </>
           
          ),

        }}
      /> */}
    </Tab.Navigator>



  );
}



function DrawerNav(props) {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      drawerContentOptions={{
        activeTintColor: '#1D3557', /* font color for active screen label */
        activeBackgroundColor: 'transparent', /* bg color for active screen */

      }}
      screenOptions={{
        headerShown: false,
        drawerLabelStyle: { color: "#FFFFFF" },
        drawerActiveBackgroundColor: "#3E48A0"

      }}
      drawerContent={props => <DrawerContent {...props} />}
    >
      <Drawer.Screen
        options={{
          drawerIcon: ({ }) => (
            <Ionicons
              name="home"
              style={{ color: '#EDEDED', fontSize: 18 }}

            />

          ),
        }}
        labelStyle={{ color: '#FFFFFF', }}
        name="Home"
        component={Tabs}

      />
     

      {/* <Drawer.Screen name="Booking" component={Home} /> */}
    </Drawer.Navigator>
  );
}

const App = () => {
  const login_status = useSelector((state) => state.User.login_status)
  const dispatch = useDispatch()


  const checkUser = async () => {
    let account = await AuthService.getAccount()
    //  console.log(account);
    if (account) {
       console.log("account", account.USERID);
      // console.log("account");
      dispatch(setuser(account))
    } else {
      // console.log("no no");
    }
  }
  useEffect(() => {
    checkUser()
  }, [])
  return (

    <NavigationContainer ref={r => {
      Navigation.setTopLevelNavigator(r);
    }}>

      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
          gestureEnabled: true,
          gestureDirection: 'horizontal',
        }}
      >
        {login_status ? <>
          <Stack.Screen name='Home' component={DrawerNav} />
          <Stack.Screen name='Mycourse' component={Mycourse} />
          <Stack.Screen name='AssesmentTest' component={AssesmentTest} />
          <Stack.Screen name='Assesment' component={Assesment} />
          <Stack.Screen name='Report' component={Report} />
          <Stack.Screen name='Payment' component={Payment} />
          <Stack.Screen name='Schedule' component={Schedule} />
          <Stack.Screen name='Poll' component={Poll} />
          <Stack.Screen name='Mycoursedetail' component={Mycoursedetail} />
          <Stack.Screen name='ViewPdf' component={ViewPdf} />
          <Stack.Screen name='ViewDoc' component={ViewDoc} />
          <Stack.Screen name='JoinClassPage' component={JoinClassPage} />
          <Stack.Screen name='More' component={More} />
          <Stack.Screen name='Assignment' component={Assignment} />
          <Stack.Screen name='TestPage' component={TestPage} />
          <Stack.Screen name='Faqs' component={Faqs} />
          <Stack.Screen name='Forums' component={Forums} />
          <Stack.Screen name='SurveysCard' component={SurveysCard} />

        </> :
          <Stack.Screen name='Login' component={Login} />}

      </Stack.Navigator>

    </NavigationContainer>

  )
}

export default App

const styles = StyleSheet.create({})
