import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView
} from 'react-native'
import React, { useState ,useEffect} from 'react'
import AuthService from '../Service/Auth'
import { vrscale, mdscale } from '../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
import Navigation from '../Navigation/index';
import Header from '../Components/Header';
{/** vectot icon */ }
import Palet from 'react-native-vector-icons/AntDesign'
import { useSelector, useDispatch } from 'react-redux'



const Mycourse = () => {

    const [course, setCourse] = useState(
        [
            {
                id: 1,
                imageBook: require('../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },
            {
                id: 2,
                imageBook: require('../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },
            {
                id: 3,
                imageBook: require('../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },
            {
                id: 4,
                imageBook: require('../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },
            {
                id: 5,
                imageBook: require('../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },
            {
                id: 6,
                imageBook: require('../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },


        ]
    )

    const CourseX = ({ item }) => {
        return (
            <Pressable onPress={() => Navigation.navigate('Mycoursedetail',{CourseScheduleId:`${item.CourseScheduleId}`,CourseId:`${item.Id}`,Name:item.Name})}>
            <View style={styles.flatBox}>
                <Image source={require('../Assets/Images/Rectangle149.jpg')} />
                <View style={{ paddingLeft: mdscale(10), flex: 1 }}>
                    <Text style={{ fontWeight: "bold", color: "#000000" }}>{item.Name}</Text>
                    {/* <Text style={{ color: "#8EB5FF" }}>P.V Kiran</Text>
                    <Text style={{ color: "#8EB5FF" }}> 15 of 28 Lessons</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                        <Palet name="clockcircleo" size={14} color={"#000000"} />
                        <Text style={{ color: "#A8A8A8", marginLeft: 5, marginRight: 7 }}>18 Hr.</Text>
                        <Palet name="star" size={14} color={"#FFB547"} />
                        <Text style={{ color: "#A8A8A8", marginLeft: 5 }}>40</Text>
                    </View> */}

                </View>
                <Image source={require('../Assets/Images/Group325.jpg')}
                        style={{ marginRight: 20 }}
                    />
            </View>
        </Pressable>
        )
    }
    const [classes, setClasses] = useState([])
    const [sessions, setSessions] = useState([])
    const userData = useSelector((state) => state.User.userData)
    async function getCourceData() {
        let result = await AuthService.getCourses({ "UserId": userData.USERID, "TenantCode": userData.TENANTCODE, "Username":userData.USERNAME })
        if (result) {
            console.log(result);
            setClasses(result.classes)
            setSessions(result.sessions)
        }
    }
    useEffect(() => {
        getCourceData()
    }, [])

    return (

        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header
                back={false}
                pagename={'My course'} />

            <FlatList
                //columnWrapperStyle={{ justifyContent: 'space-between' }}
                showsVerticalScrollIndicator={false}
                numColumns={1}
                data={classes}
                renderItem={CourseX}
                //console.log(Cardx))
                keyExtractor={item => item.id}
            />
            {/**video view */}
            {/* <View style={{
                width: "100%", height: mdscale(60),
                backgroundColor: "#313131", flexDirection: 'row', alignItems: 'center',
            }}>

                <Image source={require('../Assets/Images/vedio.jpg')}
                    style={{ height: mdscale(60) }}
                />


                <View style={{ marginLeft: 6, flex: 1 }}>
                    <Text style={{ color: "#FFFFFF" }}> C programing Langauge</Text>
                    <Text style={{ color: "#FFFFFF" }}> By P.Laxmi</Text>
                </View>
                <Palet name="pause" color={"#FFFFFF"} size={mdscale(35)}

                    style={{ marginRight: 10 }}
                />

            </View> */}

        </View>
    )
}

export default Mycourse

const styles = StyleSheet.create({
    flatBox: {
        width: mdscale(330),
         marginTop: 20,
         paddingVertical:10,
        backgroundColor: "#FFFFFF", borderRadius: 10,
        elevation: 3,
        paddingLeft: 10,
        marginHorizontal: 10, flexDirection: 'row',
        alignItems: 'center',

    }

})