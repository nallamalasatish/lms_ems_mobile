import {
    SafeAreaView,
    View,
    StyleSheet,
    Image,
    Text,
    Linking,
    TouchableOpacity,
    LayoutAnimation,
    Pressable
} from 'react-native';
import React, { useState, useEffect } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign'
import AppIcon from '../Components/AppIcon'
import { ScrollView } from 'react-native-gesture-handler';
import Navigation from '../Navigation';
import LearningMeterials from '../Service/LearningMeterials'

import { useSelector, useDispatch } from 'react-redux'



const ExpendableComponent = ({ item, subItem }) => {
    const [layoutHight, setLayoutHight] = useState(null)
    const [Show, setShow] = useState(false)

    useEffect(() => {
        if (Show == true) {
            setLayoutHight(null)
        } else {
            setLayoutHight(0)
        }
    }, [Show])
    return (
        <View style={{ width: "100%", paddingHorizontal: 15, paddingVertical: 10, }}>
            <Pressable onPress={() => setShow(s => !s)} style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                <Text style={{ color: "#EDEDED", fontSize: 17, paddingRight: 20, flex: 1 }}>{item}</Text>
                {Show == true ?
                    <AppIcon type={'AntDesign'} name='minus' size={21} color='#EDEDED' />
                    :
                    <AppIcon type={'AntDesign'} name='plus' size={21} color='#EDEDED' />
                }

            </Pressable>

            <View style={{ height: layoutHight, overflow: 'hidden' }}>
                <Text style={{ color: "#A4A4A4", paddingVertical: 10, borderBottomColor: "#464646", borderBottomWidth: 1 }}>
                    {subItem}
                </Text>
            </View>
        </View>
    )
}
const Faqs = () => {
    const [Data, setData] = useState([])

    const loginId = useSelector((state) => state.User)

    async function getFaqApi() {
        try {
            let result = await LearningMeterials.FAQGetList({ "TENANT_CODE": loginId.userData.TENANTCODE, "ContentType": 100 })
            setData(result)
        } catch (error) {
            console.log(error);
        }

    }
    useEffect(() => {
        getFaqApi()
        // console.log(loginId.userData.TENANTCODE);
    }, [])
    return (
        <View style={{ flex: 1, backgroundColor: "#313131" }}>
            <View style={{ flexDirection: "row", alignItems: "center", paddingLeft: 10, paddingVertical: 10, borderBottomColor: "#464646", borderBottomWidth: 1 }}>
                <AntDesign onPress={() => Navigation.back()} name='arrowleft' size={33} color='#FFFFFF' />
                <Text style={{ fontSize: 17, fontWeight: "700", color: "#fff", paddingLeft: 10 }} >FAQ's</Text>
            </View>
            <ScrollView>
                {Data.map(i => <ExpendableComponent item={i.HEADER} subItem={i.Text} />)}

            </ScrollView>
        </View>
    )
}

export default Faqs

const styles = StyleSheet.create({})