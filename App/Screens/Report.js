import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView
} from 'react-native'
import React, { useState } from 'react'

import { vrscale, mdscale } from '../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
import Navigation from '../Navigation/index';
import Header from '../Components/Header';
import { VictoryBar, VictoryGroup, VictoryChart, VictoryAxis, VictoryLegend } from "victory-native";

{/** vectot icon */ }
import Palet from 'react-native-vector-icons/AntDesign'

const Report = () => {



    const data = [
        { sem: 1, number: 7 },
        { sem: 2, number: 6 },
        { sem: 3, number: 8 },
        { sem: 4, number: 7 },
        { sem: 5, number: 7 },
        { sem: 6, number: 7 },
        { sem: 7, number: 7 },
        { sem: 8, number: 8 },
    ];



    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header
                back={false}
                pagename={'Report'}
            />


            <View style={styles.chartBox}>

                <VictoryChart
                    // domainPadding will add space to each side of VictoryBar to
                    // prevent it from overlapping the axis
                    domainPadding={{ x: 5 }}
                    domain={{ y: [0, 10] }}

                >
                    <VictoryAxis
                        style={{
                            axis: { stroke: "transparent" },
                            ticks: { stroke: "transparent" },


                            tickLabels: { fontSize: 9 }
                        }}
                        // tickValues specifies both the number of ticks and where
                        // they are placed on the axis

                        tickValues={[1, 2, 3, 4, 5, 6, 7, 8]}
                        tickFormat={["SEM1", "SEM2", "SEM3", "SEM4", "SEM5", "SEM6", "SEM7", "SEM8"]}
                    />
                    <VictoryAxis
                        dependentAxis
                        style={{
                            axis: { stroke: "transparent" },
                            ticks: { stroke: "transparent" },
                            //tickLabels: { fill: "transparent" }
                        }}
                    // tickFormat specifies how ticks should be displayed

                    />
                    <VictoryBar
                        data={data}
                        x="sem"
                        y="number"
                        barRatio={0.5}
                        cornerRadius={{ topLeft: 8, topRight: 8, bottomLeft: 8, bottomRight: 8 }}

                        style={{
                            //data: { fill: ({ data }) => data.x > 700 ? "green" : "#ff684f" },
                            data: { fill: "#ff684f" }

                        }}


                    />
                    <VictoryLegend x={50} y={10}

                        data={[
                            {
                                name: 'CGPA',
                                symbol: {
                                    fill: "#ffffff",
                                }
                            }
                        ]}
                    />


                </VictoryChart>


            </View>

        </View>
    )
}

export default Report

const styles = StyleSheet.create({

    chartBox: {
        width: '93%',
        backgroundColor: "#ffffff",
        elevation: 3,
        borderRadius: 10, alignSelf: 'center',
        marginTop: 20

    }
})