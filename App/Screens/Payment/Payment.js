import { StyleSheet, Text, View, Dimensions,Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView} from 'react-native'
import React, { useState } from 'react'

import { vrscale, mdscale } from '../../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
// import Navigation from '../../Navigation/index';
import Header from '../../Components/Header';
import Palet from 'react-native-vector-icons/AntDesign'




const ChakBar = ({ c,setSelect }) => {
    return (
        <>
            {c == true ?
                <Pressable onPress={() => setSelect(false)}>
                    <Image source={require('../../Assets/Images/tickmark.jpg')}
                        style={{ marginRight: 10 }} /></Pressable> :
                <Pressable onPress={() => setSelect(true)} style={{
                    height: 26,
                    width: 26,
                    marginRight: 10,
                    backgroundColor: '#fff',
                    borderRadius: 3,
                    borderWidth: 2,
                    borderColor: "#A1A1A1"
                }}>
                </Pressable>
            }
        </>
    )
}

const PaymentCard =()=>{
    const [Select, setSelect] = useState(false)
    return(
        <View style={Select == true? styles.flatBox2:styles.flatBox}>
        <View style={{ flex: 1, }}>
            <Text style={{ color: "#000000", fontSize: 14, fontWeight: '700' }}>Hostel Fees</Text>
            <Text style={{ color: "#777777", fontSize: 12, fontWeight: '400', marginTop: 7 }}>Total: 15,000 | Paid: 10,000 | Due: 5,000</Text>
        </View>
        <ChakBar c={Select} setSelect={setSelect}  />

    </View>
    )
}

const Payment = () => {
    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header
                back={false}
                pagename={'Payment'}
            />

            {/*** 1st flat card */}

            {/*** 1st flat card */}
            <PaymentCard/>
            <PaymentCard/>
            <PaymentCard/>
            <PaymentCard/>
           
          
            {/*** 1st flat card */}



            <View style={styles.flatBoxSmall}>
                <Text style={{ color: "#FFFFFF" }}>Total Amount</Text>

                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ color: "#FFFFFF", paddingRight: 6 }}>Pay</Text>
                    <Palet name="arrowright" size={18} color={"#FFFFFF"}
                        style={{}} />
                </View>





            </View>
        </View>
    )
}

export default Payment

const styles = StyleSheet.create({
    flatBox: {
        width: mdscale(330),
        height: 70, marginTop: 20,
        backgroundColor: "#FFFFFF", borderRadius: 10,
        elevation: 3,
        paddingLeft: 10,
        marginHorizontal: 10, flexDirection: 'row',
        alignItems: 'center',

        //justifyContent: "space-around"
        

    }, flatBox2: {
        width: mdscale(330),
        height: 70, marginTop: 20,
        backgroundColor: "#FFFFFF", borderRadius: 10,
        elevation: 3,
        paddingLeft: 10,
        marginHorizontal: 10, flexDirection: 'row',
        alignItems: 'center',
        //justifyContent: "space-around"
        borderWidth:1.5,borderColor:"#5DCC69"
    },

    flatBoxSmall: {
        width: mdscale(330),
        height: 50, marginTop: 40,
        backgroundColor: "#5DCC69", borderRadius: 10,
        elevation: 3,
        paddingLeft: 10,
        marginHorizontal: 10, flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between",
        paddingRight: 15
        
    },



})