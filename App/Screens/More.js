import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView
} from 'react-native'
import React, { useState } from 'react'
import Navigation from '../Navigation/index';

import { vrscale, mdscale } from '../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
import Header from '../Components/Header';

{/** vectot icon */ }
import Palet from 'react-native-vector-icons/MaterialCommunityIcons'
import Iconu from 'react-native-vector-icons/Entypo'
import Note from 'react-native-vector-icons/SimpleLineIcons'
import Feed from 'react-native-vector-icons/Foundation'
import Feedback from 'react-native-vector-icons/MaterialIcons'





const More = () => {
    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Image source={require('../Assets/Images/Groupvideo.jpg')}
                style={{ width: WIDTH }}
            />

            {/** */}
            <View style={{
                Width: WIDTH,
                height: 142, backgroundColor: "#313131"
            }}>

                <Text style={{
                    color: "#ffffff", fontSize: mdscale(17), marginVertical: 10,
                    paddingLeft: mdscale(10), fontWeight: "500"
                }}> C Programming Language</Text>

                <Text style={{
                    color: "#C8C8C8", fontSize: mdscale(12),
                    paddingLeft: mdscale(10), fontWeight: "500"
                }}> by Prof.Lakshmi</Text>

                <View style={{ flexDirection: 'row', marginVertical: 10, }}>

                    <Pressable onPress={() => Navigation.navigate('Mycoursedetail')}>
                        <Text style={{
                            color: "#A1A1A1", fontSize: mdscale(15),
                            paddingLeft: mdscale(10), fontWeight: "500",

                        }}> Contact</Text>

                    </Pressable>

                    <Pressable onPress={() => Navigation.navigate('More')}>

                        <Text style={{
                            color: "#ffffff", fontSize: mdscale(15),
                            paddingLeft: mdscale(10), fontWeight: "500",
                            textDecorationLine: 'underline',

                        }}> More</Text>

                    </Pressable>
                </View>

            </View>
            {/***** */}
            <View style={{
                height: '100%',
                width: WIDTH,
                //backgroundColor: "green",
                backgroundColor: "#313131"
            }}>


                <View style={{ flexDirection: 'row' }}>
                    <Iconu name="dots-three-vertical" color={"#FFFFFF"} size={mdscale(16)}
                        style={{ marginLeft: 10 }} />
                    <Text style={{
                        color: "#ffffff", fontSize: mdscale(15),
                        paddingLeft: mdscale(10), fontWeight: "500",
                    }}> About Course</Text>
                </View>

                <View style={{
                    Width: '80%',
                    color: "#ffffff",
                    height: 2, lineHeight: 2
                }} />
                {/** */}
                <Pressable onPress={() => Navigation.navigate('Assignment')}>
                    <View style={{ flexDirection: 'row', marginTop: vrscale(20) }}>

                        <Palet name="clipboard-text-outline" color={"#FFFFFF"} size={mdscale(16)}
                            style={{ marginLeft: 10 }} />
                        <Text style={{
                            color: "#ffffff", fontSize: mdscale(15),
                            paddingLeft: mdscale(10), fontWeight: "500",
                        }}> Assignment</Text>

                    </View>
                </Pressable>
                {/** */}

                <View style={{ flexDirection: 'row', marginTop: vrscale(20) }}>
                    <Note name="note" color={"#FFFFFF"} size={mdscale(16)}
                        style={{ marginLeft: 10 }} />
                    <Text style={{
                        color: "#ffffff", fontSize: mdscale(15),
                        paddingLeft: mdscale(10), fontWeight: "500",
                    }}> Notes</Text>
                </View>

                {/** */}

                <View style={{ flexDirection: 'row', marginTop: vrscale(20) }}>
                    <Palet name="chat-outline" color={"#FFFFFF"} size={mdscale(16)}
                        style={{ marginLeft: 10 }} />
                    <Text style={{
                        color: "#ffffff", fontSize: mdscale(15),
                        paddingLeft: mdscale(10), fontWeight: "500",
                    }}> Forums</Text>
                </View>

                {/** */}

                <View style={{ flexDirection: 'row', marginTop: vrscale(20) }}>
                    <Feed name="comment-quotes" color={"#FFFFFF"} size={mdscale(16)}
                        style={{ marginLeft: 10 }} />
                    <Text style={{
                        color: "#ffffff", fontSize: mdscale(15),
                        paddingLeft: mdscale(10), fontWeight: "500",
                    }}> FAQ's</Text>
                </View>

                {/** */}

                <View style={{ flexDirection: 'row', marginTop: vrscale(20) }}>
                    <Feedback name="feedback" color={"#FFFFFF"} size={mdscale(16)}
                        style={{ marginLeft: 10 }} />
                    <Text style={{
                        color: "#ffffff", fontSize: mdscale(15),
                        paddingLeft: mdscale(10), fontWeight: "500",
                    }}> Feedback</Text>
                </View>

            </View>
        </View>
    )
}

export default More

const styles = StyleSheet.create({})