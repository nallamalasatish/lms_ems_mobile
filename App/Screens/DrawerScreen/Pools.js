import {
    StyleSheet, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView, TextInput
} from 'react-native'
import React, { useEffect, useState } from 'react';
import Header from '../../Components/Header';
import { vrscale, mdscale } from '../../PixelRatio';
const WIDTH = Dimensions.get('window').width;
import PollsApi from '../../Service/PollsApi'
import Toast from 'react-native-simple-toast';

import { Container, Content, ListItem, Text, Radio, Right, Left } from 'native-base';
import Navigation from '../../Navigation';
import { useSelector, useDispatch } from 'react-redux'
//import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';


const OpinionPoll = ({ item, userId }) => {
    const [Ans, setAns] = useState([])
    const [Qustion, setQustion] = useState('')
    async function getAns() {
        let prams = userId + '/' + item.PollId
        try {
            let res = await PollsApi.GetPollQuestions(prams)
            console.log("geyqqq", res);
            setQustion(res[0].PollQuestion)
            setAns(res)
        } catch (error) {
            console.log(error);
        }
    }
    async function submitAns(answerId) {

        try {
            let res = await PollsApi.SavePolls({ "answer": answerId, "PollId": item.PollId, "UserId": userId })
            console.log("geyqqq", res);
            // setQustion(res[0].PollQuestion)
            // setAns(res)
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getAns()
    }, [])
    return (
        <View style={{
            width: mdscale(320),
            //height: mdscale(117),
            backgroundColor: "#FFFFFF", borderRadius: 10,
            marginLeft: 20, marginTop: 20, elevation: 3
        }}>
            <Text allowFontScaling={false} style={{
                color: "#000000",
                paddingLeft: 20, marginTop: 10, fontSize: 14, fontFamily: "Poppins-Medium"
            }}>{Qustion}</Text>
            <View style={{
                flexDirection: 'row', justifyContent: 'space-around',
                marginVertical: mdscale(5)
            }}>

                {Ans.map(item => (
                    <Pressable onPress={() => submitAns(item.OptionId)} style={styles.navBox}>

                        <Text allowFontScaling={false} style={{ color: '#6268EE' }}> {item.OptionText} </Text>

                    </Pressable>

                ))}

            </View>
        </View>
    )
}
const RadioButton = ({ item, userId }) => {
    const [AnsSubmited, setAnsSubmited] = useState(false)
    const [show, setShow] = useState(false)
    const [Ans, setAns] = useState([])
    const [PollAns, setPollAns] = useState('')
    const [Qustion, setQustion] = useState('')
    async function getAns() {
        let prams = userId + '/' + item.PollId
        try {
            let res = await PollsApi.GetPollQuestions(prams)
            console.log("geyqqq", res);
            setQustion(res[0].PollQuestion)
            setAns(res)
        } catch (error) {
            console.log(error);
        }
    }
    async function submitAns() {
        if (PollAns == '') {
            Toast.show('Please answer the Questions', Toast.SHORT, ['UIAlertController']);
        } else {
            try {
                let res = await PollsApi.SavePolls({ "answer": PollAns, "PollId": item.PollId, "UserId": userId })
                console.log("aaaaaa", res);
                // setQustion(res[0].PollQuestion)
                setAnsSubmited(true)
                // setAns(res)
            } catch (error) {
                console.log(error);
            }
        }
    }

    useEffect(() => {
        getAns()
    }, [])
    return (
        <>
            {AnsSubmited == false ?
                <>
                    {show == false ?
                        <View style={{ width: "100%", paddingHorizontal: 10 }}>
                            <Pressable onPress={() => setShow(true)} style={{
                                width: '100%',
                                backgroundColor: "#fff",
                                elevation: 5,
                                padding: 10,
                                borderRadius: 7,
                                marginTop: 10
                            }}>
                                <Text allowFontScaling={false} style={{
                                    fontWeight: "500",
                                    fontSize: 15
                                }}>
                                    {item.PollTitle}
                                </Text>
                            </Pressable></View>
                        :
                        <View style={styles.selectBox}>
                            <Text allowFontScaling={false} style={{
                                color: "#000000",
                                paddingLeft: 20, marginTop: 10, fontFamily: "Poppins-Medium"
                            }}>{Qustion}</Text>
                            {Ans.map((data) => (
                                <View style={{
                                    marginLeft: 10, marginTop: 10,
                                    flexDirection: 'row', marginRight: mdscale(40)
                                }}>

                                    <Radio color='#3C3C3C' selected={PollAns == data.OptionId ? true : false}
                                        onPress={() => setPollAns(data.OptionId)} />
                                    <Text allowFontScaling={false} style={{
                                        color: "#3C3C3C",
                                        fontSize: mdscale(14), paddingLeft: 4, fontFamily: "Poppins-Medium"
                                    }}>{data.OptionText}</Text>

                                </View>
                            ))}
                            <Pressable
                                style={styles.btn}
                                onPress={() => submitAns()}>
                                <Text allowFontScaling={false} style={styles.btn_txt}>Submit</Text>
                            </Pressable>
                        </View>
                    }
                </> : <View />
            }
        </>
    )
}
const Pools = ({ navigation }) => {
    const [ViewTab, setViewTab] = useState('Polls')
    const login_status = useSelector((state) => state.User.userData.USERID)
    const [Result, setResult] = useState([])
    const [SurveysResult, setSurveysResult] = useState([])


    async function getPolls() {
        try {
            let res = await PollsApi.getPolls(login_status)
            console.log(res);
            setResult(res)
        } catch (error) {
            console.log(error);
        }

    }

    async function getSurvey() {
        try {
            let res = await PollsApi.GetSurveys(login_status)
            console.log("reee", res);
            // setResult(res)
            setSurveysResult(res)
        } catch (error) {
            console.log(error);
        }

    }
    async function GetSurveysQustion(item, userId) {
        // Navigation.navigate('SurveysCard', {})
        let prams = userId + '/' + item.SurveyId
        try {
            let res = await PollsApi.GetSurveyQuestions(prams)
            console.log("reerrrre", res);
            Navigation.navigate('SurveysCard', res)
        } catch (error) {
            console.log(error);
        }
    }
    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getPolls()
            getSurvey()
        });
        return unsubscribe;

    }, [navigation]);

    useEffect(() => {
        getPolls()
        getSurvey()
    }, [])
    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header
                back={true}
                pagename={'Polls & Surveys'} />
            <ScrollView>
                {ViewTab == 'Polls' ?
                    <>
                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            marginTop: 20
                        }}>
                            <View style={{
                                backgroundColor: "#333E97",
                                width: 130,
                                paddingVertical: 5,
                                borderTopLeftRadius: 5,
                                borderBottomLeftRadius: 5
                            }}>
                                <Text allowFontScaling={false} style={{
                                    fontWeight: "700",
                                    fontSize: 18,
                                    color: "#fff",
                                    textAlign: "center"
                                }}>Polls</Text>
                            </View>
                            <Pressable onPress={() => setViewTab('Surveys')} style={{
                                backgroundColor: "#fff",
                                width: 130,
                                paddingVertical: 5,
                                elevation: 5,
                                borderTopRightRadius: 5,
                                borderBottomRightRadius: 5
                            }}>
                                <Text allowFontScaling={false} style={{
                                    fontWeight: "700",
                                    fontSize: 18,
                                    color: "#333E97",
                                    textAlign: "center"
                                }}>Surveys</Text>
                            </Pressable>
                        </View>

                        {/* {Result.map((item) => (
                            <>
                                {item.PollTitle == 'Opinion poll' ?
                                    <OpinionPoll item={item} userId={login_status} /> :
                                    <View />
                                }
                            </>
                        ))} */}
                        {
                             Result && Result.length == 0?
                                <Text allowFontScaling={false} style={{
                                    fontWeight: "500",
                                    fontSize: 15,
                                    textAlign:'center',
                                    paddingTop:20
                                }}>
                                     No polls available today
                                </Text>:null
                            }
                        {Result.map((item) => (
                            <>
                                <RadioButton item={item} userId={login_status} />
                            </>
                        ))}



                        {/***** Radio Button */}
                        {/* <RadioButton/> */}





                        {/***** */}

                    </> :
                    <>
                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            marginTop: 20
                        }}>
                            <Pressable onPress={() => setViewTab('Polls')} style={{
                                backgroundColor: "#fff",
                                width: 130,
                                paddingVertical: 5,
                                borderTopLeftRadius: 5,
                                borderBottomLeftRadius: 5,
                                elevation: 5,
                            }}>
                                <Text allowFontScaling={false} style={{
                                    fontWeight: "700",
                                    fontSize: 18,
                                    color: "#333E97",
                                    textAlign: "center"
                                }}>Polls</Text>
                            </Pressable>
                            <Pressable style={{
                                backgroundColor: "#333E97",
                                width: 130,
                                paddingVertical: 5,
                                elevation: 5,
                                borderTopRightRadius: 5,
                                borderBottomRightRadius: 5
                            }}>
                                <Text allowFontScaling={false} style={{
                                    fontWeight: "700",
                                    fontSize: 18,
                                    color: "#fff",
                                    textAlign: "center"
                                }}>Surveys</Text>
                            </Pressable>
                        </View>
                        <View style={{
                            flex: 1,
                            paddingHorizontal: 10,
                            marginTop: 10
                        }}>
                            {
                                SurveysResult.length == 0?
                                <Text allowFontScaling={false} style={{
                                    fontWeight: "500",
                                    fontSize: 15,
                                    textAlign:'center',
                                    paddingTop:20
                                }}>
                                     No Surveys available today
                                </Text>:null
                            }
                            {SurveysResult.map(item => (
                                <Pressable onPress={() => GetSurveysQustion(item, login_status)} style={{
                                    width: '100%',
                                    backgroundColor: "#fff",
                                    elevation: 5,
                                    padding: 10,
                                    borderRadius: 7,
                                    marginTop: 10
                                }}>
                                    <Text allowFontScaling={false} style={{
                                        fontWeight: "500",
                                        fontSize: 15
                                    }}>
                                        {item.SurveyTitle}
                                    </Text>
                                </Pressable>
                            ))}




                        </View>


                    </>}
                <View style={{ height: 20 }} />
            </ScrollView>
        </View>
    )
}

export default Pools

const styles = StyleSheet.create({
    navBox: {
        width: mdscale(120),
        height: mdscale(35),
        backgroundColor: "#ffffff", borderRadius: 10,
        flexDirection: 'row', alignItems: 'center',
        justifyContent: "space-around", borderColor: "#708FFF", borderWidth: 1,
        marginTop: mdscale(20), marginBottom: mdscale(10)
    },
    selectBox: {
        width: mdscale(320),
        //height: mdscale(380),
        backgroundColor: "#FFFFFF", borderRadius: 10,
        marginLeft: 20, marginTop: 20, elevation: 3
    },
    btn: {
        backgroundColor: "#708FFF",
        width: mdscale(300),
        height: mdscale(40),
        // marginHorizontal: mdscale(30),
        alignSelf: "center",
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: vrscale(20),
        elevation: 3,
        marginBottom: 30
    },
    btn_txt: {
        color: "#FFFFFF",
        fontWeight: '900',
        fontSize: mdscale(16), fontFamily: "Poppins-Medium"
    },
})