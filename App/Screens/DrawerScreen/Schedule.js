import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView, TextInput
} from 'react-native'
import React from 'react';
import Header from '../../Components/Header';
import { vrscale, mdscale } from '../../PixelRatio';
const WIDTH = Dimensions.get('window').width;
import { LocaleConfig } from 'react-native-calendars';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';


{/** vectot icon */ }
import Icon from 'react-native-vector-icons/AntDesign'
import Iconu from 'react-native-vector-icons/FontAwesome';

const Schedule = () => {
    LocaleConfig.locales['fr'] = {
        monthNames: [
            'Janvier',
            'Février',
            'Mars',
            'Avril',
            'Mai',
            'Juin',
            'Juillet',
            'Août',
            'Septembre',
            'Octobre',
            'Novembre',
            'Décembre'
        ],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        today: "Aujourd'hui"
    };
    LocaleConfig.defaultLocale = 'fr';

















    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header
                back={true}
                pagename={'Schedule'} />


            <View style={{
                flexDirection: 'row', justifyContent: 'space-around',
                marginVertical: 20
            }}>
                <View style={styles.navBox}>
                    <Icon name="left" size={mdscale(16)} color={"#000000"} />
                    <Text style={{ color: '#000000' }}> May </Text>
                    <Icon name="right" size={mdscale(16)} color={"#000000"} />
                </View>

                <View style={styles.navBox}>
                    <Icon name="left" size={mdscale(16)} color={"#000000"} />
                    <Text style={{ color: '#000000' }}> 2022 </Text>
                    <Icon name="right" size={mdscale(16)} color={"#000000"} />
                </View>
            </View>


            {/** calender */}


            <Calendar
                // Initially visible month. Default = now
                current={'2022-05-17'}
                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                minDate={'2012-05-10'}
                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                maxDate={'2012-05-30'}
                // Handler which gets executed on day press. Default = undefined
                onDayPress={day => {
                    console.log('selected day', day);
                }}
                // Handler which gets executed on day long press. Default = undefined
                onDayLongPress={day => {
                    console.log('selected day', day);
                }}
                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                monthFormat={'yyyy MM'}
                // Handler which gets executed when visible month changes in calendar. Default = undefined
                onMonthChange={month => {
                    console.log('month changed', month);
                }}
                // Hide month navigation arrows. Default = false
                hideArrows={true}
                // Replace default arrows with custom ones (direction can be 'left' or 'right')
                renderArrow={direction => <Arrow />}
                // Do not show days of other months in month page. Default = false
                hideExtraDays={true}
                // If hideArrows = false and hideExtraDays = false do not switch month when tapping on greyed out
                // day from another month that is visible in calendar page. Default = false
                disableMonthChange={true}
                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday
                firstDay={1}
                // Hide day names. Default = false
                hideDayNames={true}
                // Show week numbers to the left. Default = false
                showWeekNumbers={true}
                // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                onPressArrowLeft={subtractMonth => subtractMonth()}
                // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                onPressArrowRight={addMonth => addMonth()}
                // Disable left arrow. Default = false
                disableArrowLeft={true}
                // Disable right arrow. Default = false
                disableArrowRight={true}
                // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
                disableAllTouchEventsForDisabledDays={true}
                // Replace default month and year title with custom one. the function receive a date as parameter
                renderHeader={date => {
                    /*Return JSX*/
                }}
                // Enable the option to swipe between months. Default = false
                enableSwipeMonths={true}
            />






            {/**note */}

            <View style={{
                flex: 1
                // height: '45%',
                //backgroundColor: "black" 
            }}>

                <View style={styles.noteBox}>
                    <View style={styles.bar}></View>
                    <View style={{ color: "#6268EE", width: 10, height: mdscale(42) }}></View>
                    <Text style={{ color: "#000000", paddingTop: 10 }}>Notes can be noted here</Text>
                </View>
                {/*** */}
                <View style={styles.noteBox}>
                    <View style={styles.bar}></View>
                    <Text
                        //numberOfLines={5}
                        style={{ color: "#000000", margin: 10 }}>
                        Notes can be noted here Notes can be noted here Notes {'\n'}
                        can be noted here Notes can be noted here Notes can be noted {'\n'}
                        here noted here Notes can be noted here Notes can be noted here</Text>
                </View>

            </View>



            {/** Typeing Button */}
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: "center",
                // marginTop: vrscale(150),
                //backgroundColor: "green",
                width: '90%',
                marginBottom: 15,
                alignSelf: 'center',
            }}>

                <Icon name="smileo" size={18} color={"#c1c1c1"} style={{ paddingLeft: mdscale(3) }} />
                <Icon name="paperclip" size={18} color={"#c1c1c1"} style={{ paddingLeft: mdscale(5) }} />

                <View style={styles.text_input}>

                    <TextInput
                        multiline={true}
                        placeholder="Message"
                        placeholderTextColor="#c1c1c1"
                        style={{
                            fontWeight: 'bold',
                            color: "#000000",
                            //backgroundColor: "red",
                            width: '86%',
                        }} />

                    <Iconu name="microphone" size={mdscale(16)} color={"#000000"} />
                </View>
            </View>
        </View>
    )
}

export default Schedule

const styles = StyleSheet.create({
    navBox: {
        width: mdscale(140),
        height: mdscale(42),
        backgroundColor: "#ffffff", borderRadius: 10,
        flexDirection: 'row', alignItems: 'center',
        justifyContent: "space-around", elevation: 3
    },

    bar: {
        width: mdscale(5), backgroundColor: "#6268EE", borderRadius: 10,

    },

    noteBox: {
        width: '90%',

        // height: mdscale(55),
        backgroundColor: "#EFF5FF", borderTopRightRadius: 10, borderBottomRightRadius: 10,
        marginTop: 30, elevation: 3, flexDirection: 'row', alignSelf: 'center',
        //paddingLeft: mdscale(1)

    },
    text_input: {
        backgroundColor: '#EDEDED',
        width: '88%',
        marginLeft: 3,
        alignSelf: "center",
        //marginTop: mdscale(250),
        borderRadius: 10,
        //height: mdscale(40),
        elevation: 1,
        flexDirection: 'row',
        alignItems: 'center',
        //paddingStart: mdscale(15),
        justifyContent: "space-evenly",
        //marginBottom: vrscale(10),

    },
})