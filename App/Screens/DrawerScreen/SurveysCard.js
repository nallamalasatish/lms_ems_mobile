import { StyleSheet, Text, View, TextInput, ScrollView, Pressable } from 'react-native'
import React, { useState, useEffect } from 'react'
import Header from '../../Components/Header'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Radio, CheckBox } from 'native-base';
import Navigation from '../../Navigation';
import PollsApi from '../../Service/PollsApi'
import { useSelector, useDispatch } from 'react-redux'
import Toast from 'react-native-simple-toast';


const UselessTextInput = (props) => {
    return (
        <TextInput
            {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
            editable
            maxLength={40}
        />
    );
}

const RadioComponent = ({ table2, itemData, AllAnswer, setAllAnswer }) => {
    const [PollAns, setPollAns] = useState('')
    let ansOp = table2.filter(a => itemData.QuestionId == a.QuestionId)

    useEffect(() => {
        if (PollAns == '') {
            let a = AllAnswer.map(item => {
                if (item.QuestionId == itemData.QuestionId) {
                    return { ...item, optionId: '', ans: '' }
                } else {
                    return item
                }
            })
            setAllAnswer(a)
        } else {
            let a = AllAnswer.map(item => {
                if (item.QuestionId == itemData.QuestionId) {
                    return { ...item, optionId: PollAns, ans: true }
                } else {
                    return item
                }
            })
            setAllAnswer(a)
        }
    }, [PollAns])

    return (
        <>
            <View style={{
                flexDirection: "row",
                marginTop: 20
            }}>


                <Text allowFontScaling={false} style={{
                    fontWeight: "500",
                    fontSize: 15,
                    color: "red"
                }}>*</Text>
                <Text allowFontScaling={false} style={{
                    fontWeight: "500",
                    fontSize: 15,
                    color: "#3C3C3C",
                    marginLeft: 5
                }}>
                    {itemData.SurveyQuestion}
                </Text>
            </View>
            {ansOp.map(data => (
                <View style={{
                    marginLeft: 10,
                    marginTop: 10,
                    flexDirection: 'row',
                    marginRight: 20
                }}>

                    <Radio color='#656565' selected={PollAns == data.OptionId ? true : false}
                        onPress={() => setPollAns(data.OptionId)} />
                    <Text allowFontScaling={false} style={{
                        color: "#656565",
                        fontSize: 14,
                        paddingLeft: 4,
                        fontWeight: "500"
                    }}>{data.OptionText}</Text>

                </View>
            ))}

        </>
    )
}
const ChakBoxComponent = ({ table2, itemData, AllAnswer, setAllAnswer }) => {
    const [PollAns, setPollAns] = useState([])
    let ansOp = table2.filter(a => itemData.QuestionId == a.QuestionId)

    function AddCheckBox(OptionId) {
        if (PollAns.some(a => a == OptionId)) {
            setPollAns(s => s.filter(a=> a!==OptionId))
        }else {
        setPollAns(s => [...s, OptionId])
        }
    }

    useEffect(() => {
        if (PollAns.length == 0) {
            let a = AllAnswer.map(item => {
                if (item.QuestionId == itemData.QuestionId) {
                    return { ...item, optionId: '', ans: '' }
                } else {
                    return item
                }
            })
            setAllAnswer(a)
        } else {
            let b = AllAnswer.filter(f => f.QuestionId !== itemData.QuestionId)
            let a = PollAns.map(item => {
                  return  { QuestionId: itemData.QuestionId, optionId: item, ans: true,answerText:"" }
            })
            setAllAnswer([...a,...b])
        }
    }, [PollAns])

    return (
        <>
            <View style={{
                flexDirection: "row",
                marginTop: 20
            }}>


                <Text allowFontScaling={false} style={{
                    fontWeight: "500",
                    fontSize: 15,
                    color: "red"
                }}>*</Text>
                <Text allowFontScaling={false} style={{
                    fontWeight: "500",
                    fontSize: 15,
                    color: "#3C3C3C",
                    marginLeft: 5
                }}>
                    {itemData.SurveyQuestion}
                </Text>
            </View>
            {/* <Checkbox.Group  onChange={setGroupValues} value={groupValues} accessibilityLabel="choose numbers"> */}
                {ansOp.map(data => (
                    <View style={{
                        marginLeft: 10,
                        marginTop: 10,
                        flexDirection: 'row',
                        marginRight: 20
                    }}>
                          <CheckBox  checked={PollAns.some(a => a == data.OptionId) ? true : false}
                        onPress={() =>AddCheckBox(data.OptionId) }  />
                        <Text allowFontScaling={false} style={{
                            color: "#656565",
                            fontSize: 14,
                            paddingLeft: 4,
                            fontWeight: "500",
                            marginLeft:15
                        }}>{data.OptionText}</Text>

                    </View>
                ))}
            {/* </Checkbox.Group> */}

        </>
    )
}

const TextBoxComponent = ({ table2, itemData, AllAnswer, setAllAnswer }) => {
    const [value, onChangeText] = React.useState('Text box');

    useEffect(() => {
        // console.log(AllAnswer);
        if (value == 'Text box') {
            let a = AllAnswer.map(item => {
                if (item.QuestionId == itemData.QuestionId) {
                    return { ...item, answerText: '', ans: '' }
                } else {
                    return item
                }
            })
            setAllAnswer(a)
        } else {
            let a = AllAnswer.map(item => {
                if (item.QuestionId == itemData.QuestionId) {
                    return { ...item, answerText: value, ans: true }
                } else {
                    return item
                }
            })
            setAllAnswer(a)
        }
    }, [value])
    return (
        <>
            <View style={{
                flexDirection: "row",
                marginTop: 20
            }}>
                <Text allowFontScaling={false} style={{
                    fontWeight: "500",
                    fontSize: 15,
                    color: "red"
                }}>*</Text>
                <Text allowFontScaling={false} style={{
                    fontWeight: "500",
                    fontSize: 15,
                    color: "#3C3C3C"
                }}>
                      {itemData.SurveyQuestion}
                </Text>
            </View>
            <View style={{
                borderWidth: 1,
                borderColor: "#E3E3E3",
                borderRadius: 10,
                marginTop: 10
            }}>
                <UselessTextInput
                    multiline
                    numberOfLines={4}
                    onChangeText={text => onChangeText(text)}
                    value={value}
                    style={{ padding: 10, color: "#AAAAAA" }}
                />

            </View>
        </>
    )
}

const SurveysCard = ({ route }) => {
    const login_status = useSelector((state) => state.User.userData.USERID)
    const [value, onChangeText] = React.useState('Text box');
    const [AllAnswer, setAllAnswer] = useState([])
    // console.log("pppp", route.params);
    let data = route.params

    async function setAnswer() {
        console.log(AllAnswer)
       console.log('aaaa',{
            "SurveyId": data.Table[0].SurveyId,
            "ServeyOptions": [
                ...AllAnswer
            ],
            "UserId": login_status
        })
        if (AllAnswer.every(a => a.ans == true)) {
            let abc = AllAnswer.map(a =>{
                return {  
                    "QuestionId":a.QuestionId,
                    "answerText":a.answerText,
                    "optionId":a.optionId
                 }
            })
            try {
                let res = await PollsApi.SetSurveys({
                    "SurveyId": data.Table[0].SurveyId,
                    "ServeyOptions": [
                        ...abc
                    ],
                    "UserId":login_status
                })
               
                console.log("aaaaaa", res);
                Navigation.back()
            } catch (error) {
                console.log(error);
            }
        } else {
            Toast.show('please Answer All the Questions', Toast.SHORT,['UIAlertController']);
            console.log("please Answer All the Questions");
        }

    }
    useEffect(() => {
        let a =  data.Table1.map((item) => {
            if (item.QuesType == 'fbd5afa6') {
                return { QuestionId: item.QuestionId, optionId: "", ans: "",answerText:"" }
            } else if (item.QuesType == 'dd57541d') {
                return { QuestionId: item.QuestionId, optionId: "", ans: "",answerText:"" }
            }
             else if (item.QuesType == '797537f0') {
                return { QuestionId: item.QuestionId, answerText: "", ans: "",optionId: "0" }
            }
             else if (item.QuesType == '39080bf1') {
                return { QuestionId: item.QuestionId, optionId: "", ans: "",answerText:"" }
            }else{
                return { QuestionId: item.QuestionId, optionId: "", ans: "",s:"not find" }
            }
        })
        console.log(data);
        setAllAnswer(a )

    }, [])

    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header back={false} pagename={'Polls & Surveys'} />
            <ScrollView style={{
                flex: 1,
                paddingHorizontal: 10
            }}>
                <Pressable onPress={() => Navigation.back()} style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 10
                }}>
                    <AntDesign name='arrowleft' size={30} color='#333E97' />
                    <Text allowFontScaling={false} style={{
                        marginLeft: 10,
                        fontWeight: '500',
                        fontSize: 17,
                        color: "#333E97"
                    }}>back to Surveys</Text>
                </Pressable>
                <View style={{
                    width: "100%",
                    borderRadius: 7,
                    backgroundColor: "#fff",
                    elevation: 5,
                    marginTop: 20,
                    padding: 10
                }}>
                    <Text allowFontScaling={false} style={{
                        color: "#3C3C3C",
                        fontWeight: "700",
                        fontSize: 17
                    }}>
                        {data.Table[0].FinalWords}
                    </Text>
                    {data.Table1.map(item => (
                        <>
                            {item.QuesType == 'fbd5afa6' ?
                                <RadioComponent
                                    table2={data.Table2}
                                    itemData={item}
                                    AllAnswer={AllAnswer}
                                    setAllAnswer={setAllAnswer}
                                />
                                : item.QuesType == 'dd57541d' ?
                                    <RadioComponent
                                        table2={data.Table2}
                                        itemData={item}
                                        AllAnswer={AllAnswer}
                                        setAllAnswer={setAllAnswer}
                                    />
                                    : item.QuesType == '797537f0' ?
                                        <TextBoxComponent
                                            table2={data.Table2}
                                            itemData={item}
                                            AllAnswer={AllAnswer}
                                            setAllAnswer={setAllAnswer}
                                        />
                                        : item.QuesType == '39080bf1' ?
                                        <ChakBoxComponent
                                            table2={data.Table2}
                                            itemData={item}
                                            AllAnswer={AllAnswer}
                                            setAllAnswer={setAllAnswer}
                                        />
                                        : <View />}

                        </>
                    ))}
                    {/* <View style={{
                        flexDirection: "row",
                        marginTop: 20
                    }}>


                        <Text allowFontScaling={false} style={{
                            fontWeight: "500",
                            fontSize: 15,
                            color: "red"
                        }}>*</Text>
                        <Text allowFontScaling={false} style={{
                            fontWeight: "500",
                            fontSize: 15,
                            color: "#3C3C3C"
                        }}>
                            Question tittle for the Surveys section
                        </Text>
                    </View>
                    <View style={{
                        borderWidth: 1,
                        borderColor: "#E3E3E3",
                        borderRadius: 10,
                        marginTop: 10
                    }}>
                        <UselessTextInput
                            multiline
                            numberOfLines={4}
                            onChangeText={text => onChangeText(text)}
                            value={value}
                            style={{ padding: 10, color: "#AAAAAA" }}
                        />

                    </View> */}
                    {/* <View style={{
                        flexDirection: "row",
                        marginTop: 20
                    }}>


                        <Text allowFontScaling={false} style={{
                            fontWeight: "500",
                            fontSize: 15,
                            color: "red"
                        }}>*</Text>
                        <Text allowFontScaling={false} style={{
                            fontWeight: "500",
                            fontSize: 15,
                            color: "#3C3C3C",
                            marginLeft:5
                        }}>
                            Question tittle for the Surveys section
                        </Text>
                    </View>
                    <View style={{
                        marginLeft: 10,
                        marginTop: 10,
                        flexDirection: 'row',
                        marginRight: 20
                    }}>

                        <Radio selected={false} />
                        <Text allowFontScaling={false} style={{
                            color: "#656565",
                            fontSize: 14,
                            paddingLeft: 4,
                            fontWeight: "500"
                        }}>Option 1</Text>

                    </View>
                    <View style={{
                        marginLeft: 10,
                        marginTop: 10,
                        flexDirection: 'row',
                        marginRight: 20
                    }}>

                        <Radio selected={false} />
                        <Text allowFontScaling={false} style={{
                            color: "#656565",
                            fontSize: 14,
                            paddingLeft: 4,
                            fontWeight: "500"
                        }}>Option 2 with extra words</Text>

                    </View>
                    <View style={{
                        marginLeft: 10,
                        marginTop: 10,
                        flexDirection: 'row',
                        marginRight: 20
                    }}>

                        <Radio selected={true} />
                        <Text allowFontScaling={false} style={{
                            color: "#656565",
                            fontSize: 14,
                            paddingLeft: 4,
                            fontWeight: "500"
                        }}>Option 3 with extra words</Text>

                    </View> */}

                    {/* text box */}
                    {/* <View style={{
                        flexDirection: "row",
                        marginTop: 20
                    }}>


                        <Text allowFontScaling={false} style={{
                            fontWeight: "500",
                            fontSize: 15,
                            color: "red"
                        }}>*</Text>
                        <Text allowFontScaling={false} style={{
                            fontWeight: "500",
                            fontSize: 15,
                            color: "#3C3C3C"
                        }}>
                            Question tittle for the Surveys section
                        </Text>
                    </View>
                    <View style={{
                        borderWidth: 1,
                        borderColor: "#E3E3E3",
                        borderRadius: 10,
                        marginTop: 10
                    }}>
                        <UselessTextInput
                            multiline
                            numberOfLines={4}
                            onChangeText={text => onChangeText(text)}
                            value={value}
                            style={{ padding: 10, color: "#AAAAAA" }}
                        />

                    </View> */}

                    <Pressable onPress={setAnswer} style={{
                        marginTop: 20,
                        width: '100%',
                        backgroundColor: "#708FFF",
                        borderRadius: 5,
                        padding: 10
                    }}>
                        <Text allowFontScaling={false} style={{
                            fontWeight: "700",
                            color: "#fff",
                            fontSize: 17,
                            textAlign: "center"
                        }}>Submit</Text>
                    </Pressable>
                </View>
            </ScrollView >
        </View>
    )
}

export default SurveysCard

const styles = StyleSheet.create({})