//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, PixelRatio, Image } from 'react-native';
import { Layout } from 'react-native-reanimated';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
import { getPixelSizeForLayoutSize } from 'react-native/Libraries/Utilities/PixelRatio';

const { width, height } = Dimensions.get('window')
// const widthToDo = (number)=>{
//     let givenWidth = typeof number === 'number' ? number : parseFloat(number);
//     return PixelRatio.roundToNearestPixel({layoutSize :(width * givenWidth)/100})
// } 
// const heightToDo = (number)=>{
//     let givenHeight = typeof number === 'number' ? number : parseFloat(number);
//     return PixelRatio.roundToNearestPixel( layoutSize : (height * givenHeight)/100)
// } 
// create a component

// const wp = (number) => {
//     let givenWidth = typeof number === "number" ? number : parseFloat(number);
//     // return PixelRatio.roundToNearestPixel((width * givenWidth) / 100);
//     return number
// };

// const hp = (number) => {
//     let givenHeight = typeof number === "number" ? number : parseFloat(number);
//     // return PixelRatio.roundToNearestPixel((height * givenHeight) / 100);
//     return number
// };

const dp = (px: number) => {
    return px / PixelRatio.get();
};

// sp(54) converts 54px (px as in your mockup design) to sp
const sp = (px: number) => {
    return px / (PixelRatio.getFontScale() * PixelRatio.get());
};

const TestPage = () => {
    // const size = PixelRatio.getPixelSizeForLayoutSize(50);
    return (
        <View style={styles.container}>
            <Text>Alart</Text>

            <View style={{ width: sp(700), paddingVertical: 20, display: "flex", alignItems: "center", backgroundColor: "#fff", borderRadius: 20, elevation: 5 }}>
                <Image source={require("../Assets/Images/success.png")} style={{ width: sp(400), height: sp(400) }} />
                <Text style={{ color: "#B6E4B2", marginTop: 10 }}>Transaction Successful</Text>
                <Pressable style={{ width: "90%", backgroundColor: "#6DC965", borderRadius: 10, marginTop: 10 }}>
                    <Text style={{ color: "#fff", textAlign: "center", fontSize: sp(60), fontWeight: "500", paddingVertical: 7 }}>Proceed</Text>
                </Pressable>
            </View>
            <View style={{ width: sp(700), paddingVertical: 20, display: "flex", alignItems: "center", backgroundColor: "#fff", borderRadius: 20, elevation: 5 }}>
                <Image source={require("../Assets/Images/err.png")} style={{ width: sp(400), height: sp(400) }} />
                <Text style={{ color: "#F29797", marginTop: 10 }}>Transaction Failed</Text>
                <Pressable style={{ width: "90%", backgroundColor: "#E53030", borderRadius: 10, marginTop: 10 }}>
                    <Text style={{ color: "#fff", textAlign: "center", fontSize: sp(60), fontWeight: "500", paddingVertical: 7 }}>Proceed</Text>
                </Pressable>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default TestPage;
