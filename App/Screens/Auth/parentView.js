import  React from 'react';

import { Formik } from 'formik';

import { ParentSignInFormInitialValues, ParentSignInFormValidator } from './LoginHelper';
import ParentLoginForm from './ParentLoginForm';

const ParentView = (props) => {
    const {onParentLogin, intl, navigation} = props;

    return  <Formik
                initialValues={ParentSignInFormInitialValues(props)}
                validationSchema={ParentSignInFormValidator(props)}
                onSubmit={values => onParentLogin(values)}>
                {({values, handleChange, setFieldValue, errors, touched, setFieldTouched, isValid, handleSubmit}) => (
                    <ParentLoginForm
                        values={values}
                        handleChange={handleChange}
                        setFieldValue={setFieldValue}
                        errors={errors}
                        touched={touched}
                        setFieldTouched={setFieldTouched}
                        isValid={isValid}
                        handleSubmit={handleSubmit}
                        {...props}/> 
            )}
            </Formik>   
}

export default ParentView;