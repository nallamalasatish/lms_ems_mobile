//import liraries
import React, { useState } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, Pressable, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import AuthService from '../../Service/Auth'
import { useSelector, useDispatch } from 'react-redux'
import { setuser, logout } from '../../Redux/reducer/User'
import Toast from 'react-native-simple-toast';
import OtpView from '../../Components/components/OtpView/OtpView';
import { API_STATUS, BLACK_COLOR, FONT_BOLD, LOGIN_BTN_COLOR, SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT, WHITE_COLOR } from '../../Utils/utils/AppConst';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AppOkAlert from '../../Utils/utils/AlertHelper';
import api from '../../Service/api'
import { signInIntlProvider } from './LoginHelper';
import ParentView from './parentView';

function CheckPassword(inputtxt) {
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return inputtxt.match(passw)
}

const validateEmail = (email) => {
    const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    return expression.test(String(email).toLowerCase())
}
// create a component
const Login = (props) => {
    const login_status = useSelector((state) => state.User.login_status)
    const dispatch = useDispatch()
    const intl = signInIntlProvider(props);
    const [Email, setEmail] = useState("")
    const [Password, setPassword] = useState("")
    const [isStudent, setStudent] = useState(false)
    const [loading, setLoading] = useState(false);
    const [parentData, setParentData] = useState(undefined);
    const [otpVisible, setOtpVisible] = useState(false);
    const [otpError, setOtpError] = useState(undefined);

    const SubmitValue = async () => {
        if (!validateEmail(Email)) {
            console.log('Please Enter a Valid Email');
            Toast.show('Please Enter A Valid Email', Toast.SHORT, ['UIAlertController']);
        } else if (Password == "") {
            console.log('Please Enter a Valid Password');
            Toast.show('Please Enter A Valid Password', Toast.SHORT, ['UIAlertController']);
        } else {
            let LoginData = { userName: Email, password: Password }

            let result = await AuthService.login(LoginData)
            console.log(result);
            if (result && result.STATUS == true) {
                console.log("login succes");
                dispatch(setuser(result))
                AuthService.setAccount(result)
                Toast.show('Login Success', Toast.SHORT, ['UIAlertController']);
            } else if (result == "Invalid Username or password.") {
                Toast.show('Invalid Username or password.', Toast.SHORT, ['UIAlertController']);
            }

        }
    }

    const onParentLogin = async (values) => {
        setLoading(true);
        const res = await api.user.parentLogin(intl, {
            UserName: values.UserName,
            MobileNumber: values.MobileNumber,
        });
        console.log('res =====> ', res);
        setLoading(false);
        if (res && res.status == API_STATUS.OK && isNumber(res.data)) {
            const userId = res.data;
            setParentData({
                ...values,
                userId: userId,
            });
            setOtpVisible(true);
        } else {
            AppOkAlert(res.message, () => { });
        }
    };

    const onSendOtp = () => {
        onParentLogin(parentData);
    };

    const onCloseOtpView = () => {
        setOtpVisible(false);
    };

    const onVerifyOtp = async (values) => {
        setLoading(true);
        const res = await api.user.otpVerification(intl, {
            Otp: values.otp,
            UserId: parentData.userId,
        });
        console.log('res =====> ', res);
        setLoading(false);
        if (res && res.status == API_STATUS.OK && res.data) {
            let userInfo = res.data;
            userInfo = {
                ...userInfo,
                login_type: LOGIN_TYPE.PARENT,
            };
            setOtpVisible(false);
            await saveUserProfileInfo(userInfo);
            await saveUserId(userInfo.USERID);
            navigation.reset({
                index: 0,
                routes: [{ name: MAIN_ROUTE }],
            });
        } else {
            AppOkAlert(res.message, () => { });
            setOtpError(res.message);
        }
    };

    return (
        <View style={styles.container}>
            <ImageBackground
                source={require("../../Assets/Images/bg-image.png")}
                style={{
                    flex: 1,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                }}>
                    <Loading loading={loading}></Loading>
                <View style={{ flex: 1 }}>
                    <View
                        style={{

                            height: RFValue(45, STANDARD_SCREEN_HEIGHT),
                            flexDirection: 'row',
                            marginRight: 5,
                            marginTop: RFValue(hp('33%'), STANDARD_SCREEN_HEIGHT),
                            backgroundColor: WHITE_COLOR,
                            borderTopRightRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                            borderTopLeftRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                            borderBottomRightRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                            borderBottomLeftRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                //flex:1,
                                width: wp('25%'),
                                backgroundColor: !isStudent ? "#06bde6" : WHITE_COLOR,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: RFValue(10, STANDARD_SCREEN_HEIGHT),
                                borderTopLeftRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                                borderBottomRightRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                                borderBottomLeftRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                                borderTopRightRadius: isStudent
                                    ? RFValue(10, STANDARD_SCREEN_HEIGHT)
                                    : 0,
                            }}
                            onPress={() => setStudent(true)}>
                            <Text
                                style={{
                                    fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                                    color: !isStudent ? WHITE_COLOR : BLACK_COLOR,
                                    fontFamily: FONT_BOLD,
                                    fontWeight: '900',
                                }}>
                                {'student'}/{'Trainer'}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{
                                //flex:1,
                                width: wp('25%'),
                                backgroundColor: isStudent ? "#06bde6" : WHITE_COLOR,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingHorizontal: RFValue(30, STANDARD_SCREEN_HEIGHT),
                                borderTopRightRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                                borderBottomRightRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                                borderBottomLeftRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                            }}
                            onPress={() => setStudent(false)}>
                            <Text
                                style={{
                                    fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                                    color: isStudent ? WHITE_COLOR : BLACK_COLOR,
                                    fontFamily: FONT_BOLD,
                                    fontWeight: '900',
                                }}>
                                {'parent'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {parentData && (
                    <View
                        style={{
                            position: 'absolute',
                        }}>
                        <OtpView
                            message={`${intl.otpSendMessage} ${parentData.MobileNumber}`}
                            visible={otpVisible}
                            mobile={parentData.MobileNumber}
                            sendOtp={(mobile) => onSendOtp()}
                            verifyOtp={(parentData) => onVerifyOtp(parentData)}
                            onCloseOtpView={() => onCloseOtpView()}
                            otpError={otpError}
                        />
                    </View>
                )}
                {isStudent ? (<View style={{
                    marginBottom: 100,
                    width: 360,
                    maxWidth: "90%",
                    backgroundColor: "#fff",
                    borderRadius: 30,
                    elevation: 8,
                    borderWidth: 0,
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                    padding: 10
                }}>
                    <Image source={require("../../Assets/Images/logo-sam.png")} style={{ marginTop: 20 }} />
                    <Text style={{ fontSize: 22, color: "#8c8c8c", fontWeight: "700", marginTop: 10 }}>
                        Welcome to Samvaad
                    </Text>
                    <Text style={{ fontSize: 15, color: "#8c8c8c", fontWeight: "400" }}>
                        Learning Management System
                    </Text>
                    <TextInput
                        placeholder='Email'
                        placeholderTextColor="#8c8c8c"
                        value={Email}
                        onChangeText={t => setEmail(t)}
                        style={{
                            width: "90%",
                            borderBottomColor: "#555555",
                            borderBottomWidth: 2,
                            paddingBottom: 5,
                            fontSize: 17,
                            marginTop: 20,
                            color: "#8c8c8c"
                        }} />
                    <View style={{
                        width: "90%", marginTop: 10, display: "flex", flexDirection: "row", alignItems: "center",
                        borderBottomColor: "#555555",
                        borderBottomWidth: 2,

                    }}>
                        <TextInput
                            placeholder='Password'
                            placeholderTextColor="#8c8c8c"
                            value={Password}
                            onChangeText={t => setPassword(t)}
                            style={{
                                flex: 1,
                                paddingBottom: 5,
                                fontSize: 17,
                                color: "#8c8c8c"
                            }} />
                        <FontAwesome5 name='eye' color="#8c8c8c" size={20} style={{ paddingBottom: 0, marginRight: 5, marginTop: 12 }} />
                    </View>
                    <Pressable onPress={SubmitValue} style={{
                        width: "90%", marginTop: 40, marginBottom: 20, borderRadius: 40,
                        backgroundColor: "#06bde6", paddingVertical: 10

                    }}>
                        <Text style={{ color: "#fff", fontWeight: "500", fontSize: 20, textAlign: "center" }}>LOGIN</Text>
                    </Pressable>
                </View>) : (
                    <ParentView onParentLogin={onParentLogin} intl={intl} {...props} />
                )}
                {/* <View style={{
                marginBottom: 100,
                width: 360,
                maxWidth: "90%",
                backgroundColor: "#fff",
                borderRadius: 30,
                elevation: 8,
                borderWidth: 0,
                display: "flex",
                alignItems: "center",
                flexDirection: "column",
                padding: 10
            }}>
                <Image source={require("../../Assets/Images/logo-sam.png")} style={{ marginTop: 20 }} />
                <Text style={{ fontSize: 22, color: "#8c8c8c", fontWeight: "700", marginTop: 10 }}>
                    Welcome to Samvaad
                </Text>
                <Text style={{ fontSize: 15, color: "#8c8c8c", fontWeight: "400" }}>
                    Learning Management System
                </Text>
                <TextInput
                    placeholder='Email'
                    placeholderTextColor="#8c8c8c"
                    value={Email}
                    onChangeText={t => setEmail(t)}
                    style={{
                        width: "90%",
                        borderBottomColor: "#555555",
                        borderBottomWidth: 2,
                        paddingBottom: 5,
                        fontSize: 17,
                        marginTop: 20,
                        color: "#8c8c8c"
                    }} />
                <View style={{
                    width: "90%", marginTop: 10, display: "flex", flexDirection: "row", alignItems: "center",
                    borderBottomColor: "#555555",
                    borderBottomWidth: 2,

                }}>
                    <TextInput
                        placeholder='Password'
                        placeholderTextColor="#8c8c8c"
                        value={Password}
                        onChangeText={t => setPassword(t)}
                        style={{
                            flex: 1,
                            paddingBottom: 5,
                            fontSize: 17,
                            color: "#8c8c8c"
                        }} />
                    <FontAwesome5 name='eye' color="#8c8c8c" size={20} style={{ paddingBottom: 0, marginRight: 5, marginTop: 12 }} />
                </View>
                <Pressable onPress={SubmitValue} style={{
                    width: "90%", marginTop: 40, marginBottom: 20, borderRadius: 40,
                    backgroundColor: "#06bde6", paddingVertical: 10

                }}>
                    <Text style={{ color: "#fff", fontWeight: "500", fontSize: 20, textAlign: "center" }}>LOGIN</Text>
                </Pressable>
            </View> */}
            </ImageBackground>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,


    },
});

//make this component available to the app
export default Login;