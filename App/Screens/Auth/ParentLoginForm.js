import  React from 'react';
 import {View, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView, 
        Image, Keyboard, TouchableWithoutFeedback,} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import { THEME_COLOR, SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT, WHITE_COLOR, LOGIN_BTN_COLOR, BLACK_COLOR, LOGIN_BTN_COLOR1, FONT_MEDIUM } from '../../Utils/utils/AppConst';
import AppTextField from '../../Components/components/AppTextInput/AppTextInput';
import { RFValue } from 'react-native-responsive-fontsize';
import AppButton from '../../Components/components/AppButton/AppButton';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import commonStyles from '../../Utils/styles/GlobalStyles';

const ParentLoginForm = (props) => {
    const {values, handleChange, setFieldValue, errors, touched, setFieldTouched, isValid, handleSubmit, intl} = props;
    const [value, onChangeText] = React.useState('Useless Placeholder');
    const navigation = useNavigation()

    
    return  ( <ScrollView nestedScrollEnabled={true}><View style={[ 
                {...commonStyles.card}, 
                {...commonStyles.shadowEffect},
                {
                    width: wp('90%'),  
                    justifyContent:'center',
                    //marginLeft: wp('5%'),
                    alignSelf:'center',
                    paddingVertical: RFValue(20, STANDARD_SCREEN_HEIGHT),
                    borderRadius: 0,
                    borderTopEndRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                    borderBottomEndRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                    borderBottomStartRadius:  RFValue(10, STANDARD_SCREEN_HEIGHT),
                    marginBottom: RFValue(40, STANDARD_SCREEN_HEIGHT)
                }, 
                ]}>
                <AppTextField
                        placeHolder={intl.usernameEmailPlaceholder}
                        value={values.UserName.trim()}
                        //value={"sheshubompally@gmail.com"}
                        changeText={handleChange('UserName')}
                        onBlur={() => setFieldTouched('UserName')}
                        autoCapitalize={"none"}
                />
                {touched.UserName && errors.UserName &&
                    <Text style={{marginLeft: 24/**ICON Size*/, fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT), color: 'red' }}>* {errors.UserName}</Text>
                }

                <AppTextField
                        placeHolder={intl.mobileNumberPlaceholder}
                        value={values.MobileNumber.replace(/[^0-9]/g, '')}
                        //value={"9997011740"}
                        changeText={handleChange('MobileNumber')}
                        onBlur={() => setFieldTouched('MobileNumber')}
                        keyboardType={"numeric"}
                        maxLength={10}
                        autoCapitalize={"none"}/>
                        {touched.MobileNumber && errors.MobileNumber &&
                                    <Text style={{marginLeft: 24/**ICON Size*/, fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT), color: 'red' }}>* {errors.MobileNumber}</Text>
                        }
                    <AppButton
                        colors={[LOGIN_BTN_COLOR, LOGIN_BTN_COLOR1]}
                        containerStyle={{
                           // backgroundColor: LOGIN_BTN_COLOR,
                           marginTop: RFValue(10, STANDARD_SCREEN_HEIGHT),
                           width: wp('80%'),
                           paddingLeft: RFValue(20, STANDARD_SCREEN_HEIGHT),
                           paddingRight: RFValue(20, STANDARD_SCREEN_HEIGHT),
                           marginLeft: wp('5%'),
                        }}
                        textStyle={{
                            color: WHITE_COLOR,
                        }}
                        onPress={() => {
                            handleSubmit();
                        }}
                        label={intl.generateOTPPlaceholder}/>   
                
                {touched.otp && errors.otp &&
                        <Text style={{marginLeft: 24/**ICON Size*/, fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT), color: 'red' }}>* {errors.otp}</Text>
                }
                <View style={styles.forgotPasswordContainer}>
                    <TouchableOpacity style={styles.forgotPasswordView}>
                        <Text style={{color: LOGIN_BTN_COLOR, 
                            fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                            fontFamily: FONT_MEDIUM
                            }}>{intl.forgotPassword}</Text>
                    </TouchableOpacity>
                </View>
                
            </View></ScrollView>)
}

const styles = StyleSheet.create({
    forgotPasswordContainer : {
        justifyContent:'flex-end',
        alignItems:'flex-end',
        marginRight: RFValue(30, STANDARD_SCREEN_HEIGHT),
        marginTop: RFValue(10, STANDARD_SCREEN_HEIGHT)

    },
    forgotPasswordView: {
        justifyContent:'flex-end',
        alignItems:'flex-end',
        padding: RFValue(10, STANDARD_SCREEN_HEIGHT)
    }
})

export default ParentLoginForm;