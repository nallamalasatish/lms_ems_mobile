import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView, Button
} from 'react-native'
import React, { useEffect, useState } from 'react'

import { vrscale, mdscale } from '../../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
import Navigation from '../../Navigation/index';
import Header from '../../Components/Header';
// import { Picker } from '@react-native-picker/picker';
import Modal from "react-native-modal";
import { useSelector, useDispatch } from 'react-redux'
import AssesmentApi from '../../Service/AssesmentApi'

{/** vectot icon */ }
import Palet from 'react-native-vector-icons/AntDesign'
import moment from 'moment';


function currentTime() {
    let date = new Date(); 
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    let session = "AM";
  
      
    if(hh > 12){
        session = "PM";
     }
  
     hh = (hh < 10) ? "0" + hh : hh;
     mm = (mm < 10) ? "0" + mm : mm;
     ss = (ss < 10) ? "0" + ss : ss;
      
     let time = hh + "" + mm + "" + ss ;
  return time
    // document.getElementById("clock").innerText = time; 
    // var t = setTimeout(function(){ currentTime() }, 1000); 
  
  }
const Assesment = ({navigation}) => {
    const loginId = useSelector((state) => state.User.userData)
    const [ShowTest, setShowTest] = useState(false)
    const [isModalVisible, setModalVisible] = useState(false);
    const [isModalVisible1, setModalVisible1] = useState(false);

    const [selectedCourses, setSelectedCourses] = useState('');
    const [SelectcourseName, setSelectcourseName] = useState('')
    const [CoursesData, setCoursesData] = useState([])


    const [ShowCourseSchedule, setShowCourseSchedule] = useState(false)
    const [CourseScheduleData, setCourseScheduleData] = useState([])
    const [selectedShowCourseSchedule, setSelectedShowCourseSchedule] = useState('')

    const [ShowListOfAssessments, setShowListOfAssessments] = useState(false)
    const [ListOfAssessmentsData, setListOfAssessmentsData] = useState([])



    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    const toggleModal1 = () => {
        setModalVisible1(!isModalVisible1);
    };
    async function getCourses() {
        try {
            let result = await AssesmentApi.CoursesGetCourses(loginId.USERID + '/' + loginId.ROLEID)
            // setData(result)
            console.log(result);
            setCoursesData(result)
        } catch (error) {
            console.log(error);
        }

    }
    async function getCourseSchedule() {
        try {
            let result = await AssesmentApi.GetCourseSchedule({ "CourseId": selectedCourses, "UserId": loginId.USERID, "RoleId": loginId.ROLEID })
            // setData(result)
            console.log(result);
            setCourseScheduleData(result)
            setShowCourseSchedule(true)
        } catch (error) {
            console.log(error);
        }

    }
    async function getListOfAssessments( aaaaa) {
        try {
            let result = await AssesmentApi.GetListOfAssessments({ "CourseScheduleId": aaaaa, "CourseId": selectedCourses, "UserId": loginId.USERID })
            // setData(result)
            console.log('list',result);
            setListOfAssessmentsData(result)
            setShowListOfAssessments(true)
            // getShowDate(result)
        } catch (error) {
            console.log(error);
        }

    }
   
    // function getShowDate(result) {
    //     let date =  new Date()
    //     console.log(moment(date).format('MMMM Do YYYY, h:mm:ss a'));
    //     let result = text.replace("Microsoft", "W3Schools");
    //     console.log(currentTime());
    //     console.log(result.SA_START_TIME);
    // }
    useEffect(() => {
        getCourses()
        // Navigation.navigate('AssesmentTest')
    }, [])
    useEffect(() => {
        if (selectedCourses == '') {

        } else {
            getCourseSchedule()
        }
    }, [selectedCourses])
    // useEffect(() => {
    //     if (selectedShowCourseSchedule == '') {

    //     } else {
    //         getListOfAssessments()
    //     }
    // }, [selectedShowCourseSchedule])
    React.useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            setShowListOfAssessments(false)
            setShowCourseSchedule(false)
            setCourseScheduleData([])
            setSelectedCourses([])
            setListOfAssessmentsData([])
            setSelectcourseName('')
            getCourses()
        });
        return unsubscribe;

    }, [navigation]);
    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setShowListOfAssessments(false)
            setShowCourseSchedule(false)
            setCourseScheduleData([])
            setSelectedCourses([])
            setListOfAssessmentsData([])
            setSelectcourseName('')
            getCourses()
        });
        return unsubscribe;

    }, [navigation]);

    useEffect(() => {

    }, [])
    const ListOfAssessmentsComponent =({item})=>{
        const [ShowTestBtn, setShowTestBtn] = useState(false)
           function getShowDate() {
        let date =  new Date(item.SA_DATE.slice(0,10)).toDateString()
        let cdate =  new Date().toDateString()
        // console.log(date.replace("/", "-").replace("/", "-"));
        // console.log(moment(date).calendar());
        // console.log(`${date}`.slice(0,10));
        // console.log(date);
        // let result = text.replace("Microsoft", "W3Schools");
        // console.log(currentTime());  
        // if (date.slice) {
            
        // }

        if (date == cdate) {
            if (currentTime()>item.SA_START_TIME.replace(":", "").replace(":", "") &&    currentTime()<item.SA_END_TIME.replace(":", "").replace(":", "")) {
                // console.log("match");
                setShowTestBtn(true)
            }else{
                // console.log("no match");
            }  
        }else{
            console.log('nomatch');
        }
       
       
    }
    useEffect(() => {
        getShowDate()
    console.log(item);
    }, [])
    
        return(
            <View style={{
                //width: mdscale(330),
                // height: mdscale(154),
                backgroundColor: "#ECF4FF",
                marginHorizontal: mdscale(10),
                marginTop: 20,
                borderRadius: 10, elevation: 3
            }}>                
                <View style={{ paddingHorizontal: 10, marginTop: 10 }}>
                    <Text style={{
                        color: "#000000",
                        fontSize: mdscale(14), fontFamily: 'Poppins-Medium'
                    }}>{item.ASSESSMENT_ASSESSMENT_NAME}</Text>
                    <Text style={{ color: "#9C9C9D", marginBottom: 7 }}> Marks - {item.ASSESSMENT_MINPERCENTAGE}</Text>
                    <Text style={{ color: "#000000" }}>
                        {item.ASSESSMENT_ASSESSMENT_DESC}
                    </Text>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{ color: "grey" }}>
                       {/* Date - {moment(item.SA_DATE).format('MMMM d, YYYY')} */}
                       Date - {item.SA_DATE.slice(0,10)}
                    </Text>
                    <Text style={{ color: "grey" }}>
                       Time - {item.SA_START_TIME} - {item.SA_END_TIME}
                    </Text>
                    </View>
                </View>


                <View style={{
                    flexDirection: 'row', alignItems: 'center', marginTop: vrscale(10),
                    marginBottom: vrscale(10),
                    // backgroundColor: 'red',
                    width: '96%',
                    alignSelf: 'center', justifyContent: "space-evenly"
                }}>
                    {/* <View style={{
            width: 130,
            height: 40, backgroundColor: "#FFFFFF",
            borderRadius: 10, justifyContent: "center", alignItems: "center",
            borderWidth: 1, borderColor: "#708FFF",
        }}>
            <Text style={{ color: "#6268EE" ,fontWeight:"700",fontSize:13}}> View Files</Text>
        </View> */}

                  {ShowTestBtn == true?   <Pressable onPress={() => Navigation.navigate('AssesmentTest',{...item,TenantCode:loginId.TENANTCODE,AssessmentUserId:loginId.USERID,CourseId:selectedCourses})} style={{
                        width: 130,
                        height: 40, backgroundColor: "#708FFF",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",

                    }}>
                        <Text allowFontScaling={false} style={{ color: "#ffffff", fontWeight: "700", fontSize: 13 }}>Start test</Text>
                    </Pressable>:<>
                    {/* <Pressable onPress={() => Navigation.navigate('AssesmentTest',{...item,TenantCode:loginId.TENANTCODE,AssessmentUserId:loginId.USERID,CourseId:selectedCourses})} style={{
                        width: 130,
                        height: 40, backgroundColor: "#708FFF",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",

                    }}>
                        <Text allowFontScaling={false} style={{ color: "#ffffff", fontWeight: "700", fontSize: 13 }}>Start test</Text>
                    </Pressable> */}
                    </>}
                      {/* <Pressable onPress={() => Navigation.navigate('AssesmentTest',{...item,TenantCode:loginId.TENANTCODE,AssessmentUserId:loginId.USERID,CourseId:selectedCourses})} style={{
                        width: 130,
                        height: 40, backgroundColor: "#708FFF",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",

                    }}>
                        <Text allowFontScaling={false} style={{ color: "#ffffff", fontWeight: "700", fontSize: 13 }}>Start test</Text>
                    </Pressable> */}
                </View>



            </View>
        )
    }
 
    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header
                back={false}
                pagename={'Assesments'}
            />
            {ShowListOfAssessments == true ?
             <ScrollView>
             {ListOfAssessmentsData.map((item) => (
<ListOfAssessmentsComponent item={item} />
            //      <View style={{
            //          //width: mdscale(330),
            //          // height: mdscale(154),
            //          backgroundColor: "#ECF4FF",
            //          marginHorizontal: mdscale(10),
            //          marginTop: 20,
            //          borderRadius: 10, elevation: 3
            //      }}>                
            //          <View style={{ paddingHorizontal: 10, marginTop: 10 }}>
            //              <Text style={{
            //                  color: "#000000",
            //                  fontSize: mdscale(14), fontFamily: 'Poppins-Medium'
            //              }}>{item.ASSESSMENT_ASSESSMENT_NAME}</Text>
            //              <Text style={{ color: "#9C9C9D", marginBottom: 7 }}> Marks - {item.ASSESSMENT_MINPERCENTAGE}</Text>
            //              <Text style={{ color: "#000000" }}>
            //                  {item.ASSESSMENT_ASSESSMENT_DESC}
            //              </Text>
            //              <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            //              <Text style={{ color: "grey" }}>
            //                 {/* Date - {moment(item.SA_DATE).format('MMMM d, YYYY')} */}
            //                 Date - {item.SA_DATE.slice(0,10)}
            //              </Text>
            //              <Text style={{ color: "grey" }}>
            //                 Time - {item.SA_START_TIME} - {item.SA_END_TIME}
            //              </Text>
            //              </View>
            //          </View>


            //          <View style={{
            //              flexDirection: 'row', alignItems: 'center', marginTop: vrscale(10),
            //              marginBottom: vrscale(10),
            //              // backgroundColor: 'red',
            //              width: '96%',
            //              alignSelf: 'center', justifyContent: "space-evenly"
            //          }}>
            //              {/* <View style={{
            //      width: 130,
            //      height: 40, backgroundColor: "#FFFFFF",
            //      borderRadius: 10, justifyContent: "center", alignItems: "center",
            //      borderWidth: 1, borderColor: "#708FFF",
            //  }}>
            //      <Text style={{ color: "#6268EE" ,fontWeight:"700",fontSize:13}}> View Files</Text>
            //  </View> */}

            //              <Pressable onPress={() => Navigation.navigate('AssesmentTest',{...item,TenantCode:loginId.TENANTCODE,AssessmentUserId:loginId.USERID,CourseId:selectedCourses})} style={{
            //                  width: 130,
            //                  height: 40, backgroundColor: "#708FFF",
            //                  borderRadius: 10, justifyContent: "center", alignItems: "center",

            //              }}>
            //                  <Text allowFontScaling={false} style={{ color: "#ffffff", fontWeight: "700", fontSize: 13 }}>Start test</Text>
            //              </Pressable>
            //          </View>



            //      </View>
             ))}
             {/* <Button title="Show modal" onPress={toggleModal} /> */}

         </ScrollView>
         :
                <View style={{
                    flex: 1,
                    paddingHorizontal: 10,
                    marginTop: 20
                }}>
                    <Pressable onPress={toggleModal} style={{
                        width: '100%',
                        elevation: 5,
                        backgroundColor: "#fff",
                        padding: 10,
                        borderRadius: 10
                    }}>
                        <Text style={{
                            fontSize: 15,
                            color: "#4A4A4A",
                            fontWeight: "500",
                        }}>
                          {SelectcourseName==''? 'Select Courses':SelectcourseName}  
                        </Text>
                    </Pressable>
                    <Modal onBackdropPress={() => {
                        setModalVisible(false)}} isVisible={isModalVisible}>
                        <View style={{ width: "90%", height: 200, alignSelf: "center" }}>

                            {CoursesData.map((i) => (
                                <Pressable key={i.COURSE_ID} onPress={() => {
                                    setModalVisible(false)
                                    setSelectedCourses(i.COURSE_ID)
                                    setSelectcourseName(i.COURSE_NAME)
                                }} style={{
                                    width: '100%',
                                    elevation: 5,
                                    backgroundColor: "#fff",
                                    padding: 10,
                                    borderRadius: 10
                                }}>
                                    <Text style={{
                                        fontSize: 15,
                                        color: "#4A4A4A",
                                        fontWeight: "500",
                                    }}>
                                        {i.COURSE_NAME}
                                    </Text>
                                </Pressable>
                            ))}


                        </View>
                    </Modal>


                    {ShowCourseSchedule == true ?
                        <>
                            <Pressable onPress={toggleModal1} style={{
                                width: '100%',
                                elevation: 5,
                                backgroundColor: "#fff",
                                padding: 10,
                                borderRadius: 10,
                                marginTop:20
                            }}>
                                <Text style={{
                                    fontSize: 15,
                                    color: "#4A4A4A",
                                    fontWeight: "500",
                                }}>
                                    Select Courses Schedule
                                </Text>
                            </Pressable>
                            <Modal onBackdropPress={() => setModalVisible1(false)} isVisible={isModalVisible1}>
                                <View style={{ width: "90%", height: 200, alignSelf: "center" }}>

                                    {CourseScheduleData.map((i) => (
                                        <Pressable key={i.COURSESHD_ID} onPress={() =>{ 
                                            setModalVisible1(false)
                                            setSelectedShowCourseSchedule(i.COURSESHD_ID)
                                            getListOfAssessments(i.COURSESHD_ID)
                                        }} style={{
                                            width: '100%',
                                            elevation: 5,
                                            backgroundColor: "#fff",
                                            padding: 10,
                                            borderRadius: 10
                                        }}>
                                            <Text style={{
                                                fontSize: 15,
                                                color: "#4A4A4A",
                                                fontWeight: "500",
                                            }}>
                                                {i.COURSESHD_NAME}
                                            </Text>
                                        </Pressable>
                                    ))}


                                </View>
                            </Modal></>

                        // <Picker
                        //     style={{ backgroundColor: "#fff", marginTop: 20 }}
                        //     selectedValue={selectedShowCourseSchedule}
                        //     onValueChange={(itemValue, itemIndex) =>
                        //         setSelectedShowCourseSchedule(itemValue)
                        //     }>
                        //     <Picker.Item label="Select Courses Schedule" value="" />
                        //     {CourseScheduleData.map((i) => <Picker.Item label={i.COURSESHD_NAME} value={i.COURSESHD_ID} />)}

                        // </Picker>

                        : <View />
                    }
                </View> 
                
                

               
                
                }

            {/***1 st card */}
            {/* <View style={{
                //width: mdscale(330),
                // height: mdscale(154),
                backgroundColor: "#ECF4FF",
                marginHorizontal: mdscale(10),
                marginTop: 20,
                borderRadius: 10, elevation: 3
            }}>
                <View style={{ paddingLeft: 10, marginTop: 10 }}>
                    <Text style={{
                        color: "#000000",
                        fontSize: mdscale(14), fontFamily: 'Poppins-Medium'
                    }}>Assesment Name will be shown here</Text>
                    <Text style={{ color: "#9C9C9D", marginBottom: 7 }}>by Prof. Lavanya</Text>
                    <Text style={{ color: "#000000" }}>This will be the description of the assesment if any given
                        by the trainer</Text>
                </View>


                <View style={{
                    flexDirection: 'row', alignItems: 'center', marginTop: vrscale(10),
                    marginBottom: vrscale(10),
                    // backgroundColor: 'red',
                    width: '65%',
                    alignSelf: 'center', justifyContent: "space-between"
                }}>
                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#FFFFFF",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",
                        borderWidth: 1, borderColor: "#708FFF",
                    }}>
                        <Text style={{ color: "#000000" }}> View Files</Text>
                    </View>

                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#708FFF",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",

                    }}>
                        <Text style={{ color: "#ffffff" }}>Start test</Text>
                    </View>
                </View>



            </View> */}

            {/***1 st card */}
            {/* <View style={{
                //width: mdscale(330),
                // height: mdscale(154),
                backgroundColor: "#ECF4FF",
                marginHorizontal: mdscale(10),
                marginTop: 20,
                borderRadius: 10, elevation: 3
            }}>
                <View style={{ paddingLeft: 10, marginTop: 10 }}>
                    <Text style={{
                        color: "#000000",
                        fontSize: mdscale(14), fontFamily: 'Poppins-Medium'
                    }}>Assesment Name will be shown here</Text>
                    <Text style={{ color: "#9C9C9D", marginBottom: 7 }}>by Prof. Lavanya</Text>
                    <Text style={{ color: "#000000", }}>This will be the description of the assesment if any given
                        {`\n`}by the trainer</Text>
                </View>


                <View style={{
                    flexDirection: 'row', alignItems: 'center', marginTop: vrscale(10),
                    marginBottom: vrscale(10),
                    // backgroundColor: 'red',
                    width: '65%',
                    alignSelf: 'center', justifyContent: "space-between"
                }}>
                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#FFFFFF",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",
                        borderWidth: 1, borderColor: "#708FFF",
                    }}>
                        <Text style={{ color: "#000000" }}> View Files</Text>
                    </View>

                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#708FFF",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",

                    }}>
                        <Text style={{ color: "#ffffff" }}>Start test</Text>
                    </View>
                </View>



            </View> */}








        </View>
    )
}

export default Assesment

const styles = StyleSheet.create({})