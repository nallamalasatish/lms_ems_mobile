import { StyleSheet, Text, View, TextInput, Pressable, ScrollView } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../../Components/Header'
import AntDesign from 'react-native-vector-icons/AntDesign'
// import { Radio } from 'native-base';
import { Radio, Textarea } from 'native-base'
import AssesmentApi from '../../Service/AssesmentApi'
import Toast from 'react-native-simple-toast';
import Navigation from '../../Navigation'
// import {  } from 'react-native-gesture-handler'


let assmentData = {
    "AssessmentInformation": [
        {
            "ASSESSMENT_ASSESSMENT_NAME": "June mid Exam",
            "ASSESSMENT_TIMINGFORASSESSMENT": "30",
            "COURSE_NAME": "BTECH ECE",
            "ASSESSMENT_NO_OF_QUESTIONS": 6,
            "ASSESSMENT_MINPERCENTAGE": 50,
            "ASSESSMENT_MODE": 1,
            "SA_ID": 18573,
            "COMPLEXITY_TYPE": "1"
        }
    ],
    "AssessmentQuestions": [
        {
            "SRNO": 1,
            "QUESTION_ID": 5712,
            "QUESTION_COURSE": 8147,
            "QUESTION_CHAPTER": 6358,
            "QUESTION_QUESTION": "What is semiconductor?",
            "QUESTION_ANSWER": "resistivity,conductors,insulators",
            "QUESTION_CREATEDBY": 33271561,
            "QUESTION_CREATEDDATE": "2022-05-08T21:04:11.473793",
            "QUESTION_LASTMODIFIEDBY": 33271561,
            "QUESTION_LASTMODIFIEDDATE": "2022-05-08T21:04:11.473793",
            "QUESTION_STATUS": true,
            "TYPEID": 1,
            "ALLQUESTIONIDS": "5708,5710,5711,5712,5713,5714",
            "QUESTION_IMAGE": "",
            "IS_OPTION_RANDOM": false,
            "ANSWER_GIVENANSWER": null,
            "ACTUALMARKS": 5
        },
        {
            "SRNO": 2,
            "QUESTION_ID": 5714,
            "QUESTION_COURSE": 8147,
            "QUESTION_CHAPTER": 6358,
            "QUESTION_QUESTION": "Explain Energy Band diagram?",
            "QUESTION_ANSWER": "x-axis and Y-axis",
            "QUESTION_CREATEDBY": 33271561,
            "QUESTION_CREATEDDATE": "2022-05-08T21:06:04.197636",
            "QUESTION_LASTMODIFIEDBY": 33271561,
            "QUESTION_LASTMODIFIEDDATE": "2022-05-08T21:06:04.197636",
            "QUESTION_STATUS": true,
            "TYPEID": 1,
            "ALLQUESTIONIDS": "5708,5710,5711,5712,5713,5714",
            "QUESTION_IMAGE": "",
            "IS_OPTION_RANDOM": false,
            "ANSWER_GIVENANSWER": null,
            "ACTUALMARKS": 5
        },
        {
            "SRNO": 3,
            "QUESTION_ID": 5708,
            "QUESTION_COURSE": 8147,
            "QUESTION_CHAPTER": 6358,
            "QUESTION_QUESTION": "MOSFET features how many terminals?",
            "QUESTION_ANSWER": "4",
            "QUESTION_CREATEDBY": 33271561,
            "QUESTION_CREATEDDATE": "2022-05-08T19:13:25.44689",
            "QUESTION_LASTMODIFIEDBY": 33271561,
            "QUESTION_LASTMODIFIEDDATE": "2022-05-08T19:13:25.44689",
            "QUESTION_STATUS": true,
            "TYPEID": 0,
            "ALLQUESTIONIDS": "5708,5710,5711,5712,5713,5714",
            "QUESTION_IMAGE": "",
            "IS_OPTION_RANDOM": false,
            "ANSWER_GIVENANSWER": null,
            "ACTUALMARKS": 1
        },
        {
            "SRNO": 4,
            "QUESTION_ID": 5713,
            "QUESTION_COURSE": 8147,
            "QUESTION_CHAPTER": 6358,
            "QUESTION_QUESTION": "What is Insulator?",
            "QUESTION_ANSWER": "passage of electric current",
            "QUESTION_CREATEDBY": 33271561,
            "QUESTION_CREATEDDATE": "2022-05-08T21:05:00.659177",
            "QUESTION_LASTMODIFIEDBY": 33271561,
            "QUESTION_LASTMODIFIEDDATE": "2022-05-08T21:05:00.659177",
            "QUESTION_STATUS": true,
            "TYPEID": 1,
            "ALLQUESTIONIDS": "5708,5710,5711,5712,5713,5714",
            "QUESTION_IMAGE": "",
            "IS_OPTION_RANDOM": false,
            "ANSWER_GIVENANSWER": null,
            "ACTUALMARKS": 5
        },
        {
            "SRNO": 5,
            "QUESTION_ID": 5711,
            "QUESTION_COURSE": 8147,
            "QUESTION_CHAPTER": 6358,
            "QUESTION_QUESTION": "Which of the following is correct about photo diode electronic devices?",
            "QUESTION_ANSWER": " P-N junction is connected in reverse bias.",
            "QUESTION_CREATEDBY": 33271561,
            "QUESTION_CREATEDDATE": "2022-05-08T21:01:45.992074",
            "QUESTION_LASTMODIFIEDBY": 33271561,
            "QUESTION_LASTMODIFIEDDATE": "2022-05-08T21:01:45.992074",
            "QUESTION_STATUS": true,
            "TYPEID": 0,
            "ALLQUESTIONIDS": "5708,5710,5711,5712,5713,5714",
            "QUESTION_IMAGE": "",
            "IS_OPTION_RANDOM": false,
            "ANSWER_GIVENANSWER": null,
            "ACTUALMARKS": 1
        },
        {
            "SRNO": 6,
            "QUESTION_ID": 5710,
            "QUESTION_COURSE": 8147,
            "QUESTION_CHAPTER": 6358,
            "QUESTION_QUESTION": "Which of the following is not correct about a step-graded junction in electronic devices?",
            "QUESTION_ANSWER": "Diodes with step-graded junctions are slower than a normal diode",
            "QUESTION_CREATEDBY": 33271561,
            "QUESTION_CREATEDDATE": "2022-05-08T21:00:52.665188",
            "QUESTION_LASTMODIFIEDBY": 33271561,
            "QUESTION_LASTMODIFIEDDATE": "2022-05-08T21:00:52.665188",
            "QUESTION_STATUS": true,
            "TYPEID": 0,
            "ALLQUESTIONIDS": "5708,5710,5711,5712,5713,5714",
            "QUESTION_IMAGE": "",
            "IS_OPTION_RANDOM": false,
            "ANSWER_GIVENANSWER": null,
            "ACTUALMARKS": 1
        },

    ],
    "AssessmentOptions": [
        {
            "OPTION_ID": 8846,
            "QSTOPT_QUESTIONAIRE_ID": 5708,
            "QSTOPT_OPTION": "3",
            "QUESTION_ID": 5708,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8847,
            "QSTOPT_QUESTIONAIRE_ID": 5708,
            "QSTOPT_OPTION": "4",
            "QUESTION_ID": 5708,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8850,
            "QSTOPT_QUESTIONAIRE_ID": 5710,
            "QSTOPT_OPTION": "Diodes with step-graded junctions are slower than a normal diode",
            "QUESTION_ID": 5710,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8851,
            "QSTOPT_QUESTIONAIRE_ID": 5710,
            "QSTOPT_OPTION": "They are designed with abrupt junction",
            "QUESTION_ID": 5710,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8852,
            "QSTOPT_QUESTIONAIRE_ID": 5711,
            "QSTOPT_OPTION": " P-N junction is connected in reverse bias.",
            "QUESTION_ID": 5711,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8853,
            "QSTOPT_QUESTIONAIRE_ID": 5711,
            "QSTOPT_OPTION": " It is a photovoltaic cell",
            "QUESTION_ID": 5711,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8854,
            "QSTOPT_QUESTIONAIRE_ID": 5712,
            "QSTOPT_OPTION": "resistivity,conductors,insulators",
            "QUESTION_ID": 5712,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8855,
            "QSTOPT_QUESTIONAIRE_ID": 5713,
            "QSTOPT_OPTION": "passage of electric current",
            "QUESTION_ID": 5713,
            "IS_OPTION_RANDOM": false
        },
        {
            "OPTION_ID": 8856,
            "QSTOPT_QUESTIONAIRE_ID": 5714,
            "QSTOPT_OPTION": "x-axis and Y-axis",
            "QUESTION_ID": 5714,
            "IS_OPTION_RANDOM": false
        }
    ],
    "AssessmentAttempts": [
        {
            "RESULT_ATTEMPTSLEFT": 7,
            "ResumeTime": 0
        }
    ]
}

const UselessTextInput = (props) => {
    return (
        <TextInput
            {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
            editable
            maxLength={40}
            placeholder='text Box'
        />
    );
}


const RRadioComponent = ({ item, AssessmentOptions, setAssessmentAnswers, AssessmentAnswers }) => {
    const [PollAns, setPollAns] = useState('')
    let radioData = AssessmentOptions.filter(i => i.QUESTION_ID == item.QUESTION_ID)
    // console.log('radioData', AssessmentOptions);


    useEffect(() => {
        if (PollAns == '') {

        } else {
            let newAns = AssessmentAnswers.filter(a => a.QuestionId !== item.QUESTION_ID)
            let ans = {
                "TypeId": 0,
                "QuestionAnswer": item.QUESTION_ANSWER,
                "QuestionId": item.QUESTION_ID,
                "QuestionMarks": item.ACTUALMARKS,
                "Selected": PollAns
            }
            setAssessmentAnswers([...newAns, ans])
        }
    }, [PollAns])



    useEffect(() => {
        let newAns = AssessmentAnswers.filter(a => a.QuestionId == item.QUESTION_ID)
        if (!newAns.length == 0) {
            setPollAns(newAns[0].Selected)
            console.log(newAns[0].Selected);
        }
    }, [])

    return (
        <>
            <Text style={{
                fontSize: 15,
                color: "#4A4A4A",
                fontWeight: "500",
                marginTop: 20
            }}>
                {item.SRNO} {item.QUESTION_QUESTION}
            </Text>
            {radioData.map((i, index) => (
                <View key={index} style={{
                    marginLeft: 10,
                    marginTop: 10,
                    flexDirection: 'row',
                    marginRight: 20
                }}>
                    <Radio color='#656565' selected={PollAns == i.QSTOPT_OPTION ? true : false}
                        onPress={() => setPollAns(i.QSTOPT_OPTION)} />
                    <Text allowFontScaling={false} style={{
                        color: "#656565",
                        fontSize: 14,
                        paddingLeft: 4,
                        fontWeight: "500"
                    }}>{i.QSTOPT_OPTION}</Text>

                </View>
            ))}


        </>
    )
}

const TextBoxComponent = ({ item, AssessmentOptions, setAssessmentAnswers, AssessmentAnswers }) => {

    const [value, onChangeText] = React.useState('');

    useEffect(() => {
        if (value == '') {

        } else {
            let newAns = AssessmentAnswers.filter(a => a.QuestionId !== item.QUESTION_ID)
            let ans = {
                "TypeId": 1,
                "QuestionAnswer": item.QUESTION_ANSWER,
                "QuestionId": item.QUESTION_ID,
                "QuestionMarks": item.ACTUALMARKS,
                "Selected": value
            }
            setAssessmentAnswers([...newAns, ans])
        }
    }, [value])
    useEffect(() => {
        let newAns = AssessmentAnswers.filter(a => a.QuestionId == item.QUESTION_ID)
        if (!newAns.length == 0) {
            onChangeText(newAns[0].Selected)
        }
    }, [])
    // useEffect(()=>{
    //     onChangeText('defult')
    // },[])

    return (
        <>
            <Text style={{
                fontSize: 15,
                color: "#4A4A4A",
                fontWeight: "500",
                marginTop: 20
            }}>
                {item.SRNO}  {item.QUESTION_QUESTION}
            </Text>
            <View style={{
                borderWidth: 1,
                borderColor: "#A8A8A8",
                backgroundColor: "#EAEAEA",
                borderRadius: 10,
                marginTop: 10
            }}>
                <Textarea
                    multiline
                    numberOfLines={6}
                    onChangeText={text => onChangeText(text)}
                    value={value}
                    style={{ padding: 10, color: "#AAAAAA" }}
                    editable
                    
                    placeholder='text Box'
                    ></Textarea>
                {/* <UselessTextInput
                   
                /> */}

            </View>
        </>
    )
}
const AssesmentTest = (props) => {
    const [CurrentPage, setCurrentPage] = useState(1)
    const [TotalPage, setTotalPage] = useState()
    const [CurrentData, setCurrentData] = useState([assmentData.AssessmentQuestions[0]])
    let AssmentDataObj = props.route.params
    const [AllData, setAllData] = useState([])
    const [AssessmentOptions, setAssessmentOptions] = useState([])
    const [AssessmentAnswers, setAssessmentAnswers] = useState([])


    async function getAssessment() {
        console.log('start');
       
        try {
            let result = await AssesmentApi.GetAssessment({
                "AssessmentCourseId": AssmentDataObj.ASSESSMENT_COURSE_ID,
                "AssessmentId": AssmentDataObj.ASSESSMENT_ID,
                "AssessmentType": AssmentDataObj.ASSESSMENT_EXAMINATION_TYPE,
                "AssessmentUserId": AssmentDataObj.AssessmentUserId,
                "CourseScheduleId": AssmentDataObj.ASSESSMENT_COURSESECHD_ID,
                "ScheduleId": AssmentDataObj.SA_ID,
                "TenantCode": AssmentDataObj.TenantCode,
                "NoOfQuestions": AssmentDataObj.ASSESSMENT_NO_OF_QUESTIONS
            })
            // setData(result)
            console.log('asi', result);
          if ( result== 'Your no.of attempts left is 0,please contact Administrator for further assistance') {
            Toast.show('No. of attempts left = 0. Please contact your adminstrator', Toast.SHORT, ['UIAlertController']);
            Navigation.back()
          } else if (result.AssessmentAttempts[0].RESULT_ATTEMPTSLEFT == 0) {
                Toast.show('No. of attempts left = 0. Please contact your adminstrator', Toast.SHORT, ['UIAlertController']);
                Navigation.back()
            }
            setAllData(result.AssessmentQuestions)
            setAssessmentOptions(result.AssessmentOptions)
            setCurrentData(result.AssessmentQuestions.filter(a => a.SRNO == CurrentPage))
            setTotalPage(result.AssessmentQuestions.length)
            // setCoursesData(result)
        } catch (error) {
            console.log(error);

        }

    }
    async function submitAnswer() {
        console.log('submitAnswer', AssessmentAnswers);

        let finialAns = {
            "AssessmentId": AssmentDataObj.ASSESSMENT_ID,
            "AssessmentType": AssmentDataObj.ASSESSMENT_EXAMINATION_TYPE,
            "CourseId": AssmentDataObj.CourseId,
            "SaId": AssmentDataObj.SA_ID,
            "PassPercentage": 50,
            "AssessmentAnswers": [
                ...AssessmentAnswers
            ],
            "UserId": AssmentDataObj.AssessmentUserId
        }
        try {
            let result = await AssesmentApi.SetAssessments(finialAns)
            console.log('finalResult', result);
          
            Toast.show('your answer has been submitted successfully', Toast.SHORT, ['UIAlertController']);

            Navigation.back()
        } catch (error) {
            console.log(error);
        }

    }

    useEffect(() => {
        // console.log(props.route.params);
        getAssessment()

        // setAllData(assmentData.AssessmentQuestions)
        // setAssessmentOptions(assmentData.AssessmentOptions)
        // setTotalPage(assmentData.AssessmentQuestions.length)
        // setCurrentData([assmentData.AssessmentQuestions[0]])
    }, [])
    useEffect(() => {
        setCurrentData(AllData.filter(a => a.SRNO == CurrentPage))
        // console.log(AllData.filter(a => a.SRNO == CurrentPage));
    }, [CurrentPage])

    return (
        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header
                back={false}
                pagename={'Assesments'}
            />
            <View style={{
                flex: 1,
                paddingHorizontal: 10,
                marginTop: 10
            }}>
                <Text style={{
                    fontSize: 17,
                    color: "#252525",
                    fontWeight: "700"
                }}>
                    Assesment / Exam Name will be Shown up here
                    with multiple lines if the text is lenghty.
                </Text>
                <View style={{
                    width: '100%',
                    height: 1,
                    backgroundColor: "#E7E7E7",
                    marginTop: 20
                }} />
                <ScrollView>
                    {AllData.map((item, index) => (
                        <>
                            {item.SRNO == CurrentPage ?
                                <>
                                    {item.TYPEID == 1 ?
                                        <TextBoxComponent
                                            key={index}
                                            item={item}
                                            AssessmentAnswers={AssessmentAnswers}
                                            setAssessmentAnswers={setAssessmentAnswers}
                                            AssessmentOptions={AssessmentOptions}
                                        /> :
                                        <RRadioComponent
                                            key={index}
                                            item={item}
                                            AssessmentAnswers={AssessmentAnswers}
                                            setAssessmentAnswers={setAssessmentAnswers}
                                            AssessmentOptions={AssessmentOptions}
                                        />}
                                </> : <></>}
                        </>
                    ))}

                    {/*  */}

                    <View style={{
                        width: '100%',
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        marginTop: 20
                    }}>
                        <Pressable onPress={() => { CurrentPage == 1 ? {} : setCurrentPage(s => s - 1) }} style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            width: 76,
                            height: 33,
                            backgroundColor: "#BFBFBF",
                            borderRadius: 3,
                            marginRight: 5
                        }}>
                            <AntDesign name='left' size={13} color='#424242' />
                            <Text style={{
                                color: "#424242",
                                fontSize: 15,
                                paddingLeft: 3,
                                fontWeight: "500"
                            }}>
                                Prev
                            </Text>
                        </Pressable>
                        <Pressable onPress={() => { CurrentPage == TotalPage ? {} : setCurrentPage(s => s + 1) }} style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            width: 76,
                            height: 33,
                            backgroundColor: "#3E48A0",
                            borderRadius: 3,
                            marginLeft: 5
                        }}>
                            <Text style={{
                                color: "#fff",
                                fontSize: 15,
                                paddingRight: 3,
                                fontWeight: "500"
                            }}>
                                Next
                            </Text>
                            <AntDesign name='right' size={13} color='#fff' />
                        </Pressable>
                    </View>
                    {CurrentPage == TotalPage ?
                        <Pressable onPress={submitAnswer} style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            width: 176,
                            height: 46,
                            backgroundColor: "#3E48A0",
                            borderRadius: 3,
                            marginLeft: 5,
                            alignSelf: "center",
                            marginTop: 20
                        }}>
                            <Text style={{
                                color: "#fff",
                                fontSize: 18,
                                paddingRight: 3,
                                fontWeight: "700"
                            }}>
                                Submit
                            </Text>
                        </Pressable> : <></>}
                </ScrollView>
            </View>

        </View>
    )
}

export default AssesmentTest

const styles = StyleSheet.create({})