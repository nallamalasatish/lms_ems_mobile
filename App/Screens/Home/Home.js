import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView, ImageBackground,
} from 'react-native'
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import AuthService from '../../Service/Auth'
import LinearGradient from 'react-native-linear-gradient';
import CircularProgress from 'react-native-circular-progress-indicator';

import { useSelector, useDispatch } from 'react-redux'
import { vrscale, mdscale } from '../../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
import Navigation from '../../Navigation/index';
import Header from '../../Components/Header';
import LearningMeterials from '../../Service/LearningMeterials'
{/** vectot icon */ }
import Palet from 'react-native-vector-icons/AntDesign'
import Toast from 'react-native-simple-toast';



const Home = () => {
    const userData = useSelector((state) => state.User.userData)
    const [course, setCourse] = useState(
        [
            {
                id: 1,
                imageBook: require('../../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },
            {
                id: 2,
                imageBook: require('../../Assets/Images/Rectangle149.jpg'),
                teacherName: 'P.V Kiran',
                lessonLength: ' 15 of 28 Lessons'
            },
           
    
        ]
    )
    
    const CourseX = ({ item }) => {
        return (
            <Pressable onPress={() => Navigation.navigate('Mycoursedetail')}>
                <View style={styles.flatBox}>
                    <Image source={item.imageBook} />
                    <View style={{ paddingLeft: mdscale(10), flex: 1 }}>
                        <Text style={{ fontWeight: "bold", color: "#000000" }}> C Programing Langauge</Text>
                        <Text style={{ color: "#8EB5FF" }}> {item.teacherName}</Text>
                        <Text style={{ color: "#8EB5FF" }}> {item.lessonLength}</Text>
    
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                            <Palet name="clockcircleo" size={14} color={"#000000"} />
                            <Text style={{ color: "#A8A8A8", marginLeft: 5, marginRight: 7 }}>18 Hr.</Text>
                            <Palet name="star" size={14} color={"#FFB547"} />
                            <Text style={{ color: "#A8A8A8", marginLeft: 5 }}>40</Text>
                        </View>
    
                    </View>
                    <Image source={require('../../Assets/Images/Group325.jpg')}
                        style={{ marginRight: 20 }}
                    />
                </View>
            </Pressable>
        )
    }
    const [value, setValue] = useState(0);
    const [classes, setClasses] = useState([])
    const [sessions, setSessions] = useState([])

    async function getCourceData() {
        let result = await AuthService.getCourses({ "UserId": userData.USERID, "TenantCode": userData.TENANTCODE, "Username":userData.USERNAME })
        if (result) {
            // console.log(result);
            setClasses(result.classes)
            setSessions(result.sessions)
        }
    }
   async function JoinClass(item) {
    //    console.log(item);
    //    console.log(userData);
       let data = {
        "UserId": userData.USERID,
        "TenantCode":userData.TENANTCODE,
        "Username":userData.FIRSTNAME + " "+ userData.LASTNAME,
        "APPOINTMENT_ID":item.URL,
        "RoleId":userData.ROLEID
        }
        console.log(data);
    let result = await LearningMeterials.Joinurl(data)
    if (result) {
        // console.log(result);
        if (result.JoinUrl == "") {
            Toast.show('Host Not Joined', Toast.SHORT,['UIAlertController']);
        } else {
            
            Navigation.navigate('JoinClassPage',{url:result.JoinUrl })
        }
    }
}
    useEffect(() => {
        getCourceData()
    console.log(userData);
    }, [])
    return (

        <View style={{ flex: 1, backgroundColor: "#E5E5E5" }}>
            <Header name={`Hey, ${ userData.FIRSTNAME} ${userData.LASTNAME}`}
                namey={"Welcome To Samvaad LMS"}
                Home
                back={false} />
            <ScrollView>
                {/**2  box */}
                <View style={{
                    flexDirection: 'row',
                    marginTop: vrscale(20),
                    justifyContent: "space-around"
                }}>



                    <ImageBackground
                        source={require('../../Assets/Images/Card.jpg')}
                        imageStyle={{ borderRadius: 35 }}
                        style={styles.cbox}>

                        <CircularProgress
                            allowFontScaling={false}
                            radius={mdscale(55)}
                            value={25}
                            progressValueStyle={{ fontSize: 20, textAlign: "center", marginBottom: 0, paddingBottom: 0 }}
                            duration={2000}
                            valueSuffix={'%'}
                            activeStrokeWidth={16}
                            inActiveStrokeWidth={17}
                            progressValueColor={'#ecf0f1'}
                            activeStrokeColor={"#185E9F"}
                            inActiveStrokeColor={"#FFFFFF"}
                            title={'Completed'}
                            titleColor={'white'}
                            titleStyle={{
                                fontWeight: '400', fontSize: 10,
                                paddingBottom: 10, paddingTop: 0
                            }}

                        />

                        <Text
                            allowFontScaling={false}
                            style={{
                                color: "#ffffff", textAlign: "center", fontWeight: "bold",
                                fontSize: 15,
                                marginTop: vrscale(10)
                            }}>Courses</Text>


                    </ImageBackground>

                    <View style={{
                        borderRadius: 10, width: mdscale(164), height: mdscale(171),

                    }}>
                        <ImageBackground
                            source={require('../../Assets/Images/Card.jpg')}
                            imageStyle={{ borderRadius: 35 }}
                            style={styles.cbox}>

                            <CircularProgress
                                allowFontScaling={false}
                                radius={mdscale(55)}
                                value={60}
                                duration={2000}
                                valueSuffix={'%'}
                                activeStrokeWidth={16}
                                progressValueStyle={{ fontSize: 20 }}
                                inActiveStrokeWidth={17}
                                progressValueColor={'#ecf0f1'}
                                activeStrokeColor={"#185E9F"}
                                inActiveStrokeColor={"#FFFFFF"}
                                //title={'Completed'}
                                titleColor={'white'}
                                titleStyle={{ fontWeight: 'bold' }}

                            />

                            <Text
                                allowFontScaling={false}
                                style={{
                                    color: "#ffffff", textAlign: "center", fontWeight: "bold",
                                    fontSize: 15,
                                    marginTop: vrscale(2)
                                }}>Attendance{'\n'}
                                Percentage</Text>


                        </ImageBackground>
                    </View>

                </View>

                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center', marginTop: 20, marginLeft: 20, marginBottom: 10
                }}>
                    <Text style={{ color: "#6771E4", fontWeight: "bold" }}> Class Session </Text>

                    <Text style={{
                        color: "#989898",
                        textDecorationLine: 'underline',
                        paddingLeft: 10
                    }}> Events</Text>
                </View>


                {/** another Box join class */}
                <View style={styles.classBox}>

                    <FlatList
                        data={sessions}
                        renderItem={({ item, index }) => (
                            <View style={index % 2 == 0 ? styles.barBox : styles.barBoxB} >
                                <View
                                    style={{ flex: 1 ,  paddingVertical: 10}}
                                >
                                    <Text
                                        allowFontScaling={false}
                                        style={{ color: "#5A5A5A", fontWeight: "bold" ,fontSize:16 }}
                                    > {item.SessionName}</Text>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }}
                                    >
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 12 }}>by. Prof. Laxmi</Text>
                                        </View>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }}
                                    >
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 13 }}>Start Time</Text>
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 12 }}>{item.StartTime.split(' ')[0]}</Text>
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 12 }}> {item.StartTime.split(' ')[1]}</Text>
                                    </View>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }}
                                    >
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 13 }}>End Time</Text>
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 12 }}>{item.EndTime.split(' ')[0]}</Text>
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 12 }}> {item.EndTime.split(' ')[1]}</Text>
                                    </View>
                                </View>
                                <Pressable
                                    style={styles.btn}
                                    onPress={()=> JoinClass(item)}
                                >
                                    <Text
                                        allowFontScaling={false}
                                        style={[styles.btn_txt,{fontSize:15}]}>Join class</Text>
                                </Pressable>
                            </View>

                        )}
                        keyExtractor={item => item.SessionId}
                    />
                    {/* {sessions.map((data, i) => ( */}
                    {/* <View style={ styles.barBox } >
                                <View
                                    style={{ flex: 1 }}
                                >
                                    <Text
                                        allowFontScaling={false}
                                        style={{ color: "#5A5A5A", fontWeight: "bold" }}
                                    >  Lorem Ipsum has been the  </Text>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }}
                                    >
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 12 }}>by. Prof. Laxmi</Text>
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 11 }}>1/03/22</Text>
                                        <Text
                                            allowFontScaling={false}
                                            style={{ color: "#8F8F8F", fontWeight: "400", fontSize: 11 }}>02.22 PM</Text>
                                    </View>
                                </View>
                                <Pressable
                                    style={styles.btn}
                                    onPress={JoinClass}
                                >
                                    <Text
                                        allowFontScaling={false}
                                        style={styles.btn_txt}>Join class</Text>
                                </Pressable>
                            </View> */}

                    {/* // ))} */}

                    {/**** */}


                    {/**** */}





                </View>


                <Text style={{
                    color: "#6771E4",
                    fontWeight: "bold", marginTop: 20,
                    marginLeft: 20
                }}>My Courses</Text>
 {/* <FlatList
                //columnWrapperStyle={{ justifyContent: 'space-between' }}
                showsVerticalScrollIndicator={false}
                numColumns={1}
                data={course}
                renderItem={CourseX}
                //console.log(Cardx))
                keyExtractor={item => item.id}
            /> */}

                <FlatList
                    data={classes.slice(0, 2)}
                    renderItem={({ item }) => (
                        <Pressable onPress={() => Navigation.navigate('Mycoursedetail',{CourseScheduleId:`${item.CourseScheduleId}`,CourseId:`${item.Id}`,Name:item.Name})}>
                        <View style={styles.flatBox}>
                            <Image source={require('../../Assets/Images/Rectangle149.jpg')} />
                            <View style={{ paddingLeft: mdscale(10), flex: 1 }}>
                                <Text style={{ fontWeight: "bold", color: "#000000" }}>{item.Name}</Text>
                                {/* <Text style={{ color: "#8EB5FF" }}>P.V Kiran</Text>
                                <Text style={{ color: "#8EB5FF" }}> 15 of 28 Lessons</Text> */}
            
                                {/* <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                    <Palet name="clockcircleo" size={14} color={"#000000"} />
                                    <Text style={{ color: "#A8A8A8", marginLeft: 5, marginRight: 7 }}>18 Hr.</Text>
                                    <Palet name="star" size={14} color={"#FFB547"} />
                                    <Text style={{ color: "#A8A8A8", marginLeft: 5 }}>40</Text>
                                </View> */}
            
                            </View>
                            <Image source={require('../../Assets/Images/Group325.jpg')}
                                style={{ marginRight: 20 }}
                            />
                        </View>
                    </Pressable>
                    )}
                    keyExtractor={item => item.Id}
                />

                {/*** */}
                {/* {classes.slice(0, 2).map((data, i) => (
                    <View key={i} style={styles.flatBox}>
                        <Text style={{ marginBottom: 10, fontWeight: "bold", color: "#000000" }}> {data.Name}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>


                            <Image source={require('../../Assets/Images/Rectangle149.jpg')}
                                style={{}}
                            />
                            <View style={{ paddingLeft: mdscale(10), flex: 1 }}>

                                <Text style={{ color: "#8EB5FF" }}> by PV Kiran</Text>
                                <Text style={{ color: "#8EB5FF" }}> 15 0f 20 Lessons</Text>

                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                    <Palet name="clockcircleo" size={14} color={"#000000"} />
                                    <Text style={{ color: "#A8A8A8", marginLeft: 5, marginRight: 7 }}>18 Hr.</Text>
                                    <Palet name="star" size={14} color={"#FFB547"} />
                                    <Text style={{ color: "#A8A8A8", marginLeft: 5 }}>40</Text>
                                </View>

                            </View>
                            <Image source={require('../../Assets/Images/Group325.jpg')}
                                style={{ marginRight: 20 }}
                            />
                        </View>
                    </View>
                ))} */}


                {/*** */}

                {/* <View style={styles.flatBox}>
                    <Image source={require('../../Assets/Images/Rectangle149.jpg')}
                        style={{}}
                    />
                    <View style={{ paddingLeft: mdscale(10), flex: 1 }}>
                        <Text style={{ fontWeight: "bold", color: "#000000" }}> C Programing Langauge</Text>
                        <Text style={{ color: "#8EB5FF" }}> by PV Kiran</Text>
                        <Text style={{ color: "#8EB5FF" }}> 15 0f 20 Lessons</Text>

                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                            <Palet name="clockcircleo" size={14} color={"#000000"} />
                            <Text style={{ color: "#A8A8A8", marginLeft: 5, marginRight: 7 }}>18 Hr.</Text>
                            <Palet name="star" size={14} color={"#FFB547"} />
                            <Text style={{ color: "#A8A8A8", marginLeft: 5 }}>40</Text>
                        </View>

                    </View>
                    <Image source={require('../../Assets/Images/Group325.jpg')}
                        style={{ marginRight: 20 }}
                    />
                </View> */}





                <View  style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center', marginBottom: 10 }}>
                    <Text style={{
                        color: "#6771E4",
                        fontWeight: "bold",
                        marginLeft: mdscale(260), fontSize: 16, textDecorationLine: 'underline',
                    }} onPress={()=> Navigation.navigate("Mycourse")}>View All

                    </Text>
                    <Palet name="arrowright" size={18} color={"#6771E4"}
                        style={{ paddingLeft: 5 }} />
                </View>


            </ScrollView>

        </View>

    )
}

export default Home

const styles = StyleSheet.create({
    cbox: {
        width: mdscale(161), height: mdscale(174),
        alignItems: 'center', paddingTop: mdscale(20)
        //backgroundColor: "#1C69B1"
    },
    classBox: {
        width: '95%',
        alignSelf: 'center',
        //height: mdscale(208),
        borderRadius: 7,
        borderColor: "#CCE0EB",
        borderWidth: 2,
       
        marginHorizontal: 10
    },
    barBox: {
        width: '100%',
        // paddingVertical: 10,
        backgroundColor: "#DBECF5",
        paddingHorizontal: 10,
      
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-around",
    },

    barBoxB: {
        width: '100%', 
        backgroundColor: "#FFFFFF",
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-around",

    },

    flatBox: {
        width: mdscale(330),
        paddingVertical:10, marginTop: 20,
        backgroundColor: "#FFFFFF", borderRadius: 10,
        elevation: 3,
        paddingLeft: 10,
        marginHorizontal: 10, flexDirection: 'row',
        alignItems: 'center',

    },
    // flatBox: {
    //     width: mdscale(330),
    //     marginTop: 20,
    //     backgroundColor: "#FFFFFF", borderRadius: 10,
    //     elevation: 3,
    //     paddingLeft: 10,
    //     marginHorizontal: 10,
    //     paddingVertical: 10,
    //     //justifyContent: "space-around"

    // },
    btn: {
        backgroundColor: "#708FFF",
        width: mdscale(77),
        height: mdscale(35),
        // marginHorizontal: mdscale(30),
        alignSelf: "center",
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 3,
        // marginBottom: 30,
        marginLeft: mdscale(8)

    },
    btn_txt: {
        color: "#FFFFFF",
        fontWeight: '700',
        fontSize: mdscale(11)
    },

})