//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';

// create a component
const JoinClassPage = ({route}) => {
    console.log("route",route.params.url);
    return (
        <View style={styles.container}>
          <WebView source={{ uri: route.params.url }} />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
   
    },
});

//make this component available to the app
export default JoinClassPage;
