import { StyleSheet, Text, View, ScrollView, Image, Pressable } from 'react-native'
import React,{useState,useEffect} from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector, useDispatch } from 'react-redux'
import Navigation from '../Navigation';
import LearningMeterials from '../Service/LearningMeterials'

const ExpexdableComponent = ({item})=>{

const [Show, setShow] = useState(true)
const [Show1, setShow1] = useState(true)
     return(
<>
{Show == true?
 <Pressable onPress={()=> setShow1(true) } style={{
    width: "100%",
    backgroundColor: "#fff",
    padding: 10,
    borderRadius: 10,
    marginTop: 10
}}>
    <Text style={{
        color: "#4D4D4D",
        fontSize: 15,
        fontWeight: '700'
    }}>
        {item.COURSE_NAME}
    </Text>
    {Show1 == true?
    <Text style={{
        color: "#747474",
        fontSize: 13,
        fontWeight: '400'
    }}>
      {item.DESCRIPTION} 
    </Text>:<></>}
    {/* <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#D4D4D4",
        marginTop: 10
    }} /> */}
    {/* <View style={{
        flexDirection: "row",
        alignItems: "center",
        marginTop: 10
    }}>
        <View style={{
            flexDirection: "row",
            alignItems: "center",
            flex: 1
        }}>
            <Image
                style={{ width: 13, height: 13, borderRadius: 7 }}
                source={{
                    uri: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
                }}
            />
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 10
            }}>
                by
            </Text>
            <Text style={{
                color: "#624EDC",
                fontSize: 12,
                fontWeight: '500',
                marginLeft: 4
            }}>
                Ramesh K
            </Text>
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 20
            }}>
                2 days ago
            </Text>
        </View>
        <Pressable onPress={()=> setShow(false) } style={{
            flexDirection: "row",
            alignItems: "center"
        }}>
            <MaterialCommunityIcons name='comment-outline' color={'#7E7E7E'} size={13} />
            <Text style={{
                color: "#A1A1A1",
                fontSize: 13,
                fontWeight: '400',
                marginLeft: 10
            }}>12</Text>
        </Pressable >
    </View> */}
</Pressable>
:
<View style={{
    width: "100%",
    backgroundColor: "#fff",
    padding: 10,
    borderRadius: 10,
    marginTop:20
}}>
    <Text style={{
        color: "#4D4D4D",
        fontSize: 15,
        fontWeight: '700'
    }}>
        What should be the first question in Forums?
    </Text>
    <Text style={{
        color: "#747474",
        fontSize: 13,
        fontWeight: '400'
    }}>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. LoremIpsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

    </Text>
    <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#D4D4D4",
        marginTop: 10
    }} />
    <View style={{
        flexDirection: "row",
        alignItems: "center",
        marginTop: 15
    }}>
        <View style={{
            flexDirection: "row",
            alignItems: "center",
            flex: 1
        }}>
            <Image
                style={{ width: 13, height: 13, borderRadius: 7 }}
                source={{
                    uri: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
                }}
            />
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 10
            }}>
                by
            </Text>
            <Text style={{
                color: "#624EDC",
                fontSize: 12,
                fontWeight: '500',
                marginLeft: 4
            }}>
                Ramesh K
            </Text>
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 20
            }}>
                2 days ago
            </Text>
        </View>
        <View style={{
            flexDirection: "row",
            alignItems: "center"
        }}>
            <MaterialCommunityIcons name='comment-outline' color={'#7E7E7E'} size={13} />
            <Text style={{
                color: "#A1A1A1",
                fontSize: 13,
                fontWeight: '400',
                marginLeft: 10
            }}>12</Text>
        </View>
    </View>
      <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#D4D4D4",
        marginTop: 15
    }} />
    <Text style={{
         color: "#747474",
         fontSize: 13,
         fontWeight: '400',
         marginTop:10
    }}>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    </Text>
    <View style={{
            flexDirection: "row",
            alignItems: "center",
            flex: 1,
            marginTop:10
        }}>
            <Image
                style={{ width: 13, height: 13, borderRadius: 7 }}
                source={{
                    uri: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
                }}
            />

            <Text style={{
                color: "#624EDC",
                fontSize: 12,
                fontWeight: '500',
                marginLeft: 7
            }}>
               Ravi
            </Text>
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 20
            }}>
                2 days ago
            </Text>
            <Text style={{
                   color: "#624EDC",
                   fontSize: 13,
                   fontWeight: '500',
                   marginLeft: 20
            }}>
                Reply
            </Text>
        </View>
        <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#E9F5FF",
        marginTop: 10
    }} />
    <Text style={{
         color: "#747474",
         fontSize: 13,
         fontWeight: '400',
         marginTop:10
    }}>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    </Text>
    <View style={{
            flexDirection: "row",
            alignItems: "center",
            flex: 1,
            marginTop:10
        }}>
            <Image
                style={{ width: 13, height: 13, borderRadius: 7 }}
                source={{
                    uri: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
                }}
            />

            <Text style={{
                color: "#624EDC",
                fontSize: 12,
                fontWeight: '500',
                marginLeft: 7
            }}>
               Ravi
            </Text>
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 20
            }}>
                2 days ago
            </Text>
            <Text style={{
                   color: "#624EDC",
                   fontSize: 13,
                   fontWeight: '500',
                   marginLeft: 20
            }}>
                Reply
            </Text>
        </View>
        <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#E9F5FF",
        marginTop: 10
    }} />
    <Text style={{
         color: "#747474",
         fontSize: 13,
         fontWeight: '400',
         marginTop:10
    }}>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    </Text>
    <View style={{
            flexDirection: "row",
            alignItems: "center",
            flex: 1,
            marginTop:10
        }}>
            <Image
                style={{ width: 13, height: 13, borderRadius: 7 }}
                source={{
                    uri: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
                }}
            />

            <Text style={{
                color: "#624EDC",
                fontSize: 12,
                fontWeight: '500',
                marginLeft: 7
            }}>
               Ravi
            </Text>
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 20
            }}>
                2 days ago
            </Text>
            <Text style={{
                   color: "#624EDC",
                   fontSize: 13,
                   fontWeight: '500',
                   marginLeft: 20
            }}>
                Reply
            </Text>
        </View>
        <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#E9F5FF",
        marginTop: 10
    }} />
    <Text style={{
         color: "#747474",
         fontSize: 13,
         fontWeight: '400',
         marginTop:10
    }}>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    </Text>
    <View style={{
            flexDirection: "row",
            alignItems: "center",
            flex: 1,
            marginTop:10
        }}>
            <Image
                style={{ width: 13, height: 13, borderRadius: 7 }}
                source={{
                    uri: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
                }}
            />

            <Text style={{
                color: "#624EDC",
                fontSize: 12,
                fontWeight: '500',
                marginLeft: 7
            }}>
               Ravi
            </Text>
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 20
            }}>
                2 days ago
            </Text>
            <Text style={{
                   color: "#624EDC",
                   fontSize: 13,
                   fontWeight: '500',
                   marginLeft: 20
            }}>
                Reply
            </Text>
        </View>
        <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#E9F5FF",
        marginTop: 10
    }} />
    <Text style={{
         color: "#747474",
         fontSize: 13,
         fontWeight: '400',
         marginTop:10
    }}>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    </Text>
    <View style={{
            flexDirection: "row",
            alignItems: "center",
            flex: 1,
            marginTop:10
        }}>
            <Image
                style={{ width: 13, height: 13, borderRadius: 7 }}
                source={{
                    uri: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
                }}
            />

            <Text style={{
                color: "#624EDC",
                fontSize: 12,
                fontWeight: '500',
                marginLeft: 7
            }}>
               Ravi
            </Text>
            <Text style={{
                color: "#9C9C9C",
                fontSize: 12,
                fontWeight: '400',
                marginLeft: 20
            }}>
                2 days ago
            </Text>
            <Text style={{
                   color: "#624EDC",
                   fontSize: 13,
                   fontWeight: '500',
                   marginLeft: 20
            }}>
                Reply
            </Text>
        </View>
        <View style={{
        width: "100%",
        height: 1,
        backgroundColor: "#E9F5FF",
        marginTop: 10
    }} />
</View>
}
</>
     )
}

const Forums = () => {
    const loginId = useSelector((state) => state.User.userData)
    const [AllData, setAllData] = useState([])

      async function getForumsApi() {
        console.log('forems');
        try {
            let result = await LearningMeterials.ForumsGetList({"TenantCode":loginId.TENANTCODE,"RoleId":loginId.ROLEID,"DictionaryCode":loginId.DICTIONARYCODE})
            // let result = await LearningMeterials.ForumsGetList({"TenantCode":"92048666","RoleId":"3","DictionaryCode":"3a7872f9"})
          console.log('result',result);
          setAllData(result)
        } catch (error) {
            console.log(error);
        }
    }
    useEffect(() => {
        getForumsApi()
        // console.log(loginId);
    }, [])
    return (
        <View style={{
            flex: 1,
            backgroundColor: "#313131"
        }}>
            <View style={{
                flexDirection: "row",
                alignItems: "center",
                paddingLeft: 10,
                paddingVertical: 10,
                borderBottomColor: "#464646",
                borderBottomWidth: 1
            }}>
                <AntDesign onPress={() => Navigation.back()} name='arrowleft' size={33} color='#FFFFFF' />
                <Text style={{
                    fontSize: 17,
                    fontWeight: "700",
                    color: "#fff",
                    paddingLeft: 10
                }} >Fourms</Text>
            </View>
            <ScrollView style={{
                flex: 1,
                paddingHorizontal: 10,
                marginTop: 20
            }}>
                {AllData.map(a => <ExpexdableComponent item={a} />)}
              
                {/* <ExpexdableComponent/>
                <ExpexdableComponent/> */}
               
               
            </ScrollView>
        </View>
    )
}

export default Forums

const styles = StyleSheet.create({})