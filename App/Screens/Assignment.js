import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView
} from 'react-native'
import React, { useEffect, useState } from 'react'
import Navigation from '../Navigation/index';
import moment from 'moment';

import { vrscale, mdscale } from '../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
import Header from '../Components/Header';
import FileViewer from "react-native-file-viewer";
import DocumentPicker from "react-native-document-picker";
// import FileViewer from "react-native-file-viewer";
// import FileViewer from "react-native-file-viewer";
import RNFS from "react-native-fs";
{/** vectot icon */ }
import Palet from 'react-native-vector-icons/MaterialCommunityIcons'
import Iconu from 'react-native-vector-icons/AntDesign'
import Note from 'react-native-vector-icons/SimpleLineIcons'
import Feed from 'react-native-vector-icons/Foundation'
import Feedback from 'react-native-vector-icons/MaterialIcons'
import ServiceLearningMeterials from '../Service/LearningMeterials'
import Toast from 'react-native-simple-toast';

const Assignment = ({ route }) => {
    const [AssingmentsData, setAssingmentsData] = useState([])

    const renderAssingmentsData = ({ item }) => {
        async function OpenDoc() {
            try {
                const res = await DocumentPicker.pick({
                    allowMultiSelection: false,
                    type: [DocumentPicker.types.doc, DocumentPicker.types.pdf],
                })
                console.log("res",res);
                if (res && res.length!=0) {
                    Toast.show('Document Uploading Please Wait !', Toast.SHORT);
                }
                let docData = {
                    path: res[0].uri,
                    mime: res[0].type
                }
                let result = await ServiceLearningMeterials.StudentAssingmentsFileUplode(docData, 'file',
                    {
                        "AssignmentId": 1064,
                        "UserId": 99796678,
                        "ClientDocs": "ClientDocs",
                        "Course": 63
                    })
                // console.log("result");
                // console.log(result);
                Toast.show(result.message, Toast.SHORT, ['UIAlertController']);
            } catch (e) {
                // error
                console.log(e);
            }
        }
        async function ViewDoc() {
               console.log(item.ASSIGNMENT_UPLOAD);

            if (item.ASSIGNMENT_UPLOAD == null) {
                // console.log('no document found');
                
                Toast.show('no document found', Toast.SHORT, ['UIAlertController']);
            } else {


                let url = 'https://lmsqa.dhanushinfotech.com/api/Assignments' + item.ASSIGNMENT_UPLOAD.substring(1)
                console.log(url);
                function getUrlExtension(url) {
                    return url.split(/[#?]/)[0].split(".").pop().trim();
                }

                const extension = getUrlExtension(url);

                // Feel free to change main path according to your requirements.
                const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${extension}`;

                const options = {
                    fromUrl: url,
                    toFile: localFile,
                };
                RNFS.downloadFile(options)
                    .promise.then(() => FileViewer.open(localFile))
                    .then(() => {
                        // success
                        // console.log("done");
                    })
                    .catch((error) => {
                        // error
                        console.log(error);
                    });
            }
        }
        // async function ViewDoc() {
        //     //    console.log(item.EV_ASSIGNMENT_UPLOAD);

        //     if (item.EV_ASSIGNMENT_UPLOAD == null) {
        //         // console.log('no document found');

        //         Toast.show('no document found', Toast.SHORT, ['UIAlertController']);
        //     } else {


        //         let url = 'https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Uploaded%20Material' + item.EV_ASSIGNMENT_UPLOAD.substring(28)
        //         console.log(url);
        //         function getUrlExtension(url) {
        //             return url.split(/[#?]/)[0].split(".").pop().trim();
        //         }

        //         const extension = getUrlExtension(url);

        //         // Feel free to change main path according to your requirements.
        //         const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${extension}`;

        //         const options = {
        //             fromUrl: url,
        //             toFile: localFile,
        //         };
        //         RNFS.downloadFile(options)
        //             .promise.then(() => FileViewer.open(localFile))
        //             .then(() => {
        //                 // success
        //                 // console.log("done");
        //             })
        //             .catch((error) => {
        //                 // error
        //                 console.log(error);
        //             });
        //     }
        // }
        return (
            <View style={{
                //width: mdscale(330),
                // height: mdscale(154),
                backgroundColor: "#4D4D4D",
                marginHorizontal: mdscale(10),
                marginTop: 20,
                borderRadius: 10, elevation: 3
            }}>
                <View style={{ paddingLeft: 10, marginTop: 10 }}>
                    <Text style={{
                        color: "#FDFDFD",
                        fontSize: mdscale(16), fontFamily: "Poppins-Medium"
                    }}>{item.ASSIGNMENT_NAME}</Text>

                    <View style={{ flexDirection: 'row', }}>
                        {/* <Text style={{ color: "#AAAAAA", marginBottom: 7, fontSize: mdscale(12) }}>by Prof. Lavanya</Text> */}
                        <Text style={{
                            color: "#AAAAAA", marginBottom: 7,
                            paddingLeft: 0, fontSize: mdscale(12)
                        }}>{item.ASSIGNMENT_MAX_MARKS} Marks</Text>
                        <Text style={{
                            color: "#AAAAAA", marginBottom: 7,
                            paddingLeft: 8, fontSize: mdscale(12)
                        }}>Deadline: {item.ASSIGNMENT_END_DATE.slice(0,10)}</Text>
                         {/* {moment(item.ASSIGNMENT_END_DATE.slice(0,10)).format('MMMM d, YYYY')} */}
                        {/* <Text style={{
                            color: "#AAAAAA", marginBottom: 7,
                            paddingLeft: 8, fontSize: mdscale(12)
                        }}> {item.ASSIGNMENT_END_DATE.slice(0,10)}</Text> */}
                    </View>

                    {/* <Text style={{ color: "#FDFDFD", fontSize: mdscale(12) }}>This will be the description of the assesment if any given
                        {`\n`}by the trainerThis will be the description of the{`\n`}assesment if any given
                        by the trainer This will be the description {`\n`}of the assesment
                        if any {`\n`}given by the trainer</Text> */}
                </View>


                <View style={{
                    flexDirection: 'row', alignItems: 'center', marginTop: vrscale(10),
                    marginBottom: vrscale(10),
                    // backgroundColor: 'red',
                    width: '60%',
                    alignSelf: 'center', justifyContent: "space-between"
                }}>
                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#4D4D4D",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",
                        borderWidth: 1, borderColor: "#C8C8C8",
                    }}>
                        <Text onPress={ViewDoc} style={{ color: "#ffffff", fontSize: mdscale(15) }}> View</Text>
                    </View>

                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#ECECEC",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",

                    }} >
                        <Text onPress={OpenDoc} style={{ color: "#000000", fontSize: mdscale(15) }} >Upload</Text>
                    </View>
                </View>



            </View>


        )
    }

    async function getLarningMaterial() {
        try {
            // let result = await ServiceLearningMeterials.StudentAssingments(route.params)
            let result = await ServiceLearningMeterials.StudentAssingments({
                "CourseScheduleId": route.params.CourseScheduleId,
                "CourseId": route.params.CourseId,
                "UserId": route.params.UserId
            })
            // console.log( 'assss',result);
            setAssingmentsData(result)
        } catch (e) {
            console.log(e);
        }

    }


    useEffect(() => {
        // console.log(route.params);
        getLarningMaterial()
    }, [])
    return (
        <View style={{ flex: 1, backgroundColor: "#313131" }}>
            <Pressable onPress={() => Navigation.back()}>
                <View style={{
                    flexDirection: 'row', width: WIDTH,
                    borderBottomColor: "#464646", borderWidth: 1, height: vrscale(40),
                    alignItems: 'center'
                }}>
                    <Iconu name="arrowleft" color={"#FFFFFF"} size={mdscale(16)}
                        style={{ marginLeft: 10, alignSelf: 'center' }} />

                    <Text numberOfLines={1} style={{
                        color: "#ffffff",
                        fontSize: mdscale(15), fontWeight: '700', paddingLeft: 10, paddingRight: 10
                    }}>Assignments :{route.params.Name}</Text>
                </View>
            </Pressable>

            {/***1 st card */}
            <FlatList
                data={AssingmentsData}
                renderItem={renderAssingmentsData}
                keyExtractor={item => item.id}
            />
            <View style={{
                //width: mdscale(330),
                // height: mdscale(154),
                backgroundColor: "#4D4D4D",
                marginHorizontal: mdscale(10),
                marginTop: 20,
                borderRadius: 10, elevation: 3
            }}>


                {/* <View style={{ paddingLeft: 10, marginTop: 10 }}>
                    <Text style={{
                        color: "#FDFDFD",
                        fontSize: mdscale(16), fontFamily: "Poppins-Medium"
                    }}>Assesment Name will be shown here</Text>

                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{ color: "#AAAAAA", marginBottom: 7, fontSize: mdscale(12) }}>by Prof. Lavanya</Text>
                        <Text style={{
                            color: "#AAAAAA", marginBottom: 7,
                            paddingLeft: 8, fontSize: mdscale(12)
                        }}>20 Marks</Text>
                        <Text style={{
                            color: "#AAAAAA", marginBottom: 7,
                            paddingLeft: 8, fontSize: mdscale(12)
                        }}>Deadline: 24-06-2022</Text>
                    </View>

                    <Text style={{ color: "#FDFDFD", fontSize: mdscale(12) }}>This will be the description of the assesment if any given
                        {`\n`}by the trainerThis will be the description of the{`\n`}assesment if any given
                        by the trainer This will be the description {`\n`}of the assesment
                        if any {`\n`}given by the trainer</Text>
                </View> */}


                {/* <View style={{
                    flexDirection: 'row', alignItems: 'center', marginTop: vrscale(10),
                    marginBottom: vrscale(10),
                    // backgroundColor: 'red',
                    width: '60%',
                    alignSelf: 'center', justifyContent: "space-between"
                }}>
                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#4D4D4D",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",
                        borderWidth: 1, borderColor: "#C8C8C8",
                    }}>
                        <Text style={{ color: "#ffffff", fontSize: mdscale(15) }}> View</Text>
                    </View>

                    <View style={{
                        width: 100,
                        height: 30, backgroundColor: "#ECECEC",
                        borderRadius: 10, justifyContent: "center", alignItems: "center",

                    }}>
                        <Text style={{ color: "#000000", fontSize: mdscale(15) }}>Upload</Text>
                    </View>
                </View> */}



            </View>


            {/***1 st card */}




        </View>
    )
}

export default Assignment

const styles = StyleSheet.create({})