import {
    StyleSheet, Text, View, Dimensions,
    Image, Pressable, FlatList, StatusBar, SafeAreaView, ScrollView, Platform
} from 'react-native'
import React, { useEffect, useState } from 'react'
import Navigation from '../Navigation/index';
import VideoPlayer from 'react-native-video-player';
import Video from 'react-native-video';
import { vrscale, mdscale } from '../PixelRatio/index';
const WIDTH = Dimensions.get('window').width;
import Header from '../Components/Header';
import { useSelector, useDispatch } from 'react-redux'
{/** vectot icon */ }
//import Palet from 'react-native-vector-icons/AntDesign'
{/** vectot icon */ }
import Palet from 'react-native-vector-icons/MaterialCommunityIcons'
import Iconu from 'react-native-vector-icons/Entypo'
import Note from 'react-native-vector-icons/SimpleLineIcons'
import Feed from 'react-native-vector-icons/Foundation'
import Feedback from 'react-native-vector-icons/MaterialIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import LearningMeterials from '../Service/LearningMeterials';
// import TrackPlayer, { State, Capability, useProgress } from 'react-native-track-player';
import FileViewer from "react-native-file-viewer";
// import FileViewer from "react-native-file-viewer";
import RNFS from "react-native-fs";



const Mycoursedetail = ({ route, navigation }) => {
    const loginId = useSelector((state) => state.User)
    const [courseSelected, setCourseSelected] = useState(true);
    const [VideoPLay, setVideoPLay] = useState(true)
    const [ContentType, setContentType] = useState("")
    const [MATERIAL_NAME, setMATERIAL_NAME] = useState("")

    const [Comtent, setComtent] = useState("")
    const [Materialname, setMaterialname] = useState([])
    const [AudioPlay, setAudioPlay] = useState(true)
    const [course, setCourse] = useState(
        [
            {
                id: 1,
                number: 1,
                imageBook: require('../Assets/Images/zrtro04.jpg'),
                type: 'Vedio 08:13'

            },
            {
                id: 2,
                number: 2,
                imageBook: require('../Assets/Images/zrtro04.jpg'),
                type: 'Vedio 08:13'
            },
            {
                id: 3,
                number: 3,
                type: 'Vedio 08:13'

            },
            {
                id: 4,
                number: 4,
                type: 'Vedio 08:13'

            },
            {
                id: 5,
                number: 5,
                type: 'Vedio 08:13'

            },
            {
                id: 6,
                number: 6,
                type: 'Vedio 08:13'

            },
            {
                id: 3,
                number: 3,
                type: 'Vedio 08:13'

            },
            {
                id: 4,
                number: 4,
                type: 'Vedio 08:13'

            },
            {
                id: 5,
                number: 5,
                type: 'Vedio 08:13'

            },
            {
                id: 6,
                number: 6,
                type: 'Vedio 08:13'

            },
        ]
    )
    async function addAudio(audio) {
        const track2 = {
            url: 'https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload' + audio.substring(1), // Load media from the app bundle
            title: 'Coelacanth I',
            // artist: 'deadmau5',
            artwork: 'https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload' + audio.substring(1), // Load artwork from the app bundle
            // duration: 20000
        };
        // const tracks = await TrackPlayer.getQueue();
        // console.log('tracks',tracks.length);
        // // console.log(track2);
        TrackPlayer.reset();
        // if (tracks.length == 0) {

        // }else{
        //     TrackPlayer.stop()
        //     await TrackPlayer.remove([0,1]);

        // }
        // await TrackPlayer.add(track2)
        await TrackPlayer.add(track2)
        TrackPlayer.skip(2);
        await TrackPlayer.play()
        setAudioPlay(true)
        setContentType("mp3")

        // const state = await TrackPlayer.getState();
        // if (state === State.Playing) {
        //     console.log('The player is playing');
        // };


    }
    function getVideo(a) {

        // let arry = a.sort(function (a, b) { return b.OrderNo - a.OrderNo })

        // let d = ''
        // a.map((i)=>{
        //     if (d == '') {
        //         if (i.TYPE == "mp4") {
        //             let a = "https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Webinar%20Info"+i.MATERIAL_PATH.substring(8)
        //             // let a = "https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Webinar%20Info"+i.MATERIAL_PATH.substring(8)
        //             d = a
        //             setContentType("mp4")
        //         }
        //     }
        // })
        // setComtent(d)

        let index = a.findIndex(it => it.TYPE == 'mp4');

        if (index >= 0) {
            setComtent('https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Webinar%20Info' + a[index].MATERIAL_PATH.substring(8))
            setContentType("mp4")
            setMATERIAL_NAME(a[index].MATERIAL_NAME)
        }

        // a.map(i => {
        //     if (i.TYPE == "mp4") {
        //         let a = "https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Webinar%20Info"+i.MATERIAL_PATH.substring(8)
        //         // let a = "https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Webinar%20Info"+i.MATERIAL_PATH.substring(8)
        //         setComtent(a)
        //         setContentType("mp4")
        //         setMATERIAL_NAME(i.MATERIAL_NAME)
        //     }
        // });


    }
    const getLearningMeterials = async () => {
        try {
            let result = await LearningMeterials.LearningMeterials({ "CourseScheduleId": route.params.CourseScheduleId, "CourseId": route.params.CourseId, "UserId": loginId.userData.USERID })
            console.log(result);
            // let c = result.Materialname.sort(function (a, b) { return a.OrderNo - b.OrderNo })
            // let arry = result.Materialname.sort(function (a, b) { return b.OrderNo - a.OrderNo })
            // console.log({ "CourseScheduleId": route.params.CourseScheduleId, "CourseId": route.params.CourseId, "UserId": loginId.userData.USERID });
            setMaterialname(result.Materialname)
            getVideo(result.Materialname)
            // addAudio()
        } catch (e) {
            console.log(e);
        }
    }
    useEffect(() => {
        console.log(loginId.userData);
        getLearningMeterials()
        // console.log(route);
    }, [])

    const sorted = () => {
        let a;
        a = Materialname.sort(function (a, b) {
            return b.OrderNo > a.OrderNo ? -1
                : b.OrderNo < a.OrderNo ? 1
                    : 0
        })
        // console.log("a", a)
        return a;
    }

    function playContent(item) {
    
        setMATERIAL_NAME(item.MATERIAL_NAME)
        if (item.TYPE == "mp4") {
            let a = "https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Webinar%20Info" + item.MATERIAL_PATH.substring(8)
            console.log("video", a);
            setComtent(a)
            setContentType("mp4")
        } else if (item.TYPE == "mp3") {
            // addAudio(item.MATERIAL_PATH)
          
            let a = 'https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload' + item.MATERIAL_PATH.substring(1)
            console.log("video", a);
            setComtent(a)
            // setAudioPlay(false)
            setContentType("mp3")
        } else if (item.TYPE == "pdf") {
            // setContentType("pdf")
            let a = "https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Uploaded%20Material" + item.MATERIAL_PATH.substring(12)
            // console.log(a);
            Navigation.navigate("ViewPdf", { PdfSrc: a })
        } else if (item.TYPE == "docx") {
            // setContentType("pdf")
            let a = "https://lmsqa.dhanushinfotech.com/api/LearningMeterial/MaterialDownload/Uploaded%20Material" + item.MATERIAL_PATH.substring(12)
            // Navigation.navigate("ViewDoc",{PdfSrc:item.MATERIAL_PATH})
            OpenDoc(a)
            // console.log(item);
        } else {
            setContentType("")
        }
    }

    function OpenDoc(uri) {

        const url = uri;


        function getUrlExtension(url) {
            return url.split(/[#?]/)[0].split(".").pop().trim();
        }

        const extension = getUrlExtension(url);

        // Feel free to change main path according to your requirements.
        const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${extension}`;

        const options = {
            fromUrl: url,
            toFile: localFile,
        };
        RNFS.downloadFile(options)
            .promise.then(() => FileViewer.open(localFile))
            .then(() => {
                // success
                // console.log("done");
            })
            .catch((error) => {
                // error
                console.log(error);
            });
    }
    const CourseX = ({ item }) => {
        return (
            <Pressable onPress={() => playContent(item)}>
                <View style={[styles.flatBox]}>
                    <Text style={{
                        //marginLeft: mdscale(10),
                        textAlign: "center", width: 40,
                        // paddingHorizontal: 10
                        fontSize: 14, color: "#fff"
                    }}>{item.OrderNo}</Text>






                    <View style={{ paddingLeft: mdscale(1), flex: 1, }}>
                        <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                            {/* <Image source={ require('../Assets/Images/zrtro04.jpg')} style={{
                                borderRadius: 100,
                            }} /> */}
                            <Text style={{
                                flex: 1,
                                fontWeight: "500", fontSize: mdscale(14),
                                color: "#ffffff"
                            }}> {item.MATERIAL_NAME}
                            </Text>
                        </View>
                        <Text style={{
                            marginTop: 5,
                            fontWeight: "500", fontSize: mdscale(12),
                            color: "#CACACA"
                        }}> {item.TYPE}</Text>

                    </View>


                </View>
            </Pressable>
        )
    }
  
    // React.useEffect(() => {
    //     const unsubscribe = navigation.addListener('blur', () => {
    //         setVideoPLay(false)
    //         // StatusBar.setBackgroundColor('transparent')
    //     });
    //     return unsubscribe;

    // }, [navigation]);
    // React.useEffect(() => {
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         // setVideoPLay(true)
    //         // StatusBar.setBackgroundColor('transparent')
    //     });
    //     return unsubscribe;

    // }, [navigation]);
    return (
        <View style={{ flex: 1, backgroundColor: "#313131" }}>
            {/* <Image source={require('../Assets/Images/Groupvideo.jpg')}
                style={{ width: WIDTH }}
            /> */}
            <View style={{ backgroundColor: "#000", width: "100%", }}>
                <Text style={{ textAlign: "center", color: "#fff", paddingVertical: 3 }}>{MATERIAL_NAME} </Text>
            </View>
            {ContentType == "mp4" ?
                <>
                    {/* <Video
                        source={{ uri: Comtent }}
                        style={{
                            height: 200,
                            width: '100%',
                        }}
                        resizeMode="contain"
                        paused={true}
                        onLoad={(val) => console.log("val", val)}
                        onError={(e) => console.log("e", e)}
                        controls
                    /> */}
                    <VideoPlayer
                        video={{uri:Comtent}}
                        // video={{ uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4' }}
                        videoWidth={1600}
                        videoHeight={900}
                        onLoad = {(data) => console.log("data", data)}
                        onError = {(e) => console.log("e", e,e.error)}
                        // fullscreen={true}
                        // controls
                        fullScreenOnLongPress={true}
                        thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
                    />
                </>
                : ContentType == "mp3" ?

                    <>
                    <Video
                        source={{ uri: Comtent }}
                        style={{
                            height:0,
                            width: 0,

                        }}
                        resizeMode="contain"
                        paused={AudioPlay}
                        onLoad={(val) => console.log("val", val)}
                        onError={(e) => console.log("e", e)}
                        controls
                    />
                        <View style={{ width: "100%", height: 200, backgroundColor: "#000" }}>
                            {AudioPlay == true ?
                               
                                <Pressable
                                    onPress={() => {
                                        setAudioPlay(false)
                                    }}
                                    style={{ display: "flex", alignItems: "center", justifyContent: "center", width: 70, height: 70, backgroundColor: "#fff", borderRadius: 50, alignSelf: "center", marginTop: 50 }}>
                                    <Entypo name='controller-play' size={60} style={{ marginLeft: 10 }} color='#000' />
                                </Pressable>
                                :
                             <Pressable
                             onPress={() => {
                                 setAudioPlay(true)
                             }} style={{ display: "flex", alignItems: "center", justifyContent: "center", width: 70, height: 70, backgroundColor: "#fff", borderRadius: 50, alignSelf: "center", marginTop: 50 }}>
                             <Entypo name='controller-paus' size={60} color='#000' />
                         </Pressable>     
                            }
                        </View>

                    </> : ContentType == 'pdf' ?
                        <>
                            <Image source={require('../Assets/Images/Groupvideo.jpg')}
                                style={{ width: WIDTH }}
                            />
                            <Text>pdf</Text>
                        </> :
                        <>
                            <Text>No content </Text>
                        </>
            }

            {/** */}
            <View style={{
                Width: WIDTH,
                backgroundColor: "#313131"
            }}>

                <Text style={{
                    color: "#ffffff", fontSize: mdscale(17), marginVertical: 10,
                    paddingLeft: mdscale(20), fontWeight: "500"
                }}> {route.params.Name}</Text>

                {/* <Text style={{
                    color: "#C8C8C8", fontSize: mdscale(12),
                    paddingLeft: mdscale(20), fontWeight: "500"
                }}> by Prof.Lakshmi</Text> */}

                <View style={{ flexDirection: 'row', marginVertical: 10, marginTop: 20 }}>

                    <Pressable
                        style={{
                            marginLeft: 20,
                            borderBottomColor: '#fff',
                            borderBottomWidth: courseSelected ? 1.7 : 0
                        }}
                        onPress={() => setCourseSelected(true)}
                    >
                        <Text style={{
                            color: "#ffffff", fontSize: mdscale(15),
                            paddingHorizontal: 0, paddingRight: 5, fontWeight: "500",

                        }}> Content</Text>

                    </Pressable>

                    <Pressable
                        style={{
                            borderBottomColor: '#fff', marginLeft: 10,
                            borderBottomWidth: !courseSelected ? 1.7 : 0
                        }}
                        onPress={() => setCourseSelected(false)}
                    >

                        <Text style={{
                            color: "#ffffff", fontSize: mdscale(15),
                            paddingHorizontal: 0, paddingRight: 5, fontWeight: "500",
                        }}> More</Text>

                    </Pressable>
                </View>

            </View>


            {/*** */}
            {/* <View style={{
                height: '100%',
                width: WIDTH,
                //backgroundColor: "green",
                backgroundColor: "#313131"
            }}> */}
            {/* <View style={{marginTop:20,height:10,width:"100%"}} /> */}

            {
                courseSelected ?
                    <FlatList
                        //columnWrapperStyle={{ justifyContent: 'space-between' }}
                        showsVerticalScrollIndicator={false}
                        data={sorted()}
                        renderItem={CourseX}
                        //console.log(Cardx))
                        keyExtractor={item => item.id}
                    />
                    :
                    <View style={{
                        // height: '100%',
                        // width: WIDTH,
                        // //backgroundColor: "green",
                        // backgroundColor: "#313131"
                    }}>


                        {/* <View style={{ flexDirection: 'row' }}>
                            <Iconu name="dots-three-vertical" color={"#FFFFFF"} size={mdscale(16)}
                                style={{ marginLeft: 10 }} />
                            <Text style={{
                                color: "#ffffff", fontSize: mdscale(15),
                                paddingLeft: mdscale(10), fontWeight: "500",
                            }}> About Course</Text>
                        </View> */}

                        <View style={{
                            Width: '80%',
                            color: "#ffffff",
                            height: 2, lineHeight: 2
                        }} />
                        {/** */}
                        <Pressable onPress={() => {
                            // setAudioPlay(false)
                            // TrackPlayer.stop()
                            Navigation.navigate('Assignment', { CourseScheduleId: route.params.CourseScheduleId, CourseId: route.params.CourseId, UserId: loginId.userData.USERID, Name: route.params.Name })
                        }


                        } >
                            <View style={{ flexDirection: 'row', marginTop: vrscale(2) }}>

                                <Palet name="clipboard-text-outline" color={"#FFFFFF"} size={mdscale(16)}
                                    style={{ marginLeft: 10 }} />
                                <Text style={{
                                    color: "#ffffff", fontSize: mdscale(15),
                                    paddingLeft: mdscale(10), fontWeight: "500",
                                }}> Assignment</Text>

                            </View>
                        </Pressable>
                        {/** */}

                       

                        {/** */}

                        <Pressable onPress={()=> Navigation.navigate('Forums')} style={{ flexDirection: 'row', marginTop: vrscale(20) }}>
                            <Palet name="chat-outline" color={"#FFFFFF"} size={mdscale(16)}
                                style={{ marginLeft: 10 }} />
                            <Text style={{
                                color: "#ffffff", fontSize: mdscale(15),
                                paddingLeft: mdscale(10), fontWeight: "500",
                            }}> Forums</Text>
                        </Pressable>

                        {/** */}

                        <Pressable onPress={()=> Navigation.navigate('Faqs')} style={{ flexDirection: 'row', marginTop: vrscale(20) }}>
                            <Feed name="comment-quotes" color={"#FFFFFF"} size={mdscale(16)}
                                style={{ marginLeft: 10 }} />
                            <Text style={{
                                color: "#ffffff", fontSize: mdscale(15),
                                paddingLeft: mdscale(10), fontWeight: "500",
                            }}> FAQ's</Text>
                        </Pressable>

                        {/** */}

                       
                    </View>
            }



            {/* </View> */}

        </View>
    )
}

export default Mycoursedetail

const styles = StyleSheet.create({
    flatBox: {
        flexDirection: 'row',
        paddingVertical: 10, alignItems: 'center',
    }
})