import React, {Component} from 'react';
import {
    View, Text, StyleSheet, TextInput,
} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { STANDARD_SCREEN_HEIGHT, WHITE_COLOR, SMALL_FONT_SIZE, LOGIN_BTN_COLOR, 
        LOGIN_BTN_COLOR_LIGHT, FONT_REGULAR, NORMAL_FONT_SIZE, DARD_GRAY_1, BLACK_COLOR } from '../../../Utils/utils/AppConst';
import {Input} from 'react-native-elements';


const AppTextField = (props) => {
    let {value, changeText, placeHolder, containerStyle, textFiledStyle, viewStyle, 
            multiline, inputStyle, keyboardType, maxLength, inputContainerStyle, label,
            error
        } = props;
    return (<>
        <View style={[{
                    padding: RFValue(20, STANDARD_SCREEN_HEIGHT), 
                    paddingVertical: 10,
                }, {...containerStyle},]}>
            {label &&  <Text style={{
                fontSize: RFValue(NORMAL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                fontFamily: FONT_REGULAR,
                color: BLACK_COLOR,
                marginVertical: RFValue(4, STANDARD_SCREEN_HEIGHT)
            }}>{label}</Text>}

            <View style={[{...styles.viewStyle}, {...viewStyle}]}>
                <Input
                    //{...props}
                    containerStyle={[styles.textFiledStyle, {...textFiledStyle}]}
                    contextMenuHidden={true}
                    inputContainerStyle={[{
                            justifyContent:'center',
                            alignItems:'center',
                            borderWidth:0,
                            borderBottomWidth: 0,
                            
                    },inputContainerStyle ]}
                    inputStyle={[{
                        alignItems:'center',
                        justifyContent:'center',
                        alignSelf:'center',
                        //marginVertical: RFValue(12, STANDARD_SCREEN_HEIGHT),
                        color: 'black',
                        fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                        textAlignVertical: 'center',
                        fontFamily: FONT_REGULAR,
                       
                    }, {...inputStyle}]}
                    defaultValue={value}
                    placeholder={placeHolder}
                    value={value}
                    onChangeText={(text) => {
                        changeText(text);
                    }}
                    editable={true}
                    placeholderTextColor={LOGIN_BTN_COLOR_LIGHT}
                    multiline={multiline ? true : false}
                    keyboardType={keyboardType ? keyboardType : "default"}
                    maxLength={maxLength}
                    />
            </View>
        </View>
        {error && <Text style={{
                marginLeft: RFValue(24, STANDARD_SCREEN_HEIGHT)/**ICON Size*/, 
                fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT), 
                color: 'red',
                alignSelf:'flex-start',
                marginVertical: RFValue(5, STANDARD_SCREEN_HEIGHT) 
        }}>* {error}</Text>}   
        </>
    );
}

// 
const styles = StyleSheet.create({
    textFiledStyle: {
        height: '100%',
        //width: '100%',
        //padding: 10,
        paddingLeft: RFValue(20, STANDARD_SCREEN_HEIGHT),
        paddingRight: RFValue(10, STANDARD_SCREEN_HEIGHT),
    },
    viewStyle: {
        height: RFValue(44, STANDARD_SCREEN_HEIGHT),
       // width: '100%',
        borderRadius: RFValue(22, STANDARD_SCREEN_HEIGHT),
        backgroundColor: WHITE_COLOR,
        shadowColor: LOGIN_BTN_COLOR,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1.84,
        elevation: 5,
    },
});


export default AppTextField;
