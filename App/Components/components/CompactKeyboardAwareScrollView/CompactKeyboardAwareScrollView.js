import React from 'react';
import {Keyboard, TouchableWithoutFeedback, View} from 'react-native'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';



const CompactKeyboardAwareScrollView = (props) => {
    const {containerStyle} = props;
    return (<View style={[{...containerStyle}]}>
            <KeyboardAwareScrollView 
                keyboardShouldPersistTaps={"always"}
                style={[{flex:1}, {...containerStyle}]}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}> 
                {props.children}
            </TouchableWithoutFeedback>
        </KeyboardAwareScrollView></View>);
}

export default CompactKeyboardAwareScrollView;