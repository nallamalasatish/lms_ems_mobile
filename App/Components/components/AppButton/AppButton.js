import React, {Component, useState} from 'react';
import {
    View, Text, StyleSheet, TextInput, TouchableOpacity,
} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT, THEME_COLOR, WHITE_COLOR, LOGIN_BTN_COLOR, NORMAL_FONT_SIZE, FONT_REGULAR } from '../../../Utils/utils/AppConst';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP } from 'react-native-responsive-screen';

const AppButton = (props)=> {
    const {label, containerStyle, textStyle, mainContainerStyle, colors} = props;
    return  <LinearGradient 
            start={{x: 0, y: 0}} end={{x: 1, y: 0}}
            colors={colors} style={[
            {
                //padding: RFValue(30, STANDARD_SCREEN_HEIGHT), 
                //paddingVertical: 10,
                
            }, 
            {...styles.viewStyle},
            {...mainContainerStyle}, 
            {...containerStyle},
            ]}>
                <TouchableOpacity
                        style={[{justifyContent:'center', alignItems:'center',  height: RFValue(44, STANDARD_SCREEN_HEIGHT),}]}
                        {...props}
                        >
                    <Text style={[{
                                color: WHITE_COLOR, 
                                fontSize: RFValue(NORMAL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                                fontFamily: FONT_REGULAR
                                }, {...textStyle}]}>{label}</Text>
                </TouchableOpacity>
            </LinearGradient>
}

const styles = StyleSheet.create({
    textFiledStyle: {
        //flex:1,
        height: '100%',
        //width: '100%',
        //padding: 10,
        paddingLeft: RFValue(20, STANDARD_SCREEN_HEIGHT),
        paddingRight: RFValue(20, STANDARD_SCREEN_HEIGHT),
    },
    viewStyle: {
        height: RFValue(44, STANDARD_SCREEN_HEIGHT),
        //width: '100%',
        borderRadius: RFValue(22, STANDARD_SCREEN_HEIGHT),
        backgroundColor: LOGIN_BTN_COLOR,
    },
});

export default AppButton;

