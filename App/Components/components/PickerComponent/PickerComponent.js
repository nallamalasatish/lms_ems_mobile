import React, {useState} from 'react';
import  { View, Platform } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RFValue } from 'react-native-responsive-fontsize';
import { CheckBox } from 'react-native-elements';
import { LIGHT_GRAY, FONT_REGULAR, BLACK_COLOR, STANDARD_SCREEN_HEIGHT, NORMAL_FONT_SIZE, THEME_COLOR, GRAY_COLOR } from '../../../Utils/utils/AppConst';
import commonStyles from '../../../Utils/styles/GlobalStyles';

const PickerComponent = (props) => {
    const {containerStyle, onItemChecked, item, key} = props;
    return <View style={[{
            borderWidth:0,
            borderColor: LIGHT_GRAY,
            //borderBottomWidth:1,
            //paddingVertical: RFValue(16, STANDARD_SCREEN_HEIGHT),
            flexDirection: 'row',
            alignItems:'center',
            margin: RFValue(10, STANDARD_SCREEN_HEIGHT)
        }, {...containerStyle}, {...commonStyles.card}, {...commonStyles.shadowEffect}]}
        key={key}
        >
         <CheckBox
            iconLeft
            containerStyle={{
                flex:1,
                height: RFValue(40, STANDARD_SCREEN_HEIGHT), 
                backgroundColor: 'trasparent', 
                borderWidth: 0,
                justifyContent:'center',
                padding: Platform.OS == 'ios' ? 0 : RFValue(8, STANDARD_SCREEN_HEIGHT),
                }}
            title={item.value}
            textStyle={{
                width: wp('70%'),
                color:BLACK_COLOR, 
                fontFamily: FONT_REGULAR,
                fontSize: RFValue(NORMAL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
            }}
            checkedColor={THEME_COLOR}
            uncheckedColor={GRAY_COLOR}
            checked={item.isChecked}
            onPress={() => {
                onItemChecked(item, !item.isChecked)
            }}/>
    </View>
}

export default  PickerComponent;


