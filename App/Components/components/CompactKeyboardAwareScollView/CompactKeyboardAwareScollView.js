import React from 'react';

import { listenToKeyboardEvents } from '@codler/react-native-keyboard-aware-scroll-view'
import { ScrollView } from 'react-native';

const CompactKeyboardAwareScollView = listenToKeyboardEvents((props) => <ScrollView {...props} />);

export default CompactKeyboardAwareScollView;