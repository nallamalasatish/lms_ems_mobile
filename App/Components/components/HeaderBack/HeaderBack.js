import React, { useEffect, Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Platform,
    StyleSheet,
    Image,
    BackHandler
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from 'react-native-responsive-fontsize';
import {
    EXTRA_LARGE_FONT_SIZE_2, STANDARD_SCREEN_HEIGHT,
    THEME_COLOR, WHITE_COLOR, FONT_REGULAR,
    BLACK_COLOR, FONT_BOLD, EXTRA_SMALL_FONT_SIZE_1,
    NORMAL_FONT_SIZE, EXTRA_SMALL_FONT_SIZE, SMALL_FONT_SIZE,
    FONT_MEDIUM, TOOLBAR_HEIGHT, EXTRA_LARGE_FONT_SIZE, LARGE_FONT_SIZE, THEME_COLOR_2, THEME_COLOR_3, TOOLBAR_1, TOOLBAR_2
} from '../../../Utils/utils/AppConst';
import commonStyles from '../../../Utils/styles/GlobalStyles';
import { useNavigation, useRoute } from '@react-navigation/native';
// import { LOGIN_PAGE } from '../../routes/RouteConst';
import { AppOkCancelAlert } from '../../../Utils/utils/AlertHelperr';
import api from '../../../Service/api';
import LinearGradient from 'react-native-linear-gradient';

export class HeaderBackClass extends Component {
    constructor(props) {
        super(props);
        this.onBackPressed = this.onBackPressed.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener(
            'hardwareBackPress',
            this.onBackPressed,
        );
    }

    componentWillUnmount() {
        BackHandler.removeEventListener(
            'hardwareBackPress',
            this.onBackPressed,
        );
    }

    onLogoutPress = async () => {
        AppOkCancelAlert("Do you want to Logout ?", async () => {
            await api.user.logout();
            //this.props.store.dispatch(clearStore())
            this.props.navigation.reset({
                index: 0,
                routes: [{ name: LOGIN_PAGE }]
            })
        }, () => {

        })
    }

    onBackPressed = () => {
        if (this.props.customBackEvent) {
            this.props.customBackEvent();
            return true;
        }
        this.props.navigation.goBack()
        return true;
    }


    render() {
        const { letfIcon, leftIconPress, title,
            rightIcon, rightIconPress,
            isHiddenRightItem, customBackEvent } = this.props;
        return <LinearGradient
            start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
            colors={[TOOLBAR_1, TOOLBAR_2]}
            style={styles.mainContainer}>
            <View style={styles.container}>
                <TouchableOpacity
                    style={[commonStyles.centerView, styles.leftIconContainer, styles.iconContainer]}
                    onPress={() => {
                        this.onBackPressed()
                    }}
                >
                    <Icon name={"chevron-left"}
                        color={WHITE_COLOR}
                        size={RFValue(24, STANDARD_SCREEN_HEIGHT)} />
                </TouchableOpacity>

                <View style={styles.textContainer}>
                    <Text
                        numberOfLines={1}
                        style={styles.textMain}>{title}</Text>
                </View>
            </View>
        </LinearGradient>
    }
}

const HeaderBack = (props) => {
    const navigation = useNavigation();

    return (
        <HeaderBackClass
            navigation={navigation}
            {...props}
        />
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        width: wp('100%'),
        height: TOOLBAR_HEIGHT,
        //backgroundColor: THEME_COLOR,
    },
    container: {
        width: wp('100%'),
        height: TOOLBAR_HEIGHT,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconContainer: {
        width: RFValue(40, STANDARD_SCREEN_HEIGHT),
        height: RFValue(40, STANDARD_SCREEN_HEIGHT)
    },
    leftIconContainer: {
        marginLeft: 10,
        borderRadius: 20,
        overflow: 'hidden',
        borderWidth: 0
    },

    textContainer: {
        flex: 1,
        marginLeft: RFValue(4, STANDARD_SCREEN_HEIGHT)
    },

    textMain: {
        fontSize: RFValue(LARGE_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
        fontFamily: FONT_MEDIUM,
        color: WHITE_COLOR
    },

    textDesc: {
        fontSize: RFValue(EXTRA_SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
        fontFamily: FONT_BOLD,
        color: WHITE_COLOR
    },

    badgeContainer: {
        top: 2,
        left: RFValue(20, STANDARD_SCREEN_HEIGHT),
        zIndex: RFValue(20, STANDARD_SCREEN_HEIGHT),
        borderRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
        overflow: 'hidden',
        position: 'absolute',
        width: RFValue(20, STANDARD_SCREEN_HEIGHT),
        height: RFValue(20, STANDARD_SCREEN_HEIGHT),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ee841f'
    },
    badgeText: {
        color: WHITE_COLOR,
        fontFamily: FONT_REGULAR,
        fontSize: EXTRA_SMALL_FONT_SIZE_1
    }
})

export default HeaderBack;