import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  TouchableHighlight
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {RFValue} from 'react-native-responsive-fontsize';
import { THEME_COLOR, STANDARD_SCREEN_HEIGHT, WHITE_COLOR, EXTRA_LARGE_FONT_SIZE_2, FONT_BOLD, EXTRA_SMALL_FONT_SIZE, FONT_REGULAR,
   EXTRA_SMALL_FONT_SIZE_1, BLACK_COLOR, THEME_COLOR_2, 
   THEME_COLOR_3, TOOLBAR_1, TOOLBAR_2, TRANSPARENT, TOOLBAR_HEIGHT, FONT_MEDIUM } from '../../../Utils/utils/AppConst';
import { withGlobalize } from 'react-native-globalize';
import IntlProvider from '../../../Utils/utils/IntlProvider';
import commonStyles from '../../../Utils/styles/GlobalStyles';
import { useNavigation } from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import { Icons } from '../../../Assets/assets';
// import { IconLogout } from '../../assets/images/icons';
import { AppOkCancelAlert } from '../../../Utils/utils/AlertHelper';
import api from '../../../Service/api';
// import { LOGIN_PAGE } from '../../routes/RouteConst';
import RadioButtonRN from 'radio-buttons-react-native';
import { set } from 'react-native-reanimated';
import CountDown from 'react-native-countdown-component';
export const homeHeaderIntlProvider = (props) => ({
    appName: IntlProvider(props, 'app/appName'),
    logoutMsg: IntlProvider(props, 'app/logoutMsg'),
})


const HomeHeader = (props) => {
    const intl = homeHeaderIntlProvider(props);
    const navigation = useNavigation(); 
         
    const logout = () => {
      AppOkCancelAlert(
        intl.logoutMsg,
        async () => {
          await api.user.logout();
          navigation.reset({
            index: 0,
            routes: [{name: LOGIN_PAGE}],
          });
        },
        () => {},
      );
    }


    return  <LinearGradient  
            start={{x: 0, y: 0}} end={{x: 1, y: 0}}
              colors={[TOOLBAR_1, TOOLBAR_2]}
              style={[styles.container, {
              }]}>
            <TouchableOpacity
                style={[
                    commonStyles.centerView,
                    styles.leftIconContainer,
                    styles.iconContainer,
                ]}
                onPress={() => {
                  navigation.openDrawer();
                }}
                
                >
                <Icon
                    name={'bars'}
                    color={BLACK_COLOR}
                    size={RFValue(20, STANDARD_SCREEN_HEIGHT)} 
                />
            </TouchableOpacity>

            <View style={styles.textContainer}>
                <Text style={styles.textMain}>{intl.appName}</Text>
            </View>

            <TouchableOpacity
                style={[
                    commonStyles.centerView,
                    
                    styles.iconContainer,
                    {
                      backgroundColor: TRANSPARENT,
                      marginRight: RFValue(10, STANDARD_SCREEN_HEIGHT)
                    }
                ]}
                onPress={() => {
                  logout()
                }}>
                  <Image
                    source={Icons.IconLogout}
                    resizeMode={"contain"}
                    style={{
                      width: RFValue(30, STANDARD_SCREEN_HEIGHT),
                      height: RFValue(30, STANDARD_SCREEN_HEIGHT),
                      padding: RFValue(4, STANDARD_SCREEN_HEIGHT),
                      resizeMode:"contain"
                    }}
                  />
          </TouchableOpacity>
    </LinearGradient>
}

const styles = StyleSheet.create({
    container: {
      width: wp('100%'),
      height: TOOLBAR_HEIGHT,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: TRANSPARENT,
    },
    iconContainer: {
      width: RFValue(40, STANDARD_SCREEN_HEIGHT),
      height: RFValue(40, STANDARD_SCREEN_HEIGHT),
    },
    leftIconContainer: {
      marginLeft: 10,
      backgroundColor: WHITE_COLOR,
      borderRadius: 20,
      //overflow: 'hidden',
      borderWidth: 0,
    },
  
    textContainer: {
      flex: 1,
      marginLeft: RFValue(20, STANDARD_SCREEN_HEIGHT),
    },
  
    textMain: {
      fontSize: RFValue(EXTRA_LARGE_FONT_SIZE_2, STANDARD_SCREEN_HEIGHT),
      fontFamily: FONT_BOLD,
      color: WHITE_COLOR,
    },
  
    textDesc: {
      fontSize: RFValue(EXTRA_SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
      fontFamily: FONT_MEDIUM,
      color: WHITE_COLOR,
    },
  
    badgeContainer: {
      top: 2,
      left: RFValue(20, STANDARD_SCREEN_HEIGHT),
      zIndex: RFValue(20, STANDARD_SCREEN_HEIGHT),
      borderRadius: 10,
      overflow: 'hidden',
      position: 'absolute',
      width: RFValue(20, STANDARD_SCREEN_HEIGHT),
      height: RFValue(20, STANDARD_SCREEN_HEIGHT),
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#ee841f',
    },
    badgeText: {
      color: WHITE_COLOR,
      fontFamily: FONT_REGULAR,
      fontSize: EXTRA_SMALL_FONT_SIZE_1,

    },
    floatCard: {
      position: 'absolute',
      width: wp('90%'),
      backgroundColor: WHITE_COLOR,
      bottom: RFValue(-50, STANDARD_SCREEN_HEIGHT),
      alignSelf: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      height: RFValue(100, STANDARD_SCREEN_HEIGHT),
      zIndex: 100,
      borderRadius: 10,
    },
  });

export default withGlobalize(HomeHeader);