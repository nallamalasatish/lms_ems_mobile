import React from 'react';
import { WHITE_COLOR, NORMAL_FONT_SIZE, STANDARD_SCREEN_HEIGHT, EXTRA_LARGE_FONT_SIZE, TOOLBAR_1, TOOLBAR_2, DARD_GRAY_1, DARY_GRAY_2 } from '../../../Utils/utils/AppConst';
import  LinearGradient from "react-native-linear-gradient";
import {View, Text, Image} from 'react-native';
import {  Accordion, Icon } from "native-base";
import {widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from 'react-native-responsive-fontsize';
import commonStyles from '../../../Utils/styles/GlobalStyles';

const ExpandRenderHeader = (props) => {
    const {item, expanded, image} = props;
    return (
        <LinearGradient 
            start={{x: 0, y: 0}} end={{x: 1, y: 0}}
            colors={expanded ? [TOOLBAR_1, TOOLBAR_2] : [DARD_GRAY_1, DARY_GRAY_2]}
            style={[{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                //marginBottom: 5,
                //marginTop: 5,
                backgroundColor: 'transparent',
                borderRadius: 10,
            },   {...commonStyles.card},
            {...commonStyles.shadowEffect}]}>
            <View style={{flex:1, padding: 2, alignItems:'center', flexDirection: 'row' }}>
                {image &&  <Image
                    source={image}
                    style={{
                        width: RFValue(30, STANDARD_SCREEN_HEIGHT),
                        height: RFValue(30, STANDARD_SCREEN_HEIGHT),
                        marginHorizontal: RFValue(10, STANDARD_SCREEN_HEIGHT)
                    }}/>
                }
               
                <Text style={{ 
                        ontWeight: "400", 
                        fontSize: RFValue(NORMAL_FONT_SIZE, STANDARD_SCREEN_HEIGHT), 
                        color: WHITE_COLOR 
                        }}>
                    {item.title}
                </Text>
            </View>
            {expanded
                ? <Icon type="AntDesign" style={{ fontSize: EXTRA_LARGE_FONT_SIZE, color: WHITE_COLOR }} name="up" />
                : <Icon type="AntDesign" style={{ fontSize: EXTRA_LARGE_FONT_SIZE, color: WHITE_COLOR  }} name="right" />}
        </LinearGradient>
    );
}

export default ExpandRenderHeader;