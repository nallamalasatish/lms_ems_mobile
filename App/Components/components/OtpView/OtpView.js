
import React from 'react';
import {View, StyleSheet, ActivityIndicator, Modal,Text, TouchableOpacity } from "react-native";
import { THEME_COLOR, STANDARD_SCREEN_HEIGHT, FONT_MEDIUM, NORMAL_FONT_SIZE, TOOLBAR_2, LOGIN_BTN_COLOR, BLACK_COLOR, SMALL_FONT_SIZE, FONT_REGULAR, WHITE_COLOR } from '../../../Utils/utils/AppConst';
import Dialog, { SlideAnimation, DialogContent,DialogFooter, DialogButton } from 'react-native-popup-dialog';
import { isNullOrUndefined } from 'util';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from 'react-native-responsive-fontsize';
import { Formik } from 'formik';
import { withGlobalize } from 'react-native-globalize';
import {otpViewInIntlProvider, OTPViewInFormInitialValues, OTPViewInFormValidator} from './OtpViewHelper';
import AppTextField from '../AppTextInput/AppTextInput';

const OtpView = (props) => {
  const intl = otpViewInIntlProvider(props); 
  const {
    message,
    sendOtp,
    visible,
    onCloseOtpView,
    mobile,
    otpError,
    verifyOtp
  } = props;
 
  const onSubmitForm = (values) => {
    verifyOtp(values);
  }

  const resendOtp = (mobile) => {
    sendOtp(mobile)
  }

  return (<View style={{flex:1, backgroundColor: WHITE_COLOR}}>
        <Formik 
            initialValues={OTPViewInFormInitialValues(props)}
            validationSchema={OTPViewInFormValidator(intl)}
            onSubmit={values => onSubmitForm(values)}>
                {({values, handleChange, setFieldValue, errors, touched, setFieldTouched, isValid, handleSubmit}) => (
                <Dialog
                    visible={visible}
                    dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                footer={
                    <DialogFooter>
                        <DialogButton
                        textStyle={{color:LOGIN_BTN_COLOR}}
                        text="CANCEL"
                        onPress={() => {
                            setFieldValue('otp', '');
                            onCloseOtpView()
                        }}
                        />
                        <DialogButton
                        textStyle={{color:LOGIN_BTN_COLOR}}
                        text="CONFIRM"
                        onPress={() => {
                            if(isValid){
                                handleSubmit();
                            }
                        }}
                        />
                    </DialogFooter>
                }>
                <DialogContent>
                    <View style={{width:wp('80%')}}>
                        <View>
                            <Text style={{color: BLACK_COLOR,
                                fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                                fontFamily: FONT_REGULAR,
                                marginVertical: RFValue(8, STANDARD_SCREEN_HEIGHT),
                                textAlign:'center'
                            }}>{message}</Text>
                        </View>
                        <View style={{
                            //marginHorizontal: RFValue(8, STANDARD_SCREEN_HEIGHT),
                            marginTop: RFValue(8, STANDARD_SCREEN_HEIGHT)
                            }}>
                            <AppTextField 
                                placeHolder={intl.enterOtp}
                                value={values.otp.replace(/[^0-9]/g, '')}
                                //value={"9997011740"}
                                changeText={handleChange('otp')}
                                onBlur={() => setFieldTouched('otp')}
                                keyboardType={"numeric"}
                                maxLength={10}
                                autoCapitalize={"none"}
                                touchedValue={touched.otp}
                                errorValue={errors.otp}
                                containerStyle={{
                                    padding: RFValue(5, STANDARD_SCREEN_HEIGHT), 
                                    paddingVertical:  RFValue(10, STANDARD_SCREEN_HEIGHT),
                                }}
                            />
                             {(touched.otp && errors.otp) || otpError  &&
                                    <Text style={{marginLeft: 24/**ICON Size*/, fontSize: RFValue(SMALL_FONT_SIZE, STANDARD_SCREEN_HEIGHT), color: 'red' }}>* {otpError || errors.otp }</Text>
                            }

                            <TouchableOpacity 
                                style={{
                                    marginTop:RFValue(0, STANDARD_SCREEN_HEIGHT),
                                    backgroundColor: WHITE_COLOR,
                                    padding: RFValue(8, STANDARD_SCREEN_HEIGHT),
                                    height: RFValue(40, STANDARD_SCREEN_HEIGHT),
                                    borderRadius: RFValue(10, STANDARD_SCREEN_HEIGHT),
                                    justifyContent:'center',
                                    alignItems:'center'
                                }}
                                onPress={resendOtp}
                                >
                                    <Text style={{
                                        color: LOGIN_BTN_COLOR,
                                        fontSize: RFValue(NORMAL_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
                                        fontFamily: FONT_REGULAR,
                                        textAlign:'center'
                                    }}>{intl.resendOtpValidation}</Text> 
                            </TouchableOpacity>
                        </View>
                    </View>
                </DialogContent>
            </Dialog>
            )}
        </Formik>  
    </View>)
}

export default withGlobalize(OtpView);