import * as yup from 'yup';

import IntlProvider from '../../../Utils/utils/IntlProvider';

export const otpViewInIntlProvider = (props) => ({
    appName: IntlProvider(props, 'app/appName'),
    noInternet: IntlProvider(props, 'app/noInternet'),
    somethingWentWrong: IntlProvider(props, 'app/somethingWentWrong'),
    onlyDigitValidation: IntlProvider(props, 'inputValidation/onlyDigit'),
    otpLengthValidation: IntlProvider(props, 'inputValidation/otpLength'),
    otpRequiredValidation: IntlProvider(props, 'inputValidation/otpRequired'),
    resendOtpValidation: IntlProvider(props, 'inputPlaceHolder/resendOtp'),
    enterOtp: IntlProvider(props, 'inputPlaceHolder/enterOtp'),
})
export const OTPViewInFormInitialValues = (props) => ({
    otp: ''
});
  
export const OTPViewInFormValidator = (intl) => {
      
      return (
          yup.object().shape({
            otp: yup.number()
                    .typeError(intl.onlyDigitValidation)
                    .min(6, intl.otpLengthValidation)
                    .required(intl.otpRequiredValidation),  
          })
      )
}