import { StyleSheet, Text, View, Dimensions, Image, SafeAreaView, ImageBackground, Pressable } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import Navigation from '../Navigation'

import React, { useEffect } from 'react'

import { COLORS } from '../Constants/Colors';
import { vrscale, mdscale } from '../PixelRatio/index';
import { log } from 'react-native-reanimated';
const WIDTH = Dimensions.get('window').width;




const Header = (props) => {
    useEffect(()=>{
console.log(props);
console.log("subham");
    },[])
    return (

        <ImageBackground
            source={require('../Assets/Images/Group96.png')}
        >
            <View style={styles.header}>
                <View style={{}}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        {props.back == false ?
                            <Pressable
                                onPress={() => Navigation.openDrawer() }>
                                <Image source={require("../Assets/Images/Group290.png")}  />
                                 </Pressable> :

                            <Icon
                                onPress={() => props.back == false ? Navigation.openDrawer() : Navigation.back()}
                                name={props.back == false ? "align-left" : 'chevron-left'}
                                size={26}
                                color={'#FFFFFF'}

                            />}

                        <View style={{ paddingLeft: mdscale(16), }}>
                            <Text
                                style={{
                                    color: '#FFFFFF', fontFamily: 'Poppins-Medium',
                                    fontSize: 16

                                }}
                            >
                                {props.name}
                                </Text>
                            <Text
                                style={{
                                    color: '#B4B9E8', fontFamily: 'Poppins-Medium',
                                    fontSize: 12,

                                }}
                            >{props.namey}</Text>
                        </View>

                        <Text
                            style={{
                                color: '#FFFFFF', fontFamily: 'Poppins-Medium',
                                fontSize: 16,
                                textAlign: 'center',
                                //marginLeft: mdscale(50)
                            }}
                        >{props.pagename}</Text>

                    </View>

                </View>
                <View style={{}}>

                    {props.Home ?
                        <View
                            style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Material name="bell-badge" size={25} color={'#FFFFFF'} style={{ marginLeft: mdscale(12) }} />
                            <Image source={require('../Assets/Images/cutsty2.jpg')}
                                style={{
                                    width: mdscale(33), height: mdscale(32),
                                    borderRadius: 100,
                                    marginLeft: mdscale(7)
                                }} />
                        </View>
                        : null}



                </View>
            </View>
        </ImageBackground>

    )
}

export default Header

const styles = StyleSheet.create({
    header: {

        //backgroundColor: COLORS.headerColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        alignItems: 'center',
        height: 60,
        width: '100%'
    },
})