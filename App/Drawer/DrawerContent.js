import { StyleSheet, Text, View, Dimensions, Image ,Pressable} from 'react-native'
import React from 'react';

import LinearGradient from 'react-native-linear-gradient';

{/**vector icon */ }
import Poll from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import { vrscale, mdscale } from '../PixelRatio/index';
import { useSelector, useDispatch } from 'react-redux'
import { setuser, logout } from '../Redux/reducer/User'
import AuthService from '../Service/Auth'

import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';

import Navigation from '../Navigation/index';

const DrawerContent = ({ ...props }) => {
    const login_status = useSelector((state) => state.User.login_status)
    const dispatch = useDispatch()
    const logoutUser = async()=>{
        AuthService.logout()
       dispatch(logout())
    }
    return (
        <View style={{ flex: 1, backgroundColor: "#3E48A0", }}>
            <DrawerContentScrollView {...props}>

                <LinearGradient
                    colors={['#1C69B1', '#3E48A0']}

                    style={{
                        marginTop: mdscale(-4),
                        flexDirection: 'row',
                    }}>

                    <Entypo name="cross" color={"#FFFFFF"} size={mdscale(20)}
                        style={{ marginTop: 20 }}
                    onPress={() => Navigation.closeDrawer()}
                    />
             
                    <Poll name="logout" color={"#FFFFFF"} size={mdscale(20)}
                        style={{ marginLeft: 200, marginTop: 30 }}
                    onPress={logoutUser}
                    /> 

                </LinearGradient>
                {/* <DrawerItemList{...props} /> */}


                <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: mdscale(12) }}
                    icon={() => (
                         <Ionicons
                        name="home"
                        style={{ color: '#EDEDED',fontSize: mdscale(18) }}
          
                      />
                    )}
                    label="Home"
                    onPress={() => Navigation.navigate('Home')}
                />
                <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: mdscale(12) }}
                    icon={() => (
                        <Poll
                            name="poll"
                            style={{ color: "#FFFFFF", fontSize: mdscale(18) }}
                        />
                    )}
                    label="Polls & Surveys"
                    onPress={() => Navigation.navigate('Poll')}
                />

                {/* <DrawerItem
                    labelStyle={{ color: '#FFFFFF', fontSize: mdscale(12) }}
                    icon={() => (
                        <Poll
                            name="calendar-clock"

                            style={{ color: "#FFFFFF", fontSize: mdscale(18) }}
                        />
                    )}
                    label="Schedule"
                    onPress={() => Navigation.navigate('Schedule')}
                /> */}
              




            </DrawerContentScrollView>
        </View>
    )
}

export default DrawerContent

const styles = StyleSheet.create({})