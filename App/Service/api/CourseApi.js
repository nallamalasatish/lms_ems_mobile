import Base from './BaseApi';

export default class CourseApi extends Base {

  getCourseDetails(intl, data) {
    console.log('data :- ', data);
    //return this.apiClient.post(intl, 'CourseDetails', data);
    return this.apiClient.post(intl, 'api/Courses/CourseDetails', data);
  }
  getCourseDetailsByID(intl, data) {
    return this.apiClient.get(intl, `api/Courses/GetCourses/${data.userId}/${data.roleId}`);
  }
  getScheduleCourse(intl, data) {
    return this.apiClient.post(intl, 'api/CourseSchedule/GetCourseSchedule', data);
  }
  getStudentAssignment(intl, data) {
    return this.apiClient.post(intl, 'api/Assignments/StudentAssingments', data);
  }
  submitStudentAssignment(intl, data) {
    return this.apiClient.post(intl, 'api/Assignments/SubmitAssignments', data);
  }
  results(intl, data) {
    return this.apiClient.get(intl, `api/Assessment/StudentAssessementResult/${data.userId}`);
  }
  attendence(intl, data) {
    return this.apiClient.post(intl, 'api/Reports/RPT_DLC_STUDENT_ATTENDANCE', data);
  }
  getStudenReports(intl, data) {
    return this.apiClient.post(intl, 'api/Reports/SGPA_StudentReport', data);
  }
  getSemiterAndYear(intl, data) {
    return this.apiClient.post(intl, 'api/DataDictionary/GetDictionaryByKey', data);
  }

  getCourseSchedule(intl, data) {
    //return this.apiClient.post(intl, 'GetCourseSchedule', data);
    return this.apiClient.post(
      intl,
      'api/LearningMeterial/LearningMeterials',
      data,
    );
  }

  getLearningMaterials(intl, data) {
    //return this.apiClient.post(intl, 'GetCourseSchedule', data);
    return this.apiClient.post(
      intl,
      'api/CourseSchedule/GetCourseSchedule',
      data,
    );
  }

  getCourse(intl, data) {
    // return this.apiClient.get(
    //   intl,
    //   `GetAdminCourses/${data.USERID}/${data.ROLEID}/${data.TENANT_CODE}`,
    // );
    return this.apiClient.get(
      intl,
      `api/Courses/GetAdminCourses/${data.USERID}/${data.ROLEID}/${data.TENANT_CODE}`,);
  }
  loadAssessment(intl, data) {
    return this.apiClient.get(intl, `api/Assessment/LoadAssessmentDropdown/${data.COURSEID}/${data.COURSESHDID}`)
  }
  loadAssessmentByID(intl, data) {
    return this.apiClient.get(intl, `api/Assignments/GetAssignments/${data.COURSESHD_ID}`)
  }
  loadAssessmentDate(intl, data) {
    return this.apiClient.post(intl,'api/Assessment/GetAssessmentTime',data)
  }
  getScheduleAssessment(intl, data) {
    return this.apiClient.post(intl, 'api/ScheduleAssessment/GetList', data);
  }
  getAssignments(intl, data) {
    return this.apiClient.post(intl, 'api/Assignments/GetList', data)
  }
  LoadAssignment(intl, data) {
    return this.apiClient.post(intl, 'api/Assignments/LoadEvaluateAssignments', data)
  }
  LoadUserData(intl, data) {
    return this.apiClient.post(intl, 'api/Assessment/GetAssessmentUsers', data)
  }
  getAssessmentAnswers(intl, data) {
    return this.apiClient.post(intl, 'api/Assessment/GetAssessmentAnswers', data)
  }
 getAdminCourseSchedule(intl, data) {
    //return this.apiClient.post(intl, `GetAdminCourseSchedule`, data);
    return this.apiClient.post(
      intl,
      'api/CourseSchedule/GetAdminCourseSchedule',
      data,
    );
  }
  loadChapters(intl, data) {
    return this.apiClient.post(
      intl, 'api/Chapters/LoadChaptersByCourseSchedule', data);
  }
  setEvaluateAssignment(intl, data) {
    return this.apiClient.post(
      intl, 'api/Assignments/SetEvaluateAssignments', data);
  }
  getAssessmentById(intl, data) {
    return this.apiClient.post(intl, 'api/ScheduleAssessment/Get', data);
  }
  updateAssessment(intl, data) {
    return this.apiClient.post(intl, 'api/ScheduleAssessment/Update', data);
  }
  setEvaluateAssessment(intl, data) {
    return this.apiClient.post(intl, 'api/Assignments/SetEvaluateAssessments', data)
  }
  createAssessment(intl, data) {
    return this.apiClient.post(
      intl,
      'api/ScheduleAssessment/Create',
      data,
    );
  }
  loadAssessmentByCourseScheduleId(intl, data) {
    return this.apiClient.post(
      intl,
      'api/ScheduleAssessment/GetAssessmentByCourseSchedule',
      data,
    );
  }
  getSession(intl,data) {
    return this.apiClient.post (intl,'api/AVService/Joinurl',data);
  }
  getConferenceSession(intl,data) {
    return this.apiClient.post ( intl,'api/AVService/VCDetails',data);
  }
}
