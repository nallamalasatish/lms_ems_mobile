import Base from './BaseApi';

export default class MasterApi extends Base {
  getTitle(intl, data) {
    //return this.apiClient.post(intl, 'GetTitle', data);
    return this.apiClient.post(intl, 'api/DataDictionary/GetDictionaryByKey', {
      DictionaryCode: '8f3db27e',
    });
  }

  getGender(intl, data) {
    //return this.apiClient.post(intl, 'GetGender', data);
    return this.apiClient.post(intl, 'api/DataDictionary/GetDictionaryByKey', {
      DictionaryCode: '6cb13da6',
    });
  }
}
