import Base from './BaseApi';

export default class SurveyApi extends Base {
  getSurveys(intl, userId) {
    // return this.apiClient.get(intl, `GetSurveys/${userId}`);
    return this.apiClient.get(intl, `api/Survey/GetSurveys/${userId}`);
  }

  getSurveyQuestions(intl, data) {
    //return this.apiClient.get(intl, `GetSurveyQuestions/${data.userId}/${data.surveyId}`);
    return this.apiClient.get(
      intl,
      `api/Survey/GetSurveyQuestions/${data.userId}/${data.surveyId}`,
    );
  }

  saveSurvey(intl, data) {
    // return this.apiClient.post(intl, 'SetSurveys', data);
    return this.apiClient.post(intl, 'api/Survey/SetSurveys', data);
  }
}
