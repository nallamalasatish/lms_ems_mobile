import Base from './BaseApi';

export default class StudentApi extends Base {
  getStudents(intl, data) {
    //return this.apiClient.post(intl, `StudentsListBasedOnCS`, data);
    return this.apiClient.post(
      intl,
      `api/CourseSchedule/StudentsListBasedOnCS`,
      data,
    );
  }

  saveStudent(intl, data) {
    // return this.apiClient.post(intl, `SaveUser`, data);
    return this.apiClient.post(intl, `api/Account/SaveUser`, data);
  }

  updateStudent(intl, data) {
    //return this.apiClient.post(intl, `UpdateUser`, data);
    return this.apiClient.post(intl, `api/Account/UpdateUser`, data);
  }
}
