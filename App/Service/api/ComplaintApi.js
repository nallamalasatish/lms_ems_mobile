import Base from './BaseApi';

export default class ComplaintApi extends Base {
  getComplaints(intl, id) {
    // return this.apiClient.get(intl, `Complaints/${id}`);
    return this.apiClient.get(intl, `api/Account/Complaints/${id}`);
  }
}
