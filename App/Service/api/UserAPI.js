import Base from './BaseApi';
import {saveUserId, saveUserProfileInfo} from '../../Utils/utils/AsyncStorageHelper';

export default class UserAPI extends Base {
  loginUser(intl, data) {
    //return this.apiClient.post(intl, 'Login', data);
    return this.apiClient.post(intl, 'api/Account/Login', data);
  }

  parentLogin(intl, data) {
    //return this.apiClient.post(intl, 'ParentLogin', data);
    return this.apiClient.post(intl, 'api/Account/ParentLogin', data);
  }

  otpVerification(intl, data) {
    // return this.apiClient.post(intl, 'OtpVerification', data);
    return this.apiClient.post(intl, 'api/Account/OtpVerification', data);
  }

  async logout() {
    await saveUserId(undefined);
    await saveUserProfileInfo({});
  }

  getFAQ(intl, data) {
    //return this.apiClient.get(intl, `FAQ/${data.tenentCode}`);
    return this.apiClient.get(intl, `api/FAQ/Get/${data.tenentCode}`);
  }
}
