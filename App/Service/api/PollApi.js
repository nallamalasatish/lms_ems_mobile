import Base from './BaseApi';

export default class PollApi extends Base {
  getPolls(intl, userId) {
    //return this.apiClient.get(intl, `Getpolls/${userId}`);
    return this.apiClient.get(intl, `api/Poll/Getpolls/${userId}`);
  }

  getPollQuestions(intl, data) {
    // return this.apiClient.get(intl, `GetPollQuestions/${data.userId}/${data.pollId}`);
    return this.apiClient.get(
      intl,
      `api/Poll/GetPollQuestions/${data.userId}/${data.pollId}`,
    );
  }

  savePolls(intl, data) {
    // return this.apiClient.post(intl, 'SavePolls', data);
    return this.apiClient.post(intl, 'api/Poll/SavePolls', data);
  }
}
