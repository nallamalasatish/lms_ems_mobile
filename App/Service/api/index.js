import ApiClient from './ApiClient';
import UserAPI from './UserAPI';
import CourseApi from './CourseApi';
import SurveyApi from './SurveyApi';
import PollApi from './PollApi';
import ComplaintApi from './ComplaintApi';
import TrainerApi from './TrainerApi';
import StudentApi from './StudentApi';
import MasterApi from './MasterApi';

export const apiClient = new ApiClient();

const combinedAPI = {
  user: new UserAPI(apiClient),
  course: new CourseApi(apiClient),
  survey: new SurveyApi(apiClient),
  poll: new PollApi(apiClient),
  complaint: new ComplaintApi(apiClient),
  trainers: new TrainerApi(apiClient),
  student: new StudentApi(apiClient),
  master: new MasterApi(apiClient)
};

export default combinedAPI;
