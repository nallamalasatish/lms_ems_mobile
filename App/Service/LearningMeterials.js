import HttpClient from '../Utils/HttpClient';
import Storage from '../Utils/Storage';



async function LearningMeterials(data) {
    let endpoint = 'LearningMeterial/LearningMeterials';
    return HttpClient.post(endpoint, data);
  }
async function StudentAssingments(data) {
    let endpoint = 'Assignments/StudentAssingments';
    return HttpClient.post(endpoint, data);
  }
async function StudentAssingmentsFileUplode(file, imageFieldName,data) {
    let endpoint = 'Assignments/SubmitAssignments';
    return HttpClient.newFileUpload(endpoint,file, imageFieldName, data);
  }

  async function Joinurl(data) {
    let endpoint = 'AVService/Joinurl';
    return HttpClient.post(endpoint, data);
  }
  async function FAQGetList(data) {
    let endpoint = 'FAQ/GetList';
    return HttpClient.post(endpoint, data);
  }
  async function ForumsGetList(data) {
    let endpoint = 'Forums/GetList';
    return HttpClient.post(endpoint, data);
  }



  
export default {
    LearningMeterials,
    StudentAssingments,
    StudentAssingmentsFileUplode,
    Joinurl,
    FAQGetList,
    ForumsGetList
  };
  