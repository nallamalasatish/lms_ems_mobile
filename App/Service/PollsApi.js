import HttpClient from '../Utils/HttpClient';
async function getPolls(pram) {
    let endpoint = 'Poll/Getpolls/'+pram;
    return HttpClient.get(endpoint);
  }
async function GetPollQuestions(pram) {
    let endpoint = 'Poll/GetPollQuestions/'+pram;
    return HttpClient.get(endpoint);
  }
async function GetSurveys(pram) {
    let endpoint = 'Survey/GetSurveys/'+pram;
    return HttpClient.get(endpoint);
  }
async function GetSurveyQuestions(pram) {
    let endpoint = 'Survey/GetSurveyQuestions/'+pram;
    return HttpClient.get(endpoint);
  }
async function SavePolls(data) {
    let endpoint = 'Poll/SavePolls';
    return HttpClient.post(endpoint, data);
  }
async function SetSurveys(data) {
    let endpoint = 'Survey/SetSurveys';
    return HttpClient.post(endpoint, data);
  }

export default {
    getPolls,
    GetPollQuestions,
    SavePolls,
    GetSurveys,
    GetSurveyQuestions,
    SetSurveys
  };