import HttpClient from '../Utils/HttpClient';

async function CoursesGetCourses(pram) {
    let endpoint = 'Courses/GetCourses/'+pram;
    return HttpClient.get(endpoint);
  }
async function GetCourseSchedule(data) {
    let endpoint = 'CourseSchedule/GetCourseSchedule';
    return HttpClient.post(endpoint, data);
  }
async function GetListOfAssessments(data) {
    let endpoint = 'Assessment/ListOfAssessments';
    return HttpClient.post(endpoint, data);
  }
async function GetAssessment(data) {
    let endpoint = 'Assessment/GetAssessment';
    return HttpClient.post(endpoint, data);
  }
async function SetAssessments(data) {
    let endpoint = 'Assessment/SetAssessments';
    return HttpClient.post(endpoint, data);
  }


  export default {
    CoursesGetCourses,
    GetCourseSchedule,
    GetListOfAssessments,
    GetAssessment,
    SetAssessments
  }